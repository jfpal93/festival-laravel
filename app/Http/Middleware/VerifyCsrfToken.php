<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
        'participantes/*',
        'jury/*',
        'contactform/*',
        'home/*',
        'historia/*',
        'galas/*',
        'invitados/*',
		'jurados/*',
		'avales/*',
		'patrocinantes/*',
		'auspiciantes/*',
		'cronogramas/*',
		'desceventos/*',
		'invs/*',
		'lugares/*',
		'parts/*',
		'sedes/*',
		'destacados/*',
		'contactorganizacion/*',
        'contact_admin'

    ];
}
