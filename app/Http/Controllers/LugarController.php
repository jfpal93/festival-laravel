<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Lugar;

use Illuminate\Support\Facades\Auth;

class LugarController extends Controller
{
    //
    public function __construct(){
        $this->middleware("auth");
    }

    public function index()
    {
        //Muestra la coleccion de elementos
        $lugares = Lugar::all();

        return view("admin.lugares.index",["lugares"=>$lugares]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Despliega la vista para crear el nuevo elemento
        $lugar = new Lugar;
        return view("admin.lugares.create",["lugar"=>$lugar]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hasFile = $request->hasFile('cover') && $request->cover->isValid();
        $hasFile2 = $request->hasFile('cover2') && $request->cover2->isValid();
        $hasFile3 = $request->hasFile('cover3') && $request->cover3->isValid();




        //Guarda el nuevo elemento
        $lugar = new Lugar;
        $lugar->name = $request->name;
        $lugar->description = $request->description;
        $lugar->descripcion_corta = $request->short;
        $lugar->user_id = Auth::id();

        if($hasFile){
            $extension =  $request->cover->extension();
            $lugar->extension = $extension;
        }
        if($hasFile2){
            $extension2 =  $request->cover2->extension();
            $lugar->extension2 = $extension2;
        }
        if($hasFile3){
            $extension3 =  $request->cover3->extension();
            $lugar->extension3 = $extension3;
        }


        if($lugar->save()){
            if($hasFile){
                $request->cover->storeAs('images/lugares',"principal-$lugar->id.$extension");
            }
            if($hasFile2){
                $request->cover2->storeAs('images/lugares',"modal1-$lugar->id.$extension2");
            }
            if($hasFile3){
                $request->cover3->storeAs('images/lugares',"modal2-$lugar->id.$extension3");
            }

            return redirect("/home");
        }else{
            return view("admin.lugares.create",["lugar"=>$lugar]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Muestra el invitadoo
        $lugar = Lugar::find($id);
        return view('admin.lugares.show',['lugar'=>$lugar]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Edita el invitadoo
        $lugar = Lugar::find($id);
        return view("admin.lugares.edit",["lugar"=>$lugar]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$hasFile = $request->hasFile('cover') && $request->cover->isValid();
        $hasFile2 = $request->hasFile('cover2') && $request->cover2->isValid();
        $hasFile3 = $request->hasFile('cover3') && $request->cover3->isValid();

        //Actualiza lo editado
        $lugar = Lugar::find($id);
        $lugar->name = $request->name;
        $lugar->description = $request->description;
        $lugar->descripcion_corta = $request->short;
        $lugar->user_id = Auth::id();
        
        
        if($hasFile){
            $extension =  $request->cover->extension();
            $lugar->extension = $extension;
        }
        if($hasFile2){
            $extension2 =  $request->cover2->extension();
            $lugar->extension2 = $extension2;
        }
        if($hasFile3){
            $extension3 =  $request->cover3->extension();
            $lugar->extension3 = $extension3;
        }

        if($lugar->save()){
            if($hasFile){
                $request->cover->storeAs('images/lugares',"principal-$lugar->id.$extension");
            }
            if($hasFile2){
                $request->cover2->storeAs('images/lugares',"modal1-$lugar->id.$extension2");
            }
            if($hasFile3){
                $request->cover3->storeAs('images/lugares',"modal2-$lugar->id.$extension3");
            }
            return redirect("/home");
        }else{
            return view("admin.lugares.edit",["lugars"=>$lugar]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Elimina el elemento
        //$lugar = Lugar::find($id);
        Lugar::destroy($id);

        return redirect('/lugares');
    }
}
