<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jurado;

class JuryController extends Controller
{

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    //
    public function index(){
    	$jurados = Jurado::All();
		return view('admin.jurado',["jurados" => $jurados]);
	}
}
