<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\DescEvento;

use Illuminate\Support\Facades\Auth;

class DescEventoController extends Controller
{
    //
    public function __construct(){
        $this->middleware("auth");
    }

    public function index()
    {
        //Muestra la coleccion de elementos
        $desceventos = DescEvento::all();

        return view("admin.desceventos.index",["desceventos"=>$desceventos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Despliega la vista para crear el nuevo elemento
        $descevento = new DescEvento;
        return view("admin.desceventos.create",["descevento"=>$descevento]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hasFile = $request->hasFile('cover') && $request->cover->isValid();

        


        //Guarda el nuevo elemento
        $descevento = new DescEvento;
        
        $descevento->title = $request->title;
        $descevento->texto1 = $request->texto1;
        $descevento->texto2 = $request->texto2;

        $descevento->user_id = Auth::id();

        if($hasFile){
            $extension =  $request->cover->extension();
            $descevento->extension = $extension;
        }

        if($descevento->save()){
            if($hasFile){
                $request->cover->storeAs('images/desceventos',"$descevento->id.$extension");
            }
            return redirect("/home");
        }else{
            return view("admin.desceventos.create",["descevento"=>$descevento]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Muestra el invitadoo
        $descevento = DescEvento::find($id);
        return view('admin.desceventos.show',['descevento'=>$descevento]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Edita el invitadoo
        $descevento = DescEvento::find($id);
        return view("admin.desceventos.edit",["descevento"=>$descevento]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hasFile = $request->hasFile('cover') && $request->cover->isValid();
        //Actualiza lo editado
        $descevento = DescEvento::find($id);
        
        $descevento->title = $request->title;
        $descevento->texto1 = $request->texto1;
        $descevento->texto2 = $request->texto2;
        $descevento->user_id = Auth::id();

        if($hasFile){
            $extension =  $request->cover->extension();
            $descevento->extension = $extension;
        }

        if($descevento->save()){
            if($hasFile){
                $request->cover->storeAs('images/desceventos',"$descevento->id.$extension");
            }
            return redirect("/home");
        }else{
            return view("admin.desceventos.edit",["descevento"=>$descevento]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Elimina el elemento
        //$descevento = DescEvento::find($id);
        DescEvento::destroy($id);

        return redirect('/home');
    }
}

