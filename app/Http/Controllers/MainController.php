<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invitado;
use App\Part;
use App\Inv;
use App\Aval;
use App\Cronograma;
use App\DescEvento;
use App\Lugar;
use App\Sede;

use App\Auspiciante;
use App\Patrocinante;
use App\ContactCountry;
use App\Winner;
use DB;

use App\Portada;
// use DB;


class MainController extends Controller
{
    //
    public function home(){
    	// $shopping_cart_id=\Session::get('shopping_cart_id');

    	// $shopping_cart = shopping_cart::return Shopping_Cart::findBySession(null);
    	// \Session::put("shopping_cart_id",$shopping_cart->id);

    	$invitados = Invitado::All();
    	$parts = Part::All();
    	$invs = Inv::All();
    	$avals = Aval::All();
    	$cronogramas = Cronograma::All();
    	$desceventos = DescEvento::All();
    	$lugares = Lugar::All();
    	$sedes = Sede::All();
        $auspiciantes = Auspiciante::All();
        $patrocinantes = Patrocinante::All();
        $winners= Winner::All();
        // $contactcountrys = ContactCountry::All();


        $contactcountrys = DB::table('contact_countries')
                ->orderBy('country')
                ->get();
        $portada = DB::table('portadas')
                ->select('id','extension')
                ->where('name',"Portada principal")
                ->first();

        return view('main.home',["invitados" => $invitados,"parts" => $parts,"invs" => $invs,"avals" => $avals,"cronogramas" => $cronogramas,"lugares" => $lugares,"sedes" => $sedes,"desceventos" => $desceventos,"auspiciantes" => $auspiciantes,"patrocinantes" => $patrocinantes,"contactcountrys" => $contactcountrys,"portada" => $portada,"winners" => $winners]);
    }
}
