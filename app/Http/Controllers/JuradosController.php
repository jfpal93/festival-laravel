<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Jurado;

use Illuminate\Support\Facades\Auth;

class JuradosController extends Controller
{
    //
    public function __construct(){
        $this->middleware("auth");
    }

    public function index()
    {
        //Muestra la coleccion de elementos
        $jurados = Jurado::all();
        $juradoNew = new Jurado;

        return view("admin.jurados.index",["jurados"=>$jurados,"juradoNew"=>$juradoNew]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Despliega la vista para crear el nuevo elemento
        $jurado = new Jurado;
        return view("admin.jurados.create",["jurado"=>$jurado]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hasFile = $request->hasFile('cover') && $request->cover->isValid();

        $select = $request->country;


        //Guarda el nuevo elemento
        $jurado = new Jurado;
        $jurado->name = $request->name;
        $jurado->description = $request->description;
        $jurado->user_id = Auth::id();
        $jurado->country=$select;
        $jurado->descripcion_corta=$request->short;
        $jurado->tipo=$request->tipo;

        if($hasFile){
            $extension =  $request->cover->extension();
            $jurado->extension = $extension;
        }

        if($jurado->save()){
            if($hasFile){
                $request->cover->storeAs('images/jurados',"$jurado->id.$extension");
            }
            return redirect("/jury");
        }else{
            return view("admin.jurados.create",["jurado"=>$jurado]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Muestra el invitadoo
        $jurado = Jurado::find($id);
        return view('admin.jurados.show',['jurado'=>$jurado]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Edita el invitadoo
        $jurado = Jurado::find($id);
        return view("admin.jurados.edit",["jurado"=>$jurado]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$hasFile = $request->hasFile('cover') && $request->cover->isValid();
        //Actualiza lo editado
        $select = $request->country;
        $jurado = Jurado::find($id);
        $jurado->name = $request->name;
        $jurado->description = $request->description;
        $jurado->user_id = Auth::id();
        $jurado->country=$select;
        $jurado->descripcion_corta=$request->short;
        $jurado->tipo=$request->tipo;
        
        if($hasFile){
            $extension =  $request->cover->extension();
            $jurado->extension = $extension;
        }

        if($jurado->save()){
            if($hasFile){
                $request->cover->storeAs('images/jurados',"$jurado->id.$extension");
            }
            return redirect("/jury");
        }else{
            return view("admin.jurados.edit",["jurado"=>$jurado]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Elimina el elemento
        //$jurado = Jurado::find($id);
        Jurado::destroy($id);

        return redirect('/jury');
    }
}
