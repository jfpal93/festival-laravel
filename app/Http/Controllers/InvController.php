<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Inv;

use Illuminate\Support\Facades\Auth;

class InvController extends Controller
{
    //
    public function __construct(){
        $this->middleware("auth");
    }

    public function index()
    {
        //Muestra la coleccion de elementos
        $invs = Inv::all();

        return view("admin.invs.index",["invs"=>$invs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Despliega la vista para crear el nuevo elemento
        $inv = new Inv;
        return view("admin.invs.create",["inv"=>$inv]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hasFile = $request->hasFile('cover') && $request->cover->isValid();

        //Guarda el nuevo elemento
        $inv = new Inv;
        $inv->description = $request->description;
        $inv->user_id = Auth::id();

        if($hasFile){
            $extension =  $request->cover->extension();
            $inv->extension = $extension;
        }

        if($inv->save()){
            if($hasFile){
                $request->cover->storeAs('images/invs',"$inv->id.$extension");
            }
            return redirect("/home");
        }else{
            return view("admin.invs.create",["inv"=>$inv]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Muestra el invitadoo
        $inv = Inv::find($id);
        return view('admin.invs.show',['inv'=>$inv]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Edita el invitadoo
        $inv = Inv::find($id);
        return view("admin.invs.edit",["inv"=>$inv]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Actualiza lo editado
        $inv = Inv::find($id);
        $inv->description = $request->description;
        $inv->user_id = Auth::id();

        if($inv->save()){
            return redirect("/home");
        }else{
            return view("admin.invs.edit",["inv"=>$inv]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Elimina el elemento
        //$inv = Inv::find($id);
        Inv::destroy($id);

        return redirect('/home');
    }
}
