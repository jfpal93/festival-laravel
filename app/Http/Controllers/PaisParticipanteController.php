<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PaisParticipante;
use DB;
use App\ContactCountry;

use App\Portada;
 



class PaisParticipanteController extends Controller
{
    //
     public function home(){
    	// $shopping_cart_id=\Session::get('shopping_cart_id');

    	// $shopping_cart = shopping_cart::return Shopping_Cart::findBySession(null);
    	// \Session::put("shopping_cart_id",$shopping_cart->id);

    	$participantes = DB::table('pais_participantes')
                ->orderBy('country')
                ->get();
        $contactcountrys = DB::table('contact_countries')
                ->orderBy('country')
                ->get();
        $portada = DB::table('portadas')
                ->select('id','extension')
                ->where('name',"Portada alterna")
                ->first();     
        return view('main.participantes',["participantes" => $participantes,"contactcountrys" => $contactcountrys,"portada" => $portada]);
    }
}
