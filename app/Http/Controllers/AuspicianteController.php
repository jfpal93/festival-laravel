<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Auspiciante;

use Illuminate\Support\Facades\Auth;

class AuspicianteController extends Controller
{
    //
    public function __construct(){
        $this->middleware("auth");
    }

    public function index()
    {
        //Muestra la coleccion de elementos
        $auspiciantes = Auspiciante::all();

        return view("admin.auspiciantes.index",["auspiciantes"=>$auspiciantes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Despliega la vista para crear el nuevo elemento
        $auspiciante = new Auspiciante;
        return view("admin.auspiciantes.create",["auspiciante"=>$auspiciante]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hasFile = $request->hasFile('cover') && $request->cover->isValid();

        //Guarda el nuevo elemento
        $auspiciante = new Auspiciante;
        $auspiciante->name = $request->name;        $auspiciante->user_id = Auth::id();

        if($hasFile){
            $extension =  $request->cover->extension();
            $auspiciante->extension = $extension;
        }

        if($auspiciante->save()){
            if($hasFile){
                $request->cover->storeAs('images/auspiciantes',"$auspiciante->id.$extension");
            }
            return redirect("/home");
        }else{
            return view("admin.auspiciantes.create",["auspiciante"=>$auspiciante]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Muestra el invitadoo
        $auspiciante = Auspiciante::find($id);
        return view('admin.auspiciantes.show',['auspiciante'=>$auspiciante]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Edita el invitadoo
        $auspiciante = Auspiciante::find($id);
        return view("admin.auspiciantes.edit",["auspiciante"=>$auspiciante]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hasFile = $request->hasFile('cover') && $request->cover->isValid();

        //Actualiza lo editado
        $auspiciante = Auspiciante::find($id);
        $auspiciante->name = $request->name;
        $auspiciante->user_id = Auth::id();

        if($hasFile){
            $extension =  $request->cover->extension();
            $auspiciante->extension = $extension;
        }

        if($auspiciante->save()){
            if($hasFile){
                $request->cover->storeAs('images/auspiciantes',"$auspiciante->id.$extension");
            }
            return redirect("/home");
        }else{
            return view("admin.auspiciantes.edit",["auspiciante"=>$auspiciante]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Elimina el elemento
        //$auspiciante = Auspiciante::find($id);
        Auspiciante::destroy($id);

        return redirect('/home');
    }
}
