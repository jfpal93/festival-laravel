<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Destacado;
use App\Gala;

class HistoryController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galas = Gala::All();
        $destacados = Destacado::All();
        
        return view('admin.historia',["galas" => $galas,"destacados" => $destacados]);
    }
}
