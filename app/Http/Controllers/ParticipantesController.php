<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\PaisParticipante;

use Illuminate\Support\Facades\Auth;

class ParticipantesController extends Controller
{
    //
    public function __construct(){
        $this->middleware("auth");
    }

    public function index()
    {
        //Muestra la coleccion de elementos
        $participantes = PaisParticipante::All();

        return view("admin.participantes.index",["participantes"=>$participantes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Despliega la vista para crear el nuevo elemento
        $participante = new PaisParticipante;
        return view("admin.participantes.create",["participante"=>$participante]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hasFile = $request->hasFile('cover') && $request->cover->isValid();
        $hasFile2 = $request->hasFile('cover2') && $request->cover2->isValid();
        $hasFile3 = $request->hasFile('cover3') && $request->cover3->isValid();


        $select = $request->country;


        //Guarda el nuevo elemento
        $participante = new PaisParticipante;

        $participante->song = $request->song;
        $participante->country = $select;
        $participante->link = $request->link;

        //Interprete
        $participante->interprete_name = $request->interprete_name;
        $participante->interprete_description = $request->interprete_description;

        //Autor
        $participante->autor_name = $request->autor_name;
        $participante->autor_description = $request->autor_description;

        //Compositor
        $participante->compositor_name = $request->compositor_name;
        $participante->compositor_description = $request->compositor_description;

        //Social
        $participante->facebook = $request->facebook;
        $participante->twitter = $request->twitter;
        $participante->instagram = $request->instagram;
        $participante->youtube = $request->youtube;
        $participante->web = $request->web;
        $participante->social6 = $request->social6;


        $participante->user_id = Auth::id();

        if($hasFile){
            $extension =  $request->cover->extension();
            $participante->interprete_extension = $extension;
        }
        if($hasFile2){
            $extension2 =  $request->cover2->extension();
            $participante->autor_extension = $extension2;
        }
        if($hasFile3){
            $extension3 =  $request->cover3->extension();
            $participante->compositor_extension = $extension3;
        }

        if($participante->save()){
            if($hasFile){
                $request->cover->storeAs('images/participantes',"interprete-$participante->id-$participante->country.$extension");
            }
            if($hasFile2){
                $request->cover2->storeAs('images/participantes',"autor-$participante->id-$participante->country.$extension2");
            }
            if($hasFile3){
                $request->cover3->storeAs('images/participantes',"compositor-$participante->id-$participante->country.$extension3");
            }
            return redirect("/participantes");
        }else{
            return view("admin.participantes.create",["participante"=>$participante]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Muestra el invitadoo
        $participante = PaisParticipante::find($id);
        return view('admin.participantes.show',['participante'=>$participante]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Edita el invitadoo
        $participante = PaisParticipante::find($id);
        return view("admin.participantes.edit",["participante"=>$participante]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hasFile = $request->hasFile('cover') && $request->cover->isValid();
        $hasFile2 = $request->hasFile('cover2') && $request->cover2->isValid();
        $hasFile3 = $request->hasFile('cover3') && $request->cover3->isValid();

        //Actualiza lo editado
        $select = $request->country;
        $participante = PaisParticipante::find($id);
        $participante->song = $request->song;
        $participante->country = $select;
        $participante->link = $request->link;

        //Interprete
        $participante->interprete_name = $request->interprete_name;
        $participante->interprete_description = $request->interprete_description;

        //Autor
        $participante->autor_name = $request->autor_name;
        $participante->autor_description = $request->autor_description;

        //Compositor
        $participante->compositor_name = $request->compositor_name;
        $participante->compositor_description = $request->compositor_description;

        //Social
        $participante->facebook = $request->facebook;
        $participante->twitter = $request->twitter;
        $participante->instagram = $request->instagram;
        $participante->youtube = $request->youtube;
        $participante->web = $request->web;
        $participante->social6 = $request->social6;
        
        $participante->user_id = Auth::id();

        // if($participante->save()){
        //     return redirect("/participantes");
        // }else{
        //     return view("admin.participantes.edit",["participante"=>$participante]);

        // }

        if($hasFile){
            $extension =  $request->cover->extension();
            $participante->interprete_extension = $extension;
        }
        if($hasFile2){
            $extension2 =  $request->cover2->extension();
            $participante->autor_extension = $extension2;
        }
        if($hasFile3){
            $extension3 =  $request->cover3->extension();
            $participante->compositor_extension = $extension3;
        }

        if($participante->save()){
            if($hasFile){
                $request->cover->storeAs('images/participantes',"interprete-$participante->id-$participante->country.$extension");
            }
            if($hasFile2){
                $request->cover2->storeAs('images/participantes',"autor-$participante->id-$participante->country.$extension2");
            }
            if($hasFile3){
                $request->cover3->storeAs('images/participantes',"compositor-$participante->id-$participante->country.$extension3");
            }
            return redirect("/participantes");
        }else{
            return view("admin.participantes.create",["participante"=>$participante]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Elimina el elemento
        //$participante = PaisParticipante::find($id);
        PaisParticipante::destroy($id);

        return redirect('/participantes');
    }
}
