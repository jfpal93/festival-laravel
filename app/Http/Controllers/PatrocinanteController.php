<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Patrocinante;

use Illuminate\Support\Facades\Auth;

class PatrocinanteController extends Controller
{
    //
    //
    public function __construct(){
        $this->middleware("auth");
    }

    public function index()
    {
        //Muestra la coleccion de elementos
        $patrocinantes = Patrocinante::all();
        return view("admin.patrocinantes.index",["patrocinantes"=>$patrocinantes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Despliega la vista para crear el nuevo elemento
        $patrocinante = new Patrocinante;
        return view("admin.patrocinantes.create",["patrocinante"=>$patrocinante]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hasFile = $request->hasFile('cover') && $request->cover->isValid();

        //Guarda el nuevo elemento
        $patrocinante = new Patrocinante;
        $patrocinante->name = $request->name;        
        $patrocinante->user_id = Auth::id();

        if($hasFile){
            $extension =  $request->cover->extension();
            $patrocinante->extension = $extension;
        }

        if($patrocinante->save()){
            if($hasFile){
                $request->cover->storeAs('images/patrocinantes',"$patrocinante->id.$extension");
            }
            return redirect("/home");
        }else{
            return view("admin.patrocinantes.create",["patrocinante"=>$patrocinante]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Muestra el invitadoo
        $patrocinante = Patrocinante::find($id);
        return view('admin.patrocinantes.show',['patrocinante'=>$patrocinante]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Edita el invitadoo
        $patrocinante = Patrocinante::find($id);
        return view("admin.patrocinantes.edit",["patrocinante"=>$patrocinante]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hasFile = $request->hasFile('cover') && $request->cover->isValid();

        //Actualiza lo editado
        $patrocinante = Patrocinante::find($id);
        $patrocinante->name = $request->name;
        $patrocinante->user_id = Auth::id();

        if($hasFile){
            $extension =  $request->cover->extension();
            $patrocinante->extension = $extension;
        }

        if($patrocinante->save()){
            if($hasFile){
                $request->cover->storeAs('images/patrocinantes',"$patrocinante->id.$extension");
            }
            return redirect("/home");
        }else{
            return view("admin.patrocinantes.edit",["patrocinante"=>$patrocinante]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Elimina el elemento
        //$patrocinante = Patrocinante::find($id);
        Patrocinante::destroy($id);

        return redirect('/home');
    }
}
