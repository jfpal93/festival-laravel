<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactFormRequest;
use App\Notifications\InboxMessage;

use App\Mail\ContactForm;

use App\Contacto;
use Mail;
use App\ContactCountry;
use DB;

use App\Portada;
   



class ContactosController extends Controller
{
    //
    
     public function home(){
    	// $shopping_cart_id=\Session::get('shopping_cart_id');

    	// $shopping_cart = shopping_cart::return Shopping_Cart::findBySession(null);
    	// \Session::put("shopping_cart_id",$shopping_cart->id);

    	// $contactos = Contacto::All();
      $contactcountrys = DB::table('contact_countries')
                ->orderBy('country')
                ->get();
      $portada = DB::table('portadas')
                ->select('id','extension')
                ->where('name',"Portada alterna")
                ->first();   
        return view('main.contactos',["contactcountrys" => $contactcountrys,"portada" => $portada]);
    }

    public function mailToAdmin(Request $request)
	{        
        \Mail::send('mailers.contact',
        array(
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'user_message' => $request->get('message')
        ), function($message)
          {
               $message->from('noreply@machincorp.com');
               $message->to('festival.pde@gmail.com', 'Admin')->subject('Mail contacto página web');
           });
         
            return back()->with('success', 'Thanks for contacting us!'); 
   

	}
}
