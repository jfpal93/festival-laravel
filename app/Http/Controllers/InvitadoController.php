<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Invitado;

use DB;
use App\ContactCountry;

use App\Portada;
 



class InvitadoController extends Controller
{
    //
    public function home(){
    	// $shopping_cart_id=\Session::get('shopping_cart_id');

    	// $shopping_cart = shopping_cart::return Shopping_Cart::findBySession(null);
    	// \Session::put("shopping_cart_id",$shopping_cart->id);

    	$invitados = Invitado::All();
        $contactcountrys = DB::table('contact_countries')
                ->orderBy('country')
                ->get();
        $portada = DB::table('portadas')
                ->select('id','extension')
                ->where('name',"Portada alterna")
                ->first();     
        return view('main.invitados',["invitados" => $invitados,"contactcountrys" => $contactcountrys,"portada" => $portada]);
    }
}
