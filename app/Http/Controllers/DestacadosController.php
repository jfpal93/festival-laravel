<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Destacado;

use Illuminate\Support\Facades\Auth;

class DestacadosController extends Controller
{
    //
    public function __construct(){
        $this->middleware("auth");
    }

    public function index()
    {
        //Muestra la coleccion de elementos
        $destacados = Destacado::all();

        return view("admin.destacados.index",["destacados"=>$destacados]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Despliega la vista para crear el nuevo elemento
        $destacado = new Destacado;
        return view("admin.destacados.create",["destacado"=>$destacado]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hasFile = $request->hasFile('cover') && $request->cover->isValid();

        
        $select = $request->day;

        //Guarda el nuevo elemento
        $destacado = new Destacado;
        
        $destacado->title = $request->title;
        $destacado->ano = $request->ano;
        $destacado->link = $request->link;

        $destacado->user_id = Auth::id();

        if($hasFile){
            $extension =  $request->cover->extension();
            $destacado->extension = $extension;
        }

        if($destacado->save()){
            if($hasFile){
                $request->cover->storeAs('images/destacados',"$destacado->id.$extension");
            }
            return redirect("/historia");
        }else{
            return view("admin.destacados.create",["destacado"=>$destacado]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Muestra el invitadoo
        $destacado = Destacado::find($id);
        return view('admin.destacados.show',['destacado'=>$destacado]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Edita el invitadoo
        $destacado = Destacado::find($id);
        return view("admin.destacados.edit",["destacado"=>$destacado]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Actualiza lo editado
        $destacado = Destacado::find($id);
        
        $destacado->title = $request->title;
        $destacado->ano = $request->ano;
        $destacado->link = $request->link;
        $destacado->user_id = Auth::id();

        if($destacado->save()){
            return redirect("/historia");
        }else{
            return view("admin.destacados.edit",["destacado"=>$destacado]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Elimina el elemento
        //$destacado = Destacado::find($id);
        Destacado::destroy($id);

        return redirect('/historia');
    }
}
