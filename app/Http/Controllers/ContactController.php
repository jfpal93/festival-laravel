<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Contacto;

class ContactController extends Controller
{
    //
    public function __construct(){
        $this->middleware("auth");
    }

    public function index()
    {
        //Muestra la coleccion de elementos
        $contactos = Contacto::all();

        return view("admin.contactos.index",["contactos"=>$contactos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Despliega la vista para crear el nuevo elemento
        $contacto = new Contacto;
        return view("admin.contactos.create",["contacto"=>$contacto]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hasFile = $request->hasFile('cover') && $request->cover->isValid();

        //Guarda el nuevo elemento
        $contacto = new Contacto;
        $contacto->name = $request->name;   
        $contacto->mail = $request->mail;         
        $contacto->user_id = Auth::id();

        if($hasFile){
            $extension =  $request->cover->extension();
            $contacto->extension = $extension;
        }

        if($contacto->save()){
            if($hasFile){
                $request->cover->storeAs('images/contactos',"$contacto->id.$extension");
            }
            return redirect("/contactform");
        }else{
            return view("admin.contactos.create",["contacto"=>$contacto]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Muestra el invitadoo
        $contacto = Contacto::find($id);
        return view('admin.contactos.show',['contacto'=>$contacto]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Edita el invitadoo
        $contacto = Contacto::find($id);
        return view("admin.contactos.edit",["contacto"=>$contacto]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Actualiza lo editado
        $contacto = Contacto::find($id);
        $contacto->name = $request->name;
        $contacto->mail = $request->mail;   
        $contacto->user_id = Auth::id();

        if($contacto->save()){
            return redirect("/contactform");
        }else{
            return view("admin.contactos.edit",["contacto"=>$contacto]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Elimina el elemento
        //$contacto = Contacto::find($id);
        Contacto::destroy($id);

        return redirect('/contactform');
    }
}
