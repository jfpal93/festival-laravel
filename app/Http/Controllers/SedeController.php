<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Sede;

use Illuminate\Support\Facades\Auth;

class SedeController extends Controller
{
    //
    public function __construct(){
        $this->middleware("auth");
    }

    public function index()
    {
        //Muestra la coleccion de elementos
        $sedes = Sede::all();

        return view("admin.sedes.index",["sedes"=>$sedes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Despliega la vista para crear el nuevo elemento
        $sede = new Sede;
        return view("admin.sedes.create",["sede"=>$sede]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hasFile = $request->hasFile('cover') && $request->cover->isValid();
        $hasFile2 = $request->hasFile('cover2') && $request->cover2->isValid();
        $hasFile3 = $request->hasFile('cover3') && $request->cover3->isValid();


        //Guarda el nuevo elemento
        $sede = new Sede;
        $sede->name = $request->name;
        $sede->description = $request->description;
        $sede->descripcion_corta = $request->short;
        $sede->user_id = Auth::id();

        if($hasFile){
            $extension =  $request->cover->extension();
            $sede->extension = $extension;
        }
        if($hasFile2){
            $extension2 =  $request->cover2->extension();
            $sede->extension2 = $extension2;
        }
        if($hasFile3){
            $extension3 =  $request->cover3->extension();
            $sede->extension3 = $extension3;
        }

        if($sede->save()){
            if($hasFile){
                $request->cover->storeAs('images/sedes',"principal-$sede->id.$extension");
            }
            if($hasFile2){
                $request->cover2->storeAs('images/sedes',"modal1-$sede->id.$extension2");
            }
            if($hasFile3){
                $request->cover3->storeAs('images/sedes',"modal2-$sede->id.$extension3");
            }
            return redirect("/home");
        }else{
            return view("admin.sedes.create",["sede"=>$sede]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Muestra el invitadoo
        $sede = Sede::find($id);
        return view('admin.sedes.show',['sede'=>$sede]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Edita el invitadoo
        $sede = Sede::find($id);
        return view("admin.sedes.edit",["sede"=>$sede]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $hasFile = $request->hasFile('cover') && $request->cover->isValid();
        $hasFile2 = $request->hasFile('cover2') && $request->cover2->isValid();
        $hasFile3 = $request->hasFile('cover3') && $request->cover3->isValid();
        //Actualiza lo editado
        $sede = Sede::find($id);
        $sede->name = $request->name;
        $sede->description = $request->description;
        $sede->descripcion_corta = $request->short;
        $sede->user_id = Auth::id();

        if($hasFile){
            $extension =  $request->cover->extension();
            $sede->extension = $extension;
        }
        if($hasFile2){
            $extension2 =  $request->cover2->extension();
            $sede->extension2 = $extension2;
        }
        if($hasFile3){
            $extension3 =  $request->cover3->extension();
            $sede->extension3 = $extension3;
        }

        if($sede->save()){
            if($hasFile){
                $request->cover->storeAs('images/sedes',"principal-$sede->id.$extension");
            }
            if($hasFile2){
                $request->cover2->storeAs('images/sedes',"modal1-$sede->id.$extension2");
            }
            if($hasFile3){
                $request->cover3->storeAs('images/sedes',"modal2-$sede->id.$extension3");
            }
            return redirect("/home");
        }else{
            return view("admin.sedes.edit",["sede"=>$sede]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Elimina el elemento
        //$sede = Sede::find($id);
        Sede::destroy($id);

        return redirect('/home');
    }
}
