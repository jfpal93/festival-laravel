<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Contacto;
use App\ContactCountry;
use Illuminate\Support\Facades\Auth;


class ContactFormController extends Controller
{
    //


/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contactos = Contacto::All();
        $contactcountrys = ContactCountry::All();
        
        return view('admin.contactos',["contactos" => $contactos,"contactcountrys" => $contactcountrys]);
    }

}
