<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Gala;
use App\Destacado;
use App\ContactCountry;
use DB;

use App\Portada;



class HistoriaController extends Controller
{
    //
    public function home(){
    	$galas = DB::table('galas')
                ->orderBy('ano','DESC')
                ->get();
		$contactcountrys = DB::table('contact_countries')
                ->orderBy('country')
                ->get();
        $portada = DB::table('portadas')
                ->select('id','extension')
                ->where('name',"Portada alterna")
                ->first();      
		return view('main.historia',["galas" => $galas,"contactcountrys" => $contactcountrys,"portada" => $portada]);
	}
    
}
