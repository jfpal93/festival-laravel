<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Cronograma;

use Illuminate\Support\Facades\Auth;

class CronogramaController extends Controller
{
    //
    public function __construct(){
        $this->middleware("auth");
    }

    public function index()
    {
        //Muestra la coleccion de elementos
        $cronogramas = Cronograma::all();

        return view("admin.cronogramas.index",["cronogramas"=>$cronogramas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Despliega la vista para crear el nuevo elemento
        $cronograma = new Cronograma;
        return view("admin.cronogramas.create",["cronograma"=>$cronograma]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        //Guarda el nuevo elemento
        $cronograma = new Cronograma;
        
        $cronograma->fecha = $request->fecha;
        $cronograma->lugar = $request->lugar;
        $cronograma->evento = $request->evento;

        $cronograma->user_id = Auth::id();


        if($cronograma->save()){
            
            return redirect("/home");
        }else{
            return view("admin.cronogramas.create",["cronograma"=>$cronograma]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Muestra el invitadoo
        $cronograma = Cronograma::find($id);
        return view('admin.cronogramas.show',['cronograma'=>$cronograma]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Edita el invitadoo
        $cronograma = Cronograma::find($id);
        return view("admin.cronogramas.edit",["cronograma"=>$cronograma]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Actualiza lo editado
        $cronograma = Cronograma::find($id);
        
        $cronograma->fecha = $request->fecha;
        $cronograma->lugar = $request->lugar;
        $cronograma->evento = $request->evento;

        if($cronograma->save()){
            return redirect("/home");
        }else{
            return view("admin.cronogramas.edit",["cronograma"=>$cronograma]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Elimina el elemento
        //$cronograma = Cronograma::find($id);
        Cronograma::destroy($id);

        return redirect('/home');
    }
}
