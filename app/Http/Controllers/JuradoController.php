<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Jurado;
use App\ContactCountry;

use App\Portada;
      


class JuradoController extends Controller
{
    //
    public function home(){
    	// $jurados = Jurado::All();
    	$jurados = DB::table('jurados')
                ->where('tipo','participante')
                ->get();
    	$juradosvideo = DB::table('jurados')
                ->where('tipo','video')
                ->get();
        $contactcountrys = DB::table('contact_countries')
                ->orderBy('country')
                ->get();
        $portada = DB::table('portadas')
                ->select('id','extension')
                ->where('name',"Portada alterna")
                ->first();
		return view('main.jurado',["jurados" => $jurados,"juradosvideo"=>$juradosvideo,"contactcountrys" => $contactcountrys,"portada" => $portada]);
	}
}
