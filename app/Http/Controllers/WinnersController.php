<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Winner;

use Illuminate\Support\Facades\Auth;

class WinnersController extends Controller
{
    //
     public function __construct(){
        $this->middleware("auth");
    }

    public function index()
    {
        //Muestra la coleccion de elementos
        $winners = Winner::all();

        return view("admin.winners.index",["winners"=>$winners]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Despliega la vista para crear el nuevo elemento
        $winner = new Winner;
        return view("admin.winners.create",["winner"=>$winner]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hasFile = $request->hasFile('cover') && $request->cover->isValid();

        $select = $request->country;


        //Guarda el nuevo elemento
        $winner = new Winner;
        $winner->name = $request->name;
        $winner->description = $request->description;
        $winner->user_id = Auth::id();
        $winner->country=$select;
        $winner->descripcion_corta=$request->short;
        $winner->link=$request->link;

        if($hasFile){
            $extension =  $request->cover->extension();
            $winner->extension = $extension;
        }

        if($winner->save()){
            if($hasFile){
                $request->cover->storeAs('images/winners',"$winner->id.$extension");
            }
            return redirect("/winners");
        }else{
            return view("admin.winners.create",["winner"=>$winner]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Muestra el winnero
        $winner = Winner::find($id);
        return view('admin.winners.show',['winner'=>$winner]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Edita el winnero
        $winner = Winner::find($id);
        return view("admin.winners.edit",["winner"=>$winner]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	
        
        $winner = Winner::find($id);
        // dd($request->enabled);
        if($request->enabled==="1"){
            $winner->enabled = 1;
        }
        else{
            $winner->enabled = 0;
        }

        
        $winner->ano = $request->ano;

        $winner->titulo1 = $request->titulo1;
        $winner->link1 = $request->link1;

        $winner->titulo2 = $request->titulo2;
        $winner->link2 = $request->link2;

        $winner->titulo3 = $request->titulo3;
        $winner->link3 = $request->link3;

        $winner->user_id = Auth::id();
        
        
        if($winner->save()){
           
            return redirect("/winners");
        }else{
            return view("admin.winners.edit",["winner"=>$winner]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Elimina el elemento
        //$winner = Winner::find($id);
        Winner::destroy($id);

        return redirect('/winners');
    }
}
