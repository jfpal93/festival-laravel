<?php

namespace App\Http\Controllers;


use App\Gala;

use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class GalasController extends Controller
{
    //
    public function __construct(){
        $this->middleware("auth");
    }

    public function index()
    {
        //Muestra la coleccion de elementos
        $galas = Gala::all();

        return view("admin.galas.index",["galas"=>$galas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Despliega la vista para crear el nuevo elemento
        $gala = new Gala;
        return view("admin.galas.create",["gala"=>$gala]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hasFile = $request->hasFile('cover') && $request->cover->isValid();

        

        //Guarda el nuevo elemento
        $gala = new Gala;
        
        $gala->ano = $request->ano;
        $gala->link = $request->link;
        
        $gala->titulo1 = $request->titulo1;
        $gala->nombre1 = $request->nombre1;
        $gala->country1 = $request->country1;
        $gala->cancion1 = $request->cancion1;

        $gala->titulo2 = $request->titulo2;
        $gala->nombre2 = $request->nombre2;
        $gala->country2 = $request->country2;
        $gala->cancion2 = $request->cancion2;

        $gala->titulo3 = $request->titulo3;
        $gala->nombre3 = $request->nombre3;
        $gala->country3 = $request->country3;
        $gala->cancion3 = $request->cancion3;

        $gala->titulo4 = $request->titulo4;
        $gala->nombre4 = $request->nombre4;
        $gala->country4 = $request->country4;
        $gala->cancion4 = $request->cancion4;

        $gala->user_id = Auth::id();

        if($hasFile){
            $extension =  $request->cover->extension();
            $gala->extension = $extension;
        }

        if($gala->save()){
            if($hasFile){
                $request->cover->storeAs('images/galas',"$gala->id.$extension");
            }
            return redirect("/historia");
        }else{
            return view("admin.galas.create",["gala"=>$gala]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Muestra el invitadoo
        $gala = Gala::find($id);
        return view('admin.galas.show',['gala'=>$gala]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Edita el invitadoo
        $gala = Gala::find($id);
        return view("admin.galas.edit",["gala"=>$gala]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Actualiza lo editado
        $gala = Gala::find($id);
        
        $gala->ano = $request->ano;
        $gala->link = $request->link;

        $gala->titulo1 = $request->titulo1;
        $gala->nombre1 = $request->nombre1;
        $gala->country1 = $request->country1;
        $gala->cancion1 = $request->cancion1;

        $gala->titulo2 = $request->titulo2;
        $gala->nombre2 = $request->nombre2;
        $gala->country2 = $request->country2;
        $gala->cancion2 = $request->cancion2;

        $gala->titulo3 = $request->titulo3;
        $gala->nombre3 = $request->nombre3;
        $gala->country3 = $request->country3;
        $gala->cancion3 = $request->cancion3;

        $gala->titulo4 = $request->titulo4;
        $gala->nombre4 = $request->nombre4;
        $gala->country4 = $request->country4;
        $gala->cancion4 = $request->cancion4;

        $gala->user_id = Auth::id();

        if($gala->save()){
            return redirect("/historia");
        }else{
            return view("admin.galas.edit",["gala"=>$gala]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Elimina el elemento
        //$gala = Gala::find($id);
        Gala::destroy($id);

        return redirect('/home');
    }
}
