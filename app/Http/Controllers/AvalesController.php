<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Aval;

use Illuminate\Support\Facades\Auth;

class AvalesController extends Controller
{
    //
    public function __construct(){
        $this->middleware("auth");
    }

    public function index()
    {
        //Muestra la coleccion de elementos
        $avales = Aval::all();

        return view("admin.avales.index",["avales"=>$avales]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Despliega la vista para crear el nuevo elemento
        $aval = new Aval;
        return view("admin.avales.create",["aval"=>$aval]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hasFile = $request->hasFile('cover') && $request->cover->isValid();

        //Guarda el nuevo elemento
        $aval = new Aval;
        $aval->name = $request->name;        $aval->user_id = Auth::id();

        if($hasFile){
            $extension =  $request->cover->extension();
            $aval->extension = $extension;
        }

        if($aval->save()){
            if($hasFile){
                $request->cover->storeAs('images/avales',"$aval->id.$extension");
            }
            return redirect("/home");
        }else{
            return view("admin.avales.create",["aval"=>$aval]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Muestra el invitadoo
        $aval = Aval::find($id);
        return view('admin.avales.show',['aval'=>$aval]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Edita el invitadoo
        $aval = Aval::find($id);
        return view("admin.avales.edit",["aval"=>$aval]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hasFile = $request->hasFile('cover') && $request->cover->isValid();

        //Actualiza lo editado
        $aval = Aval::find($id);
        $aval->name = $request->name;
        $aval->user_id = Auth::id();

        if($hasFile){
            $extension =  $request->cover->extension();
            $aval->extension = $extension;
        }

        if($aval->save()){
            if($hasFile){
                $request->cover->storeAs('images/avales',"$aval->id.$extension");
            }
            return redirect("/home");
        }else{
            return view("admin.avales.edit",["aval"=>$aval]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Elimina el elemento
        //$aval = Aval::find($id);
        Aval::destroy($id);

        return redirect('/home');
    }
}
