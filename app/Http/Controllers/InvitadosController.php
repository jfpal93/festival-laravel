<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invitado;

use Illuminate\Support\Facades\Auth;

class InvitadosController extends Controller
{
    //
    public function __construct(){
        $this->middleware("auth");
    }

    public function index()
    {
        //Muestra la coleccion de elementos
        $invitados = Invitado::all();

        return view("admin.invitados.index",["invitados"=>$invitados]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Despliega la vista para crear el nuevo elemento
        $invitado = new Invitado;
        return view("admin.invitados.create",["invitado"=>$invitado]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hasFile = $request->hasFile('cover') && $request->cover->isValid();

        $select = $request->country;


        //Guarda el nuevo elemento
        $invitado = new Invitado;
        $invitado->name = $request->name;
        $invitado->description = $request->description;
        $invitado->user_id = Auth::id();
        $invitado->country=$select;
        $invitado->descripcion_corta=$request->short;
        $invitado->link=$request->link;

        if($hasFile){
            $extension =  $request->cover->extension();
            $invitado->extension = $extension;
        }

        if($invitado->save()){
            if($hasFile){
                $request->cover->storeAs('images/invitados',"$invitado->id.$extension");
            }
            return redirect("/invitados");
        }else{
            return view("admin.invitados.create",["invitado"=>$invitado]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Muestra el invitadoo
        $invitado = Invitado::find($id);
        return view('admin.invitados.show',['invitado'=>$invitado]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Edita el invitadoo
        $invitado = Invitado::find($id);
        return view("admin.invitados.edit",["invitado"=>$invitado]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$hasFile = $request->hasFile('cover') && $request->cover->isValid();
        //Actualiza lo editado
        $select = $request->country;
        $invitado = Invitado::find($id);
        $invitado->name = $request->name;
        $invitado->description = $request->description;
        $invitado->user_id = Auth::id();
        $invitado->country=$select;
        $invitado->descripcion_corta=$request->short;
        $invitado->link=$request->link;
        
        if($hasFile){
            $extension =  $request->cover->extension();
            $invitado->extension = $extension;
        }

        if($invitado->save()){
           if($hasFile){
                $request->cover->storeAs('images/invitados',"$invitado->id.$extension");
            }
            return redirect("/invitados");
        }else{
            return view("admin.invitados.edit",["invitado"=>$invitado]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Elimina el elemento
        //$invitado = Invitado::find($id);
        Invitado::destroy($id);

        return redirect('/invitados');
    }
}
