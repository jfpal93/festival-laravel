<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Part;

use Illuminate\Support\Facades\Auth;

class PartController extends Controller
{
    //
    public function __construct(){
        $this->middleware("auth");
    }

    public function index()
    {
        //Muestra la coleccion de elementos
        $parts = Part::all();

        return view("admin.parts.index",["parts"=>$parts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Despliega la vista para crear el nuevo elemento
        $part = new Part;
        return view("admin.parts.create",["part"=>$part]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hasFile = $request->hasFile('cover') && $request->cover->isValid();

        //Guarda el nuevo elemento
        $part = new Part;
        $part->description = $request->description;
        $part->user_id = Auth::id();

        if($hasFile){
            $extension =  $request->cover->extension();
            $part->extension = $extension;
        }

        if($part->save()){
            if($hasFile){
                $request->cover->storeAs('images/parts',"$part->id.$extension");
            }
            return redirect("/home");
        }else{
            return view("admin.parts.create",["part"=>$part]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Muestra el invitadoo
        $part = Part::find($id);
        return view('admin.parts.show',['part'=>$part]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Edita el invitadoo
        $part = Part::find($id);
        return view("admin.parts.edit",["part"=>$part]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Actualiza lo editado
        $part = Part::find($id);
        $part->description = $request->description;
        $part->user_id = Auth::id();

        if($part->save()){
            return redirect("/home");
        }else{
            return view("admin.parts.edit",["part"=>$part]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Elimina el elemento
        //$part = Part::find($id);
        Part::destroy($id);

        return redirect('/home');
    }
}
