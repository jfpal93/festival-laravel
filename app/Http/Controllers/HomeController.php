<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Invitado;
use App\Part;
use App\Inv;
use App\Aval;
use App\Cronograma;
use App\DescEvento;
use App\Lugar;
use App\Sede;
use App\Auspiciante;
use App\Patrocinante;
use App\Portada;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invitados = Invitado::All();
        $parts = Part::All();
        $invs = Inv::All();
        $avals = Aval::All();
        $cronogramas = Cronograma::All();
        $desceventos = DescEvento::All();
        $lugars = Lugar::All();
        $sedes = Sede::All();
        $portadas = Portada::All();
        
        $auspiciantes = Auspiciante::All();
        $patrocinantes = Patrocinante::All();
        return view('admin.home',["invitados" => $invitados,"parts" => $parts,"invs" => $invs,"avales" => $avals,"cronogramas" => $cronogramas,"lugares" => $lugars,"sedes" => $sedes,"desceventos" => $desceventos,"auspiciantes" => $auspiciantes,"patrocinantes" => $patrocinantes,"portadas" => $portadas]);
    }
}
