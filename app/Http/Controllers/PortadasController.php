<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Portada;
use Illuminate\Support\Facades\Auth;

class PortadasController extends Controller
{
    //
    public function __construct(){
        $this->middleware("auth");
    }

    public function index()
    {
        //Muestra la coleccion de elementos
        $portadas = Portada::all();
        return view("admin.portadas.index",["portadas"=>$portadas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Despliega la vista para crear el nuevo elemento
        $portada = new Portada;
        return view("admin.portadas.create",["portada"=>$portada]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hasFile = $request->hasFile('cover') && $request->cover->isValid();

        //Guarda el nuevo elemento
        $portada = new Portada;
        $portada->name = $request->name;        
        $portada->user_id = Auth::id();

        if($hasFile){
            $extension =  $request->cover->extension();
            $portada->extension = $extension;
        }

        if($portada->save()){
            if($hasFile){
                $request->cover->storeAs('images/portadas',"$portada->id.$extension");
            }
            return redirect("/home");
        }else{
            return view("admin.portadas.create",["portada"=>$portada]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Muestra el invitadoo
        $portada = Portada::find($id);
        return view('admin.portadas.show',['portada'=>$portada]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Edita el invitadoo
        $portada = Portada::find($id);
        return view("admin.portadas.edit",["portada"=>$portada]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hasFile = $request->hasFile('cover') && $request->cover->isValid();

        //Actualiza lo editado
        $portada = Portada::find($id);
        // $portada->name = $request->name;
        $portada->user_id = Auth::id();

        if($hasFile){
            $extension =  $request->cover->extension();
            $portada->extension = $extension;
        }

        if($portada->save()){
            if($hasFile){
                $request->cover->storeAs('images/portadas',"$portada->id.$extension");
            }
            return redirect("/home");
        }else{
            return view("admin.portadas.edit",["portada"=>$portada]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Elimina el elemento
        //$portada = Portada::find($id);
        Portada::destroy($id);

        return redirect('/home');
    }
}
