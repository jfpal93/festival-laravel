<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interprete extends Model
{
	protected $table='interpretes';
    protected $fillable = ['name','pais_participante_id','description','extension'];
    //
    public function paisparticipante()
    {
        return $this->belongsTo('App\PaisParticipante','pais_participante_id','id');
    }
}
