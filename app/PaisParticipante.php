<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaisParticipante extends Model
{

	// protected $table='pais_participantes';
 //    protected $fillable = ['song','country','link'];
 //    //
 //    public function inteprete()
 //  {
 //    return $this->hasOne('app\Interprete','id','pais_participante_id');
 //  }
 //  public function autor()
 //  {
 //    return $this->hasOne('app\Autor','id','pais_participante_id');
 //  }
 //  public function compositor()
 //  {
 //    return $this->hasOne('app\Compositor','id','pais_participante_id');
 //  }
	public function orderByCountry(){
      return PaisParticipante::orderBy("country")->get();

    }
}
