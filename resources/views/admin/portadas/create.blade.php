

<!-- Modal -->
  <div class="modal fade" id="ModalPortadaCreate" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Nuevo Portada</h4>
        </div>
        <div class="modal-body">
          @include("admin.portadas.form",['portada'=>$portada,'url' => '/portadas','method'=>'POST'])
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>


 <script type="text/javascript">	

 </script>