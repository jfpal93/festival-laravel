<!--FOrmulario-->
{!! Form::open(['url'=>$url,'method'=>$method, 'files'=>true]) !!} 
{!! csrf_field() !!}
	<h3>{{$portada->name}}</h3>
	
	<h4>Imagen</h4>
	<div class="form-group">
		{{ Form::file('cover',[])}}
	</div>
	
	<div class="form-group text-right">
		<input type="submit" value="Guardar" class="btn btn-success">
	</div>

{!! Form::close() !!}