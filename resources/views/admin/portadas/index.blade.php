<div class="row">
        <div class="col-md-12">

            <div class="white-box">
		            <div class="comment-body">
		            	<h3>Portadas</h3> 
						
						@foreach ($portadas as $portada)
						
					    <div class="mail-contnet">
					        <h5>{{$portada->name}}</h5> 
					    	@if($portada->extension)
								<img src="{{url("/portadas/images/portadas/$portada->id.$portada->extension")}}" class="img-circle" width="10%" height="auto">
							@endif
					        <span class="time pull-right">
					        	<a href="" data-toggle="modal" data-target="#ModalSedeEdit{{$portada->id}}">Editar</a >
					        </span>
					        
					    </div>
					    	@include("admin.portadas.edit",["portada"=>$portada])
					    @endforeach
					</div>
                

            </div>
        </div>
    </div>




