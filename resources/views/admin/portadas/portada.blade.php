<div class="hovereffect1 w3ls_banner_bottom_grid">
    <h4>Imagen principal</h4>
    <div class="user-img"> 
        @if($portada->extension)
            <img src="{{url("/portadas/images/portadas/principal-$portada->id.$portada->extension")}}" class="img-circle" width="10%" height="auto">
        @endif
    </div>
    <div class="overlay">
    	<h3>Sede</h3>
        <p>{{$portada->name}}</p>
    </div>
</div>