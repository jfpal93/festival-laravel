
    <div class="row">
        <div class="col-md-12">

            <div class="white-box">
		            <div class="comment-body">
		            	<h1>Jurado</h1>
		            	<div class="container">
							<table class="table table-bordered">
								<thead>
									<tr>
										<td>Nombre</td>
										<td>Descripción corta</td>
										<td>Pais</td>
										<td>Tipo</td>
									</tr>
								</thead>
								
								<tbody>
									@foreach ($jurados as $jurado)
										<tr>

											<td>{{$jurado->name}}</td>
											<td><span class="mail-desc" style="display: block; width: 100px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{{$jurado->descripcion_corta}}</span></td>
											
											<td style="text-transform: capitalize;">
												@if($jurado->country === 'peru')
					                            Perú

					                            @elseif($jurado->country === 'puertorico')
					                            Puerto Rico

					                            @elseif($jurado->country === 'elsalvador')
					                            El Salvador

					                            @elseif($jurado->country === 'estadosunidos')
					                            Estados Unidos

					                            @elseif($jurado->country === 'espana')
					                            España

					                            @elseif($jurado->country === 'repdom')
					                            República Dominicana

					                            @elseif($jurado->country === 'sierraleona')
					                            Sierra Leona

					                            @elseif($jurado->country === 'haiti')
					                            Haití

					                            @elseif($jurado->country === 'mexico')
					                            México

					                            @elseif($jurado->country === 'panama')
					                            Panamá

					                            @else


					                            {{$jurado->country}} 
					                            @endif

											</td>
											<td>
												@if($jurado->tipo==='participante')
												Canción
												@else
												Videoclip
												@endif
												</td>
											
											<td> 
												<a href="" data-toggle="modal" data-target="#ModalJuradoShow{{$jurado->id}}">Ver</a >
					        					<a href="" data-toggle="modal" data-target="#ModalJuradoEdit{{$jurado->id}}">Editar</a >
												@include("admin.jurados.edit",["jurado"=>$jurado])

												@include("admin.jurados.show",["jurado"=>$jurado])
												@include('admin.jurados.delete',['jurado'=>$jurado])
												
											</td>
											
											
										</tr>
										
									@endforeach
									
								</tbody>

							</table>
						</div>
						<div class="floating">
						<a class="btn btn-primary btn-fab" href="" data-toggle="modal" onclick="emptyModalNewJury()" data-target="#ModalJuradoCreate">
							<i class="material-icons">add</i>
								</a >
						</div>
					</div>
                

            </div>
        </div>
    </div>
	


@include("admin.jurados.create",["jurado"=>$jurado])

<script type="text/javascript">
	function GetElementInsideContainer(containerID, childID) {
	    var elm = {};
	    var elms = document.getElementById(containerID).getElementsByTagName("*");
	    for (var i = 0; i < elms.length; i++) {
	        if (elms[i].id === childID) {
	            elm = elms[i];
	            break;
	        }
	    }
	    return elm;
	}
	function emptyModalNewJury(){
		var e = GetElementInsideContainer("ModalJuradoCreate", "nameJury");
		e.value="";
		var l = GetElementInsideContainer("ModalJuradoCreate", "shortJury");
		l.value="";
		var c = GetElementInsideContainer("ModalJuradoCreate", "countJury");
		c.value="null";
		var c2 = GetElementInsideContainer("ModalJuradoCreate", "typeJury");
		c2.value="null";
		var ni = GetElementInsideContainer("ModalJuradoCreate", "descJury");
		ni.value="";
	}
</script>



