<!--FOrmulario-->
{!! Form::open(['url'=>$url,'method'=>$method, 'files'=>true]) !!} 
{!! csrf_field() !!}

	<div class="form-group">
		
		{{ Form::text('name',$jurado->name,['class'=>'form-control', 'placeholder'=>'Nombre de Jurado','required' => 'required','id'=>'nameJury'])}}
	</div>
	<div class="form-group">
		{{ Form::file('cover',[])}}
	</div>

	<h4>País</h4>
	<div class="form-group">
		{{ Form::select('country',[null=>'- Seleccionar Pais -',
		'argentina'=>'Argentina',
		'bolivia'=>'Bolivia',
		'brasil'=>'Brasil',
		'chile'=>'Chile',
		'colombia'=>'Colombia',
		'ecuador'=>'Ecuador',
		'elsalvador'=>'El Salvador',
		'espana'=>'España',
		'estadosunidos'=>'Estados Unidos',
		'guatemala'=>'Guatemala',
		'haiti'=>'Haití',
		'italia'=>'Italia',
		'mexico'=>'México',
		'nicaragua'=>'Nicaragua',
		'panama'=>'Panamá',
		'paraguay'=>'Paraguay',
		'peru'=>'Perú',
		'puertorico'=>'Puerto Rico',
		'repdom'=>'República Dominicana',
		'sierraleona'=>'Sierra Leona',
		'uruguay'=>'Uruguay',
		'venezuela'=>'Venezuela'

		],$jurado->country,['required' => 'required','id'=>'countJury'])}}
	</div>

	<h4>Tipo</h4>
	<div class="form-group">
		{{ Form::select('tipo',[null=>'- Tipo -',
		'participante'=>'Canción',
		'video'=>'VideoClip'
		

		],$jurado->tipo,['required' => 'required','id'=>'typeJury'])}}
	</div>
	<div class="form-group">
		
		{{ Form::textarea('short',$jurado->descripcion_corta,['class'=>'form-control', 'placeholder'=>'Biografía corta','rows' => 4,'id'=>'shortJury'])}}
	</div>
	<div class="form-group">
		{{ Form::textarea('description',$jurado->description,['class'=>'form-control', 'placeholder'=>'Biografía','rows' => 4,'id'=>'descJury'])}}
		
	</div>
	<div class="form-group text-right">
		<input type="submit" value="Enviar" class="btn btn-success">
	</div>

{!! Form::close() !!}