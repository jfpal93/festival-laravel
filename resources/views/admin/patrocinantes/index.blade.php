


<div class="row">
        <div class="col-md-12">

            <div class="white-box">
		            <div class="comment-body">
		            	<h3>Patrocinantes</h3> 
						
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<td>logo</td>
										<td>Nombre</td>
										
									</tr>
								</thead>
								
								<tbody>
									@foreach ($patrocinantes as $patrocinante)
										<tr>
											<td >
												@if($patrocinante->extension)
													<img src="{{url("/patrocinantes/images/patrocinantes/$patrocinante->id.$patrocinante->extension")}}" class="img-circle" width="10%" height="auto">
												@endif

											</td>
										
											<td >{{$patrocinante->name}}</td>
											
											
											<td> 
												<a href="" data-toggle="modal" data-target="#ModalPatrocinanteShow{{$patrocinante->id}}">Ver</a >
					        					<a href="" data-toggle="modal" data-target="#ModalPatrocinanteEdit{{$patrocinante->id}}">Editar</a >


												
												@include("admin.patrocinantes.edit",["patrocinante"=>$patrocinante])

												@include("admin.patrocinantes.show",["patrocinante"=>$patrocinante])
												@include('admin.patrocinantes.delete',['patrocinante'=>$patrocinante])
											</td>
											
										</tr>
										
									@endforeach
									
								</tbody>

							</table>
						</div>
					    <div class="floating">
							<a class="btn btn-primary btn-fab" href="" data-toggle="modal" onclick="emptyModalNewPatrocinante()" data-target="#ModalPatrocinanteCreate">	
							<i class="material-icons">add</i>
								</a >
						</div>
					</div>
                

            </div>
        </div>
    </div>
@include("admin.patrocinantes.create",["patrocinantes"=>$patrocinantes])

<script type="text/javascript">
	function GetElementInsideContainer(containerID, childID) {
	    var elm = {};
	    var elms = document.getElementById(containerID).getElementsByTagName("*");
	    for (var i = 0; i < elms.length; i++) {
	        if (elms[i].id === childID) {
	            elm = elms[i];
	            break;
	        }
	    }
	    return elm;
	}
	function emptyModalNewPatrocinante(){
		var e = GetElementInsideContainer("ModalPatrocinanteCreate", "namePatrocinante");
		e.value="";
	}



</script>