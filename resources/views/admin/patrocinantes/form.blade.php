<!--FOrmulario-->
{!! Form::open(['url'=>$url,'method'=>$method, 'files'=>true]) !!} 
{!! csrf_field() !!}
	<h3>Nombre Patrocinante</h3>
	<div class="form-group">
		
		{{ Form::text('name',$patrocinante->name,['class'=>'form-control', 'placeholder'=>'Nombre patrocinante','required' => 'required',"id"=>"namePatrocinante"])}}
	</div
>
	<div class="form-group">
		{{ Form::file('cover',[])}}
	</div>
	
	
	<div class="form-group text-right">
		<input type="submit" value="Guardar" class="btn btn-success">
	</div>

{!! Form::close() !!}