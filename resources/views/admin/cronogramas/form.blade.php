<!--FOrmulario-->
{!! Form::open(['url'=>$url,'method'=>$method, 'files'=>true]) !!} 
{!! csrf_field() !!}
	<div class="form-group">
		{{ Form::text('fecha',$cronograma->fecha,['class'=>'form-control', 'placeholder'=>'Fecha','required' => 'required'])}}
		
	</div>
	
	<div class="form-group">
		
		{{ Form::text('lugar',$cronograma->lugar,['class'=>'form-control', 'placeholder'=>'Lugar','required' => 'required'])}}
	</div>

	<div class="form-group">
		
		{{ Form::text('evento',$cronograma->evento,['class'=>'form-control', 'placeholder'=>'Evento','required' => 'required'])}}
	</div>
	
	<div class="form-group text-right">

		<input type="submit" value="Enviar" class="btn btn-success">
	</div>

{!! Form::close() !!}