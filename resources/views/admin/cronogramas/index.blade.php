
<div class="row">
        <div class="col-md-12">

            <div class="white-box">
		            <div class="comment-body">
		            	<h3>Cronograma</h3> 
						
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<td>Fecha</td>
										<td>Lugar</td>
										<td>Evento</td>
									</tr>
								</thead>
								
								<tbody>
									@foreach ($cronogramas as $cronograma)
										<tr>
											<td class="txt-oflo">{{$cronograma->fecha}}</td>
											<td class="txt-oflo">{{$cronograma->lugar}}</td>
											<td class="txt-oflo">{{$cronograma->evento}}</td>
											
											<td> 
												<a href="" data-toggle="modal" data-target="#ModalCronogramaShow{{$cronograma->id}}">Ver</a >
					        					<a href="" data-toggle="modal" data-target="#ModalCronogramaEdit{{$cronograma->id}}">Editar</a >
												@include("admin.cronogramas.edit",["cronograma"=>$cronograma])

												@include("admin.cronogramas.show",["cronograma"=>$cronograma])
												@include('admin.cronogramas.delete',['cronograma'=>$cronograma])
											</td>
											
										</tr>
										
									@endforeach
									
								</tbody>

							</table>
						</div>
					    <div class="floating">
							<a href="{{url('/cronogramas/create')}}" class="btn btn-primary btn-fab">
								
								<i class="material-icons">add</i>
							</a>
						</div>
					</div>
                

            </div>
        </div>
    </div>

     @include("admin.cronogramas.create",["cronogramas"=>$cronogramas])