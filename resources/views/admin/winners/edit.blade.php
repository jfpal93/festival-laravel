
<!-- Modal -->
  <div class="modal fade" id="ModalWinnerEdit" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      
        @include('admin.winners.form',['winner'=>$winner,'url' => '/winners/'.$winner->id,'method'=>'PATCH'])

         
      
    </div>
  </div>