FOrmulario-->
{!! Form::open(['url'=>$url,'method'=>$method, 'files'=>true]) !!} 
{!! csrf_field() !!}

<!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">Editar</h4>

    </div>
    <div class="modal-body">
    	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
    		Activar Modal Ganadores
	    	<label class="switch" style="margin-bottom: -2px !important;">
			  <!-- <input type="checkbox"> -->
			  
			  @if($winner->enabled===0)
			  	{{Form::hidden('enabled',0)}}
				{{Form::checkbox('enabled')}}

				@else

				{{Form::hidden('enabled',0)}}
				{{Form::checkbox('enabled',1,true)}}


			  @endif

				


			  <span class="slider round"></span>
			</label>
		</div>
		<br>
		
		<br>
		<div class="form-group">
			
			{{ Form::text('ano',$winner->ano,['class'=>'form-control', 'placeholder'=>'Año','required' => 'required','id'=>'ano'])}}
		</div>


		<div class="form-group">
			
			{{ Form::text('titulo1',$winner->titulo1,['class'=>'form-control', 'placeholder'=>'Titulo 1','required' => 'required','id'=>'titulo1'])}}
		</div>

		<div class="form-group">
			
			{{ Form::text('link1',$winner->link1,['class'=>'form-control', 'placeholder'=>'Link Youtube','required' => 'required','id'=>'link1'])}}
		</div>


		<div class="form-group">
			
			{{ Form::text('titulo2',$winner->titulo2,['class'=>'form-control', 'placeholder'=>'Titulo 2','required' => 'required','id'=>'titulo2'])}}
		</div>

		<div class="form-group">
			
			{{ Form::text('link2',$winner->link2,['class'=>'form-control', 'placeholder'=>'Link Youtube','required' => 'required','id'=>'link2'])}}
		</div>


		<div class="form-group">
			
			{{ Form::text('titulo3',$winner->titulo3,['class'=>'form-control', 'placeholder'=>'Titulo 3','required' => 'required','id'=>'titulo3'])}}
		</div>

		<div class="form-group">
			
			{{ Form::text('link3',$winner->link3,['class'=>'form-control', 'placeholder'=>'Link Youtube','required' => 'required','id'=>'link3'])}}
		</div>
		<div class="form-group text-right">
			<input type="submit" value="Enviar" class="btn btn-success">
		</div>
	 </div>
        
         	
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>


{!! Form::close() !!}

<!-- <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">			        	
			        	<label class="switch">
						  <input type="checkbox">
						  <span class="slider round"></span>
						</label>
			        </div> 