@extends("layouts.app2")

@section("content")
<!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="fa fa-bars"></i></a>
                <!-- <div class="top-left-part"><a class="logo" href="index.html"><b><img src="plugins/images/pixeladmin-logo.png" alt="home" /></b><span class="hidden-xs"><img src="plugins/images/pixeladmin-text.png" alt="home" /></span></a></div>
                <ul class="nav navbar-top-links navbar-left m-l-20 hidden-xs">
                    <li>
                        <form role="search" class="app-search hidden-xs">
                            <input type="text" placeholder="Search..." class="form-control"> <a href=""><i class="fa fa-search"></i></a>
                        </form>
                    </li>
                </ul> -->
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li>
                    @if (Auth::guest())
                       
                    @else
                        <!-- <a class="profile-pic" href="#"><b class="hidden-xs">Admin</b> </a> -->
                        <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out  fa-fw" aria-hidden="true"></i><span class="hide-menu">Logout</span>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {!! csrf_field() !!}
                            </form>
                    @endif
                        
                    </li>
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- Left navbar-header -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                <ul class="nav" id="side-menu">
                    <li style="padding: 10px 0 0;">
                        <a href="{{url('/home')}}" class="waves-effect"><i class="fa fa-home fa-fw" aria-hidden="true"></i><span class="hide-menu">Index</span></a>
                    </li>
                    <li>
                        <a href="{{url('/historia')}}" class="waves-effect"><i class="fa fa-history fa-fw" aria-hidden="true"></i><span class="hide-menu">Historia</span></a>
                    </li>
                    <li>
                        <a href="{{url('/jury')}}" class="waves-effect"><i class="fa fa-dashboard  fa-fw" aria-hidden="true"></i><span class="hide-menu">Jurados</span></a>
                    </li>
                     <li>
                        <a href="{{url('/participantes')}}" class="waves-effect"><i class="fa fa-fort-awesome  fa-fw" aria-hidden="true"></i><span class="hide-menu">Participantes</span></a>
                    </li>
                    <li>
                        <a href="{{url('/invitados')}}" class="waves-effect"><i class="fa fa-microphone  fa-fw" aria-hidden="true"></i><span class="hide-menu">Invitados</span></a>
                    </li>
                    <li>
                        <a href="{{url('/contactform')}}" class="waves-effect"><i class="fa fa-envelope-o  fa-fw" aria-hidden="true"></i><span class="hide-menu">Contactos</span></a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Left navbar-header end -->
        <!-- Page Content -->
        <div id="page-wrapper">

			<div class="container-fluid">
			    <div class="row bg-title">
			        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			            <h4 class="page-title">Ganadores
			            	
						</h4>
			        </div>
			        <!-- <div class="col-lg-6 col-md-5 col-sm-5 col-xs-12">
			        	
			        </div>
			        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">			        	
			        	<label class="switch">
						  <input type="checkbox">
						  <span class="slider round"></span>
						</label>
			        </div> -->

			    </div>
				<div class="white-box">
                    @foreach ($winners as $winner)
                        
                        <div class="mail-contnet">
                            <h4>{{$winner->ano}}</h4>
                            <div class="col-sm-12 col-sm-4 col-md-4 col-lg-4">
                                <h5>{{$winner->titulo1}}</h5> 
                                <!-- class="col-sm-12 col-sm-4 col-md-4 col-lg-4" -->
                                <div class="cntl-image ">
                                    <div class="video-container2"><iframe width="853" height="480" src="{{$winner->link1}}" frameborder="0" allowfullscreen></iframe></div>
                                    
                                </div>
                            </div>

                            <div class="col-sm-12 col-sm-4 col-md-4 col-lg-4">
                                <h5>{{$winner->titulo2}}</h5> 
                                <div class="cntl-image ">
                                    <div class="video-container2"><iframe width="853" height="480" src="{{$winner->link2}}" frameborder="0" allowfullscreen></iframe></div>
                                    
                                </div>
                            </div>

                            <div class="col-sm-12 col-sm-4 col-md-4 col-lg-4">
                                <h5>{{$winner->titulo3}}</h5> 
                                <div class="cntl-image ">
                                    <div class="video-container2"><iframe width="853" height="480" src="{{$winner->link3}}" frameborder="0" allowfullscreen></iframe></div>
                                    
                                </div>
                            </div>
                            <span class="time pull-right">
                                
                                <a href="" data-toggle="modal" data-target="#ModalWinnerEdit">Editar</a >
                            </span>
                            
                        </div>
                    @endforeach
					
				</div>
				
			<!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; machincorp.com </footer>





        </div>
	</div>


@include("admin.winners.edit",["winners"=>$winners])



@endsection