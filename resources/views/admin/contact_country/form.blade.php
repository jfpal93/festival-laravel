<!--FOrmulario-->
{!! Form::open(['url'=>$url,'method'=>$method, 'files'=>true]) !!} 
{!! csrf_field() !!}
	

	<div class="form-group">
		
		{{ Form::text('name',$contactcountry->name,['class'=>'form-control', 'placeholder'=>'Nombre','required' => 'required','id'=>'countryContactName'])}}
	</div>

	<div class="form-group">
		{{ Form::select('country',[null=>'- Seleccionar Pais -',
		'argentina'=>'Argentina',
		'bolivia'=>'Bolivia',
		'brasil'=>'Brasil',
		'chile'=>'Chile',
		'colombia'=>'Colombia',
		'ecuador'=>'Ecuador',
		'elsalvador'=>'El Salvador',
		'espana'=>'España',
		'estadosunidos'=>'Estados Unidos',
		'guatemala'=>'Guatemala',
		'haiti'=>'Haití',
		'italia'=>'Italia',
		'mexico'=>'México',
		'nicaragua'=>'Nicaragua',
		'nigeria'=>'Nigeria',
		'panama'=>'Panamá',
		'paraguay'=>'Paraguay',
		'peru'=>'Perú',
		'puertorico'=>'Puerto Rico',
		'repdom'=>'República Dominicana',
		'sierraleona'=>'Sierra Leona',
		'uruguay'=>'Uruguay',
		'venezuela'=>'Venezuela'

		],$contactcountry->country,['required' => 'required','id'=>'countryName'])}}
	</div>

	<div class="form-group">
		
		{{ Form::text('mail',$contactcountry->mail,['class'=>'form-control', 'placeholder'=>'Correo','required' => 'required','id'=>'countryContactMail'])}}
	</div>
	

	<div class="form-group text-right">
		<input type="submit" value="Enviar" class="btn btn-success">
	</div>

{!! Form::close() !!}