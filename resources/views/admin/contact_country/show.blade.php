
  <div class="modal fade" id="ModalContactCountryShow{{$contactcountry->id}}" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Contacto Nacional</h4>
        </div>
		@include("admin.contact_country.contactcountry",["contactcountry"=>$contactcountry])
         	
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>