

<!-- Modal -->
  <div class="modal fade" id="ModalContactCountryEdit{{$contactcountry->id}}" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Editar</h4>
        </div>
        <div class="modal-body">
		@include('admin.contact_country.form',['contactcountry'=>$contactcountry,'url' => '/contactcountry/'.$contactcountry->id,'method'=>'PATCH'])
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>