
<div class="row">
        <div class="col-md-12">

            <div class="white-box">
		            <div class="comment-body">
		            	<h3>Contacto por país</h3> 
						
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<td>Nombre</td>
										<td>Correo</td>
										<td>Pais</td>
									</tr>
								</thead>
								
								<tbody>
									@foreach ($contactcountrys as $contactcountry)
										<tr>
											<td class="txt-oflo">{{$contactcountry->name}}</td>
											<td class="txt-oflo">{{$contactcountry->mail}}</td>
											<td class="txt-oflo">{{$contactcountry->country}}</td>
											
											<td> 
												<a href="" data-toggle="modal" data-target="#ModalContactCountryShow{{$contactcountry->id}}">Ver</a >
					        					<a href="" data-toggle="modal" data-target="#ModalContactCountryEdit{{$contactcountry->id}}">Editar</a >
												@include("admin.contact_country.edit",["contactcountry"=>$contactcountry])

												@include("admin.contact_country.show",["contactcountry"=>$contactcountry])
												@include('admin.contact_country.delete',['contactcountry'=>$contactcountry])
											</td>
											
										</tr>
										
									@endforeach
									
								</tbody>

							</table>
						</div>
					    <div class="floating">
						<a class="btn btn-primary btn-fab" href="" data-toggle="modal" onclick="emptyModalNewCountryContact()" data-target="#ModalContactCountryCreate">
							<i class="material-icons">add</i>
								</a >
					</div>
                

            </div>
        </div>
    </div>

    @include("admin.contact_country.create",["contactcountrys"=>$contactcountrys])

<script type="text/javascript">
	function GetElementInsideContainer(containerID, childID) {
	    var elm = {};
	    var elms = document.getElementById(containerID).getElementsByTagName("*");
	    for (var i = 0; i < elms.length; i++) {
	        if (elms[i].id === childID) {
	            elm = elms[i];
	            break;
	        }
	    }
	    return elm;
	}
	function emptyModalNewCountryContact(){
		var e = GetElementInsideContainer("ModalContactCountryCreate", "countryContactName");
		e.value="";
		var e2 = GetElementInsideContainer("ModalContactCountryCreate", "countryName");
		e2.value="null";
		var e3 = GetElementInsideContainer("ModalContactCountryCreate", "countryContactMail");
		e3.value="";
	}
</script>