<!--FOrmulario-->
{!! Form::open(['url'=>$url,'method'=>$method, 'files'=>true]) !!} 
{!! csrf_field() !!}
	<div class="form-group">
		
		{{ Form::text('name',$auspiciante->name,['class'=>'form-control', 'placeholder'=>'Nombre auspiciante','required' => 'required','id'=>'auspicianteName'])}}
	</div
>
	<div class="form-group">
		{{ Form::file('cover',['required' => 'required'])}}
	</div>
	
	
	<div class="form-group text-right">
		<!-- <a href="{{url('/home')}}">Regresar</a> -->

		<input type="submit" value="Enviar" class="btn btn-success">
	</div>

{!! Form::close() !!}