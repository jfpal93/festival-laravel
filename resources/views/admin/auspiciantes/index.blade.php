


<div class="row">
        <div class="col-md-12">

            <div class="white-box">
		            <div class="comment-body">
		            	<h3>Auspiciantes</h3> 
						
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<td>logo</td>
										<td>Nombre</td>
										
									</tr>
								</thead>
								
								<tbody>
									@foreach ($auspiciantes as $auspiciante)
										<tr>
											<td >
												@if($auspiciante->extension)
													<img src="{{url("/auspiciantes/images/auspiciantes/$auspiciante->id.$auspiciante->extension")}}" class="img-circle" width="10%" height="auto">
												@endif

											</td>
										
											<td >{{$auspiciante->name}}</td>
											
											
											<td> 
												<!-- <a href="{{url('/auspiciantes/'.$auspiciante->id.'/edit')}}">
													Editar
												</a> -->
												<a href="" data-toggle="modal" data-target="#ModalAuspicianteShow{{$auspiciante->id}}">Ver</a >
					        					<a href="" data-toggle="modal" data-target="#ModalAuspicianteEdit{{$auspiciante->id}}">Editar</a >
												@include("admin.auspiciantes.edit",["auspiciante"=>$auspiciante])

												@include("admin.auspiciantes.show",["auspiciante"=>$auspiciante])
												@include('admin.auspiciantes.delete',['auspiciante'=>$auspiciante])
											</td>
											
										</tr>
										
									@endforeach
									
								</tbody>

							</table>
						</div>
					    <div class="floating">
							<a class="btn btn-primary btn-fab" href="" data-toggle="modal" onclick="emptyModalNewAuspiciante()" data-target="#ModalAuspicianteCreate">
							<i class="material-icons">add</i>
								</a >
						</div>
					</div>
                

            </div>
        </div>
    </div>

    @include("admin.auspiciantes.create",["auspiciantes"=>$auspiciantes])

    <script type="text/javascript">
	function GetElementInsideContainer(containerID, childID) {
	    var elm = {};
	    var elms = document.getElementById(containerID).getElementsByTagName("*");
	    for (var i = 0; i < elms.length; i++) {
	        if (elms[i].id === childID) {
	            elm = elms[i];
	            break;
	        }
	    }
	    return elm;
	}
	function emptyModalNewAuspiciante(){
		var e = GetElementInsideContainer("ModalAuspicianteCreate", "auspicianteName");
		e.value="";
		
	}
	</script>