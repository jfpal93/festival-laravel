@extends('layouts.app2')

@section('content')
<!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="fa fa-bars"></i></a>
                
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li>
                    @if (Auth::guest())
                       
                    @else
                        <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out  fa-fw" aria-hidden="true"></i><span class="hide-menu">Logout</span>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {!! csrf_field() !!}
                            </form>
                    @endif
                        
                    </li>
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- Left navbar-header -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                <ul class="nav" id="side-menu">
                    <li style="padding: 10px 0 0;">
                        <a href="{{url('/home')}}" class="waves-effect"><i class="fa fa-home fa-fw" aria-hidden="true"></i><span class="hide-menu">Index</span></a>
                    </li>
                    <li>
                        <a href="{{url('/historia')}}" class="waves-effect"><i class="fa fa-history fa-fw" aria-hidden="true"></i><span class="hide-menu">Historia</span></a>
                    </li>
                    <li>
                        <a href="{{url('/jury')}}" class="waves-effect"><i class="fa fa-dashboard  fa-fw" aria-hidden="true"></i><span class="hide-menu">Jurados</span></a>
                    </li>
                     <li>
                        <a href="{{url('/participantes')}}" class="waves-effect"><i class="fa fa-fort-awesome  fa-fw" aria-hidden="true"></i><span class="hide-menu">Participantes</span></a>
                    </li>
                    <li>
                        <a href="{{url('/invitados')}}" class="waves-effect"><i class="fa fa-microphone  fa-fw" aria-hidden="true"></i><span class="hide-menu">Invitados</span></a>
                    </li>
                    <li>
                        <a href="{{url('/contactform')}}" class="waves-effect"><i class="fa fa-envelope-o  fa-fw" aria-hidden="true"></i><span class="hide-menu">Contactos</span></a>
                    </li>
                    <li >
                        <a href="{{url('/winners')}}" class="waves-effect"><i class="fa fa-trophy  fa-fw" aria-hidden="true"></i><span class="hide-menu">Ganadores</span></a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Left navbar-header end -->
        <!-- Page Content -->
        <div id="page-wrapper">
           


			<div class="container-fluid">
			    <div class="row bg-title">
			        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			            <h4 class="page-title">Historia</h4>
			        </div>
			    </div>
			    
			    @include("admin.galas.index",["galas"=>$galas])
			    
			    
			</div>
<!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; machincorp.com </footer>





        </div>
        <!-- /#page-wrapper -->

@endsection

