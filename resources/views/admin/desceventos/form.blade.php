<!--FOrmulario-->
{!! Form::open(['url'=>$url,'method'=>$method, 'files'=>true]) !!}
{!! csrf_field() !!} 
	<div class="form-group">
		
		{{ Form::text('title',$descevento->title,['class'=>'form-control', 'placeholder'=>'Titulo','required' => 'required'])}}
	</div>
	

	<div class="form-group">
		{{ Form::textarea('texto1',$descevento->texto1,['class'=>'form-control', 'placeholder'=>'Texto 1','required' => 'required', 'rows' => 4])}}
		
	</div>

	<div class="form-group">
		{{ Form::textarea('texto2',$descevento->texto2,['class'=>'form-control', 'placeholder'=>'Texto 2','required' => 'required'])}}
		
	</div>

	<div class="form-group">
		{{ Form::file('cover',['required' => 'required'])}}
	</div>

	<div class="form-group text-right">
		<input type="submit" value="Guardar" class="btn btn-success">
	</div>

{!! Form::close() !!}