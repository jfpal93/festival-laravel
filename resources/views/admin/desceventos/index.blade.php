
<div class="row">
        <div class="col-md-12">

            <div class="white-box">
		            <div class="comment-body">
		            	<h3>Descripción Evento</h3> 
						
						@foreach ($desceventos as $descevento)
						<div class="user-img"> 
							@if($descevento->extension)
								<img src="{{url("/desceventos/images/desceventos/$descevento->id.$descevento->extension")}}" class="img-circle" width="10%" height="auto">
							@endif
						</div>
					    <div class="mail-contnet">
					        <h5>Titulo</h5> 
					    	<span class="mail-desc">{{$descevento->title}}</span>
					    	<h5>Texto 1</h5> 
					    	<span class="mail-desc">{{$descevento->texto1}}</span>
					    	<h5>Texto 2</h5> 
					    	<span class="mail-desc">{{$descevento->texto1}}</span>
					        <span class="time pull-right">
								<a href="" data-toggle="modal" data-target="#ModalDesceventoShow">Ver</a >
					        	<a href="" data-toggle="modal" data-target="#ModalDesceventoEdit">Editar</a >

					        </span>
					        
					    </div>
					    @endforeach
					</div>
                

            </div>
        </div>
    </div>


@include("admin.desceventos.edit",["desceventos"=>$desceventos])
@include("admin.desceventos.show",["desceventos"=>$desceventos])