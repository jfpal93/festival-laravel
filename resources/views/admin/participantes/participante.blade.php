
<div class="modal-body">
    
        <div class="video-container"><iframe width="853" height="480" src="{{$participante->link}}" frameborder="0" allowfullscreen></iframe></div>
                        
        <br>
        <h4>{{$participante->song}}</a></h4>
        <p class="capitalize">{{$participante->country}} - &#160; <img id="img" src="images/banderaP/{{$participante->country}}.png"> </p>
        </div>
        <div class="modal-body">
            <div class="col-md-12 w3_modal_body_right">
                <h4>Autor</h4>
                @if($participante->autor_extension)
                <img src="{{url("/participantes/images/participantes/autor-$participante->id-$participante->country.$participante->autor_extension")}}" class="img-responsive">
            @endif
                <p><i>{{$participante->autor_name}}</i>{{$participante->autor_description}}</p>
            </div>
            
            
         </div>

         <div class="modal-body">
                <div class="col-md-12 w3_modal_body_right">
                    <h4>Compositor</h4>
                    @if($participante->compositor_extension)
                        <img src="{{url("/participantes/images/participantes/compositor-$participante->id-$participante->country.$participante->compositor_extension")}}" class="img-responsive">
                    @endif
                    <p><i>{{$participante->compositor_name}}</i>{{$participante->compositor_description}}</p>
                </div>
                
                
            </div>


            <div class="modal-body">
                <div class="col-md-12 w3_modal_body_right">
                    <h4>Intérprete</h4>
                    @if($participante->interprete_extension)
                        <img src="{{url("/participantes/images/participantes/interprete-$participante->id-$participante->country.$participante->interprete_extension")}}" class="img-responsive">
                    @endif
                    <p><i>{{$participante->interprete_name}}</i>{{$participante->interprete_description}}</p>
                    <div class="col-md-12 w3_modal_body_right">
                    @if($participante->facebook)
                    <p>Facebook: {{$participante->facebook}}</p>
                    @endif
                    @if($participante->twitter)
                    <p>Twitter: {{$participante->twitter}}</p>
                    @endif
                    @if($participante->instagram)
                    <p>Instagram: {{$participante->instagram}}</p>
                    @endif
                    @if($participante->youtube)
                    <p>YouTube: {{$participante->youtube}}</p>
                    @endif
                    @if($participante->web)
                    <p>Website: {{$participante->web}}</p>
                    @endif
                    @if($participante->social6)
                    <p>Otro: {{$participante->social6}}</p>
                    @endif
                    </div>
                </div>

                
            </div>