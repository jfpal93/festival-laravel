@extends("layouts.app2")

@section("content")
<!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="fa fa-bars"></i></a>
                
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li>
                    @if (Auth::guest())
                       
                    @else
                        <!-- <a class="profile-pic" href="#"><b class="hidden-xs">Admin</b> </a> -->
                        <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out  fa-fw" aria-hidden="true"></i><span class="hide-menu">Logout</span>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {!! csrf_field() !!}
                            </form>
                    @endif
                        
                    </li>
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- Left navbar-header -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                <ul class="nav" id="side-menu">
                    <li style="padding: 10px 0 0;">
                        <a href="{{url('/home')}}" class="waves-effect"><i class="fa fa-home fa-fw" aria-hidden="true"></i><span class="hide-menu">Index</span></a>
                    </li>
                    <li>
                        <a href="{{url('/historia')}}" class="waves-effect"><i class="fa fa-history fa-fw" aria-hidden="true"></i><span class="hide-menu">Historia</span></a>
                    </li>
                    <li>
                        <a href="{{url('/jury')}}" class="waves-effect"><i class="fa fa-dashboard  fa-fw" aria-hidden="true"></i><span class="hide-menu">Jurados</span></a>
                    </li>
                     <li>
                        <a href="{{url('/participantes')}}" class="waves-effect"><i class="fa fa-fort-awesome  fa-fw" aria-hidden="true"></i><span class="hide-menu">Participantes</span></a>
                    </li>
                    <li>
                        <a href="{{url('/invitados')}}" class="waves-effect"><i class="fa fa-microphone  fa-fw" aria-hidden="true"></i><span class="hide-menu">Invitados</span></a>
                    </li>
                    <li>
                        <a href="{{url('/contactform')}}" class="waves-effect"><i class="fa fa-envelope-o  fa-fw" aria-hidden="true"></i><span class="hide-menu">Contactos</span></a>
                    </li>
                    <li >
                        <a href="{{url('/winners')}}" class="waves-effect"><i class="fa fa-trophy  fa-fw" aria-hidden="true"></i><span class="hide-menu">Ganadores</span></a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Left navbar-header end -->
        <!-- Page Content -->
        <div id="page-wrapper">
	<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Países Participantes</h4>
        </div>
    </div>
	<div class="white-box">

		<table class="table">
			<thead>
				<tr>
					<td>Número</td>
					<td>Canción</td>
					<td>Pais</td>
					<td >Intérprete</td>
					<td >Autor</td>
					<td >Compositor</td>
				</tr>
			</thead>
			
			<tbody>
				@foreach ($participantes as $index=>$participante)
					<tr>
						<td>{{$index + 1}}</td>
						<td >{{$participante->song}}</td>
						<td style="text-transform: capitalize;">
							@if($participante->country === 'peru')
                            Perú

                            @elseif($participante->country === 'puertorico')
                            Puerto Rico

                            @elseif($participante->country === 'elsalvador')
                            El Salvador

                            @elseif($participante->country === 'estadosunidos')
                            Estados Unidos

                            @elseif($participante->country === 'espana')
                            España

                            @elseif($participante->country === 'repdom')
                            República Dominicana

                            @elseif($participante->country === 'sierraleona')
                            Sierra Leona

                            @elseif($participante->country === 'haiti')
                            Haití

                            @elseif($participante->country === 'mexico')
                            México

                            @elseif($participante->country === 'panama')
                            Panamá

                            @else


                            {{$participante->country}} 
                            @endif

							</td>
						<td>
							{{$participante->interprete_name}}
						</td>
						<td>
							{{$participante->autor_name}}
						</td>
						<td>
							{{$participante->compositor_name}}
							
						</td>
						

						

						

						
						<td> 
							<a href="" data-toggle="modal" data-target="#ModalParticipanteShow{{$participante->id}}">Ver</a >
        					<a href="" data-toggle="modal" data-target="#ModalParticipanteEdit{{$participante->id}}">Editar</a >
							@include("admin.participantes.edit",["participante"=>$participante])

							@include("admin.participantes.show",["participante"=>$participante])
							@include('admin.participantes.delete',['participante'=>$participante])
						</td>
						
					</tr>
					
				@endforeach
				
			</tbody>

		</table>
	</div>

	<div class="floating">
		<a class="btn btn-primary btn-fab" href="" data-toggle="modal" onclick="emptyModalNewParticipante()" data-target="#ModalParticipanteCreate">
							<i class="material-icons">add</i>
								</a >
	</div>

	<!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; machincorp.com </footer>





        </div>
        <!-- /#page-wrapper -->
        @include("admin.participantes.create",["participantes"=>$participantes])

<script type="text/javascript">
	function GetElementInsideContainer(containerID, childID) {
	    var elm = {};
	    var elms = document.getElementById(containerID).getElementsByTagName("*");
	    for (var i = 0; i < elms.length; i++) {
	        if (elms[i].id === childID) {
	            elm = elms[i];
	            break;
	        }
	    }
	    return elm;
	}
	function emptyModalNewParticipante(){
		var e = GetElementInsideContainer("ModalParticipanteCreate", "songPart");
		e.value="";
		var l = GetElementInsideContainer("ModalParticipanteCreate", "linkPart");
		l.value="";
		var c = GetElementInsideContainer("ModalParticipanteCreate", "countryPart");
		c.value="null";
		var ni = GetElementInsideContainer("ModalParticipanteCreate", "nomIntPart");
		ni.value="";
		var di = GetElementInsideContainer("ModalParticipanteCreate", "descIntPart");
		di.value="";
		var na = GetElementInsideContainer("ModalParticipanteCreate", "nomAutPart");
		na.value="";
		var da = GetElementInsideContainer("ModalParticipanteCreate", "descAutPart");
		da.value="";
		var nc = GetElementInsideContainer("ModalParticipanteCreate", "nomCompPart");
		nc.value="";
		var dc = GetElementInsideContainer("ModalParticipanteCreate", "descCompPart");
		dc.value="";
		var fb = GetElementInsideContainer("ModalParticipanteCreate", "linkFace");
		fb.value="";
		var twt = GetElementInsideContainer("ModalParticipanteCreate", "linkTwt");
		twt.value="";
		var insta = GetElementInsideContainer("ModalParticipanteCreate", "linkInsta");
		insta.value="";
		var yt = GetElementInsideContainer("ModalParticipanteCreate", "linkYou");
		yt.value="";
		var web = GetElementInsideContainer("ModalParticipanteCreate", "linkWeb");
		web.value="";
		var sc = GetElementInsideContainer("ModalParticipanteCreate", "linkSocial");
		sc.value="";
	}
	</script>

@endsection

