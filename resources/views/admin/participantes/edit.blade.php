
<!-- Modal -->
  <div class="modal fade" id="ModalParticipanteEdit{{$participante->id}}" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Editar Pais Participante</h4>
        </div>
        <div class="modal-body">
         	@include('admin.participantes.form',['participante'=>$participante,'url' => '/participantes/'.$participante->id,'method'=>'PATCH'])
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>