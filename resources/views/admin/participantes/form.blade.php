<!--FOrmulario-->
{!! Form::open(['url'=>$url,'method'=>$method, 'files'=>true]) !!} 
{!! csrf_field() !!}
	<div class="form-group">
		
		{{ Form::text('song',$participante->song,['class'=>'form-control', 'placeholder'=>'Nombre de canción','required' => 'required','id'=>'songPart'])}}
	</div>

	<div class="form-group">
		
		{{ Form::text('link',$participante->link,['class'=>'form-control', 'placeholder'=>'Link Videoclip','required' => 'required','id'=>'linkPart'])}}
	</div>

	<div class="form-group">
		
		{{ Form::text('facebook',$participante->facebook,['class'=>'form-control', 'placeholder'=>'Link Facebook','id'=>'linkFace'])}}
	</div>

	<div class="form-group">
		
		{{ Form::text('twitter',$participante->twitter,['class'=>'form-control', 'placeholder'=>'Link Twitter','id'=>'linkTwt'])}}
	</div>
	<div class="form-group">
		
		{{ Form::text('instagram',$participante->instagram,['class'=>'form-control', 'placeholder'=>'Link Intagram','id'=>'linkInsta'])}}
	</div>
	<div class="form-group">
		
		{{ Form::text('youtube',$participante->youtube,['class'=>'form-control', 'placeholder'=>'Link YouTube','id'=>'linkYou'])}}
	</div>
	<div class="form-group">
		
		{{ Form::text('web',$participante->web,['class'=>'form-control', 'placeholder'=>'Link Website','id'=>'linkWeb'])}}
	</div>
	<div class="form-group">
		
		{{ Form::text('social6',$participante->social6,['class'=>'form-control', 'placeholder'=>'Otro','id'=>'linkS6'])}}
	</div>


	<h4>País</h4>
	<div class="form-group">
		{{ Form::select('country',[null=>'- Seleccionar Pais -',
		'argentina'=>'Argentina',
		'bolivia'=>'Bolivia',
		'brasil'=>'Brasil',
		'chile'=>'Chile',
		'colombia'=>'Colombia',
		'ecuador'=>'Ecuador',
		'elsalvador'=>'El Salvador',
		'espana'=>'España',
		'estadosunidos'=>'Estados Unidos',
		'guatemala'=>'Guatemala',
		'haiti'=>'Haití',
		'italia'=>'Italia',
		'mexico'=>'México',
		'nicaragua'=>'Nicaragua',
		'nigeria'=>'Nigeria',
		'panama'=>'Panamá',
		'paraguay'=>'Paraguay',
		'peru'=>'Perú',
		'puertorico'=>'Puerto Rico',
		'repdom'=>'República Dominicana',
		'sierraleona'=>'Sierra Leona',
		'uruguay'=>'Uruguay',
		'venezuela'=>'Venezuela'

		],$participante->country,['required' => 'required','id'=>'countryPart'])}}
	</div>

	<h4>Intérprete</h4>
	<div class="form-group">

		{{ Form::file('cover',[])}}
	</div>

	<div class="form-group">
		
		{{ Form::text('interprete_name',$participante->interprete_name,['class'=>'form-control', 'placeholder'=>'Nombre Intérprete','required' => 'required','id'=>'nomIntPart'])}}
	</div>

	<div class="form-group">
		{{ Form::textarea('interprete_description',$participante->interprete_description,['class'=>'form-control', 'placeholder'=>'Descripción Intérprete','rows' => 4,'id'=>'descIntPart'])}}
		
	</div>

	<h4>Autor</h4>
	<div class="form-group">

		{{ Form::file('cover2',[])}}
	</div>

	<div class="form-group">
		
		{{ Form::text('autor_name',$participante->autor_name,['class'=>'form-control', 'placeholder'=>'Nombre Autor','required' => 'required','id'=>'nomAutPart'])}}
	</div>

	<div class="form-group">
		{{ Form::textarea('autor_description',$participante->autor_description,['class'=>'form-control', 'placeholder'=>'Descripción Autor','id'=>'descAutPart','required' => 'required','rows' => 4])}}
		
	</div>

	<h4>Compositor</h4>
	<div class="form-group">

		{{ Form::file('cover3',[])}}
	</div>

	<div class="form-group">
		
		{{ Form::text('compositor_name',$participante->compositor_name,['class'=>'form-control', 'placeholder'=>'Nombre Compositor','required' => 'required','id'=>'nomCompPart'])}}
	</div>

	<div class="form-group">
		{{ Form::textarea('compositor_description',$participante->compositor_description,['class'=>'form-control', 'placeholder'=>'Descripción Compositor','required' => 'required','rows' => 4,'id'=>"descCompPart"])}}
		
	</div>
	<div class="form-group text-right">

		<input type="submit" value="Enviar" class="btn btn-success">
	</div>

{!! Form::close() !!}