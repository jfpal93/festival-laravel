

<!-- Modal -->
  <div class="modal fade" id="ModalParticipanteShow{{$participante->id}}" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Pais Participante</h4>
        </div>
        @include("admin.participantes.participante",["participante"=>$participante])
         	
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>