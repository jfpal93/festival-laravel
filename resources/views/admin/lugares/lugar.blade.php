<div class="hovereffect1 w3ls_banner_bottom_grid">
	<h4>Imagen principal</h4>

	<div class="user-img"> 
    <img src="{{url("/lugares/images/lugares/principal-$lugar->id.$lugar->extension")}}" alt=" " class="img-responsive" />
    </div>
    <h4>Anexo 1</h4>
    <div class="user-img"> 
        @if($lugar->extension2)
            <img src="{{url("/lugares/images/lugares/modal1-$lugar->id.$lugar->extension2")}}" class="img-circle" width="10%" height="auto">
        @endif
    </div>
    <h4>Anexo 2</h4>
    <div class="user-img"> 
        @if($lugar->extension3)
            <img src="{{url("/lugares/images/lugares/modal2-$lugar->id.$lugar->extension3")}}" class="img-circle" width="10%" height="auto">
        @endif
    </div>
    <div class="overlay">
        <h4>{{$lugar->name}}</h4>
        <h4>{{$lugar->description}}</h4>
    </div>
</div>