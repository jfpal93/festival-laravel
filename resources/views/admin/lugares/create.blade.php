@extends("layouts.app2")

@section("content")

	<div class="container white">
		<h1>Nuevo Lugar</h1>
		@include("admin.lugares.form",['lugar'=>$lugar,'url' => '/lugares','method'=>'POST'])
	</div>



@endsection