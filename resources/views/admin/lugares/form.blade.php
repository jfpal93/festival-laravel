<!--FOrmulario-->
{!! Form::open(['url'=>$url,'method'=>$method, 'files'=>true]) !!} 
{!! csrf_field() !!}
	<div class="form-group">
		
		{{ Form::text('name',$lugar->name,['class'=>'form-control', 'placeholder'=>'Nombre','required' => 'required'])}}
	</div>
	<h4>Imagen principal</h4>
	<div class="form-group">
		{{ Form::file('cover',[])}}
	</div>
	<h4>Anexo 1</h4>
	<div class="form-group">
		{{ Form::file('cover2',[])}}
	</div>
	<h4>Anexo 2</h4>
	<div class="form-group">
		{{ Form::file('cover3',[])}}
	</div>
	<div class="form-group">
		{{ Form::textarea('short',$lugar->descripcion_corta,['class'=>'form-control', 'placeholder'=>'Descripción corta','required' => 'required','rows' => 4])}}
		
	</div>
	<div class="form-group">
		{{ Form::textarea('description',$lugar->description,['class'=>'form-control', 'placeholder'=>'Descripción','required' => 'required','rows' => 4])}}
		
	</div>
	<div class="form-group text-right">

		<input type="submit" value="Guardar" class="btn btn-success">
	</div>

{!! Form::close() !!}