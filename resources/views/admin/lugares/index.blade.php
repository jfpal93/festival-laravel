<div class="row">
        <div class="col-md-12">

            <div class="white-box">
		            <div class="comment-body">
		            	<h3>Lugar</h3> 
						
						@foreach ($lugares as $lugar)
					    <div class="mail-contnet">
					        <h5>Nombre</h5> 
					    	<span class="mail-desc">{{$lugar->name}}</span>
					    	<h5>Descripción Corta</h5> 
					    	<span class="mail-desc">{{$lugar->descripcion_corta}}</span>
					    	<h5>Descripción</h5> 
					    	<span class="mail-desc" style="display: block; width: 500px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{{$lugar->description}}</span>
					        <span class="time pull-right">
					        	<a href="" data-toggle="modal" data-target="#ModalLugarShow">Ver</a >
					        	<a href="" data-toggle="modal" data-target="#ModalLugarEdit">Editar</a >
					        </span>
					    </div>
					    @endforeach
					</div>
                

            </div>
        </div>
    </div>

@include("admin.lugares.edit",["lugares"=>$lugares])
@include("admin.lugares.show",["lugares"=>$lugares])