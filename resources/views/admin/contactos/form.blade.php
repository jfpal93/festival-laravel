<!--FOrmulario-->
{!! Form::open(['url'=>$url,'method'=>$method, 'files'=>true]) !!} 
{!! csrf_field() !!}
	

	<div class="form-group">
		
		{{ Form::text('name',$contacto->name,['class'=>'form-control', 'placeholder'=>'Nombre','required' => 'required'])}}
	</div>

	<div class="form-group">
		
		{{ Form::text('mail',$contacto->mail,['class'=>'form-control', 'placeholder'=>'Correo','required' => 'required'])}}
	</div>
	
	<div class="form-group">
		{{ Form::file('cover',['required' => 'required'])}}
	</div>

	<div class="form-group text-right">
		<input type="submit" value="Enviar" class="btn btn-success">
	</div>

{!! Form::close() !!}