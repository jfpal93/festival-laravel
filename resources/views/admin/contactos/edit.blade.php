@extends("layouts.app2")

@section("content")

	<div class="container white">
		<h1>Editar</h1>
		@include('admin.contactos.form',['contacto'=>$contacto,'url' => '/contactorganizacion/'.$contacto->id,'method'=>'PATCH'])
	</div>



@endsection