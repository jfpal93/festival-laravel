@extends("layouts.app2")

@section("content")

	<div class="container white">
		<h1>Nuevo Correo</h1>
		@include("admin.contactos.form",['contacto'=>$contacto,'url' => '/contactorganizacion','method'=>'POST'])
	</div>



@endsection