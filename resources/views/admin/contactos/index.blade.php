
<div class="row">
        <div class="col-md-12">

            <div class="white-box">
		            <div class="comment-body">
		            	<h3>Contacto Organización</h3> 
						
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<td>Nombre</td>
										<td>Correo</td>
									</tr>
								</thead>
								
								<tbody>
									@foreach ($contactos as $contacto)
										<tr>
											<td class="txt-oflo">{{$contacto->name}}</td>
											<td class="txt-oflo">{{$contacto->mail}}</td>
											
											<td> 
												<a href="{{url('/contactorganizacion/'.$contacto->id.'/edit')}}">
													Editar
												</a>
												@include('admin.contactos.delete',['contacto'=>$contacto])
											</td>
											
										</tr>
										
									@endforeach
									
								</tbody>

							</table>
						</div>
					    <div class="floating">
							<a href="{{url('/contactorganizacion/create')}}" class="btn btn-primary btn-fab">
								
								<i class="material-icons">add</i>
							</a>
						</div>
					</div>
                

            </div>
        </div>
    </div>