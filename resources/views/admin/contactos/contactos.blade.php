<div class="hovereffect1 w3ls_banner_bottom_grid">
@if(Auth::check() && $contacto->user_id==Auth::user()->id)
	<div class="absolute actions">
		<a href="{{url('/contactos/'.$contacto->id.'/edit')}}">
			Editar
		</a>
		@include('contactos.delete',['contacto'=>$contacto])
	</div>
@endif

    <img src="{{url("/contactos/images/contactos/$contacto->id.$contacto->extension")}}" alt=" " class="img-responsive" />
    <div class="overlay">
        <h4>{{$contacto->name}}</h4>
        <h4>{{$contacto->mail}}</h4>

    </div>
</div>