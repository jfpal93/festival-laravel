<div class="row">
        <div class="col-md-12">

            <div class="white-box">
		            <div class="comment-body">
		            	<h3>Sede</h3> 
						
						@foreach ($sedes as $sede)
						
					    <div class="mail-contnet">
					        <h5>Nombre</h5> 
					    	<span class="mail-desc">{{$sede->name}}</span>
					    	<h5>Descripción Corta</h5> 
					    	<span class="mail-desc">{{$sede->descripcion_corta}}</span>
					    	<h5>Descripción</h5> 
					    	<span class="mail-desc" style="display: block; width: 500px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{{$sede->description}}</span>
					        <span class="time pull-right">
					        	<a href="" data-toggle="modal" data-target="#ModalSedeShow">Ver</a >
					        	<a href="" data-toggle="modal" data-target="#ModalSedeEdit">Editar</a >
					        </span>
					        
					    </div>
					    @endforeach
					</div>
                

            </div>
        </div>
    </div>


@include("admin.sedes.edit",["sedes"=>$sedes])
@include("admin.sedes.show",["sedes"=>$sedes])


