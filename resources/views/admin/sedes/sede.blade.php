<div class="hovereffect1 w3ls_banner_bottom_grid">
    <h4>Imagen principal</h4>
    <div class="user-img"> 
        @if($sede->extension)
            <img src="{{url("/sedes/images/sedes/principal-$sede->id.$sede->extension")}}" class="img-circle" width="10%" height="auto">
        @endif
    </div>
    <h4>Anexo 1</h4>
    <div class="user-img"> 
        @if($sede->extension2)
            <img src="{{url("/sedes/images/sedes/modal1-$sede->id.$sede->extension2")}}" class="img-circle" width="10%" height="auto">
        @endif
    </div>
    <h4>Anexo 2</h4>
    <div class="user-img"> 
        @if($sede->extension3)
            <img src="{{url("/sedes/images/sedes/modal2-$sede->id.$sede->extension3")}}" class="img-circle" width="10%" height="auto">
        @endif
    </div>
    <div class="overlay">
    	<h3>Sede</h3>
        <p>{{$sede->name}}</p>
        <br>
        <h3>Descirpcion Corta</h3>
        <p>{{$sede->descripcion_corta}}</p>
        <br>
        <h3>Descripcion Larga</h3>
        <p>{{$sede->description}}</p>
    </div>
</div>