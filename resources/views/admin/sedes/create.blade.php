@extends("layouts.app2")

@section("content")

	<div class="container white">
		<h1>Nuevo Sede</h1>
		@include("admin.sedes.form",['sede'=>$sede,'url' => '/sedes','method'=>'POST'])
	</div>



@endsection