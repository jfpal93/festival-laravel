@extends("layouts.app2")

@section("content")

	<div class="container white">
		<h1>Editar</h1>
		@include('admin.destacados.form',['destacado'=>$destacado,'url' => '/destacados/'.$destacado->id,'method'=>'PATCH'])
	</div>



@endsection