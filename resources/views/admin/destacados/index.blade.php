
<div class="row">
        <div class="col-md-12">

            <div class="white-box">
		            <div class="comment-body">
		            	<h3>Destacados</h3> 
						
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<td>mes</td>
										<td>ano</td>
										<td>link</td>
									</tr>
								</thead>
								
								<tbody>
									@foreach ($destacados as $destacado)
										<tr>
											<td class="txt-oflo">{{$destacado->mes}}</td>
											<td class="txt-oflo">{{$destacado->ano}}</td>
											<td class="txt-oflo">{{$destacado->link}}</td>
											
											<td> 
												<a href="{{url('/destacados/'.$destacado->id.'/edit')}}">
													Editar
												</a>
												@include('admin.destacados.delete',['destacado'=>$destacado])
											</td>
											
										</tr>
										
									@endforeach
									
								</tbody>

							</table>
						</div>
					    <div class="floating">
							<a href="{{url('/destacados/create')}}" class="btn btn-primary btn-fab">
								
								<i class="material-icons">add</i>
							</a>
						</div>
					</div>
                

            </div>
        </div>
    </div>