<div class="hovereffect1 w3ls_banner_bottom_grid">
@if(Auth::check() && $destacado->user_id==Auth::user()->id)
	<div class="absolute actions">
		<a href="{{url('/destacados/'.$destacado->id.'/edit')}}">
			Editar
		</a>
		@include('destacados.delete',['destacado'=>$destacado])
	</div>
@endif

    <div class="overlay">
        <h4>{{$destacado->mes}}</h4>
        <h4>{{$destacado->ano}}</h4>
        <h4>{{$destacado->link}}</h4>

        <h4>{{$destacado->horafin}}</h4>

    </div>
</div>