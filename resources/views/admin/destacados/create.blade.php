@extends("layouts.app2")

@section("content")

	<div class="container white">
		<h1>Destacados</h1>
		@include("admin.destacados.form",['destacado'=>$destacado,'url' => '/destacados','method'=>'POST'])
	</div>



@endsection