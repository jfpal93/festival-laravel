

<!-- Modal -->
  <div class="modal fade" id="ModalGalaCreate" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Galas</h4>
        </div>
        <div class="modal-body">
		@include("admin.galas.form",['gala'=>$gala,'url' => '/galas','method'=>'POST'])
         </div>
        
         	
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>