<!--FOrmulario-->
{!! Form::open(['url'=>$url,'method'=>$method, 'files'=>true]) !!} 
{!! csrf_field() !!}

	<div class="form-group">
		
		{{ Form::text('ano',$gala->ano,['class'=>'form-control', 'placeholder'=>'Año','required' => 'required','id'=>'ano'])}}
	</div>

	<div class="form-group">
		
		{{ Form::text('link',$gala->link,['class'=>'form-control', 'placeholder'=>'Link','required' => 'required','id'=>'link'])}}
	</div>

	<h3>Ganador 1</h3>
	<div class="form-group">
		{{ Form::select('country1',[null=>'- Seleccionar Pais -',
		'argentina'=>'Argentina',
		'bolivia'=>'Bolivia',
		'brasil'=>'Brasil',
		'chile'=>'Chile',
		'colombia'=>'Colombia',
		'cuba'=>'Cuba',
		'ecuador'=>'Ecuador',
		'elsalvador'=>'El Salvador',
		'espana'=>'España',
		'estadosunidos'=>'Estados Unidos',
		'guatemala'=>'Guatemala',
		'haiti'=>'Haití',
		'italia'=>'Italia',
		'mexico'=>'México',
		'nicaragua'=>'Nicaragua',
		'panama'=>'Panamá',
		'paraguay'=>'Paraguay',
		'peru'=>'Perú',
		'repdom'=>'República Dominicana',
		'sierraleona'=>'Sierra Leona',
		'uruguay'=>'Uruguay',
		'venezuela'=>'Venezuela'

		],$gala->country1,['id'=>'count1'])}}
	</div>

	<div class="form-group">
		
		{{ Form::text('titulo1',$gala->titulo1,['class'=>'form-control', 'placeholder'=>'Titulo','id'=>'titulo1'])}}
	</div>

	

	<div class="form-group">
		
		{{ Form::text('nombre1',$gala->nombre1,['class'=>'form-control', 'placeholder'=>'Nombre','id'=>'nombre1'])}}
	</div>

	<div class="form-group">
		
		{{ Form::text('cancion1',$gala->cancion1,['class'=>'form-control', 'placeholder'=>'Canción','id'=>'cancion1'])}}
	</div>
	



	<h3>Ganador 2</h3>

	<div class="form-group">
		{{ Form::select('country2',[null=>'- Seleccionar Pais -',
		'argentina'=>'Argentina',
		'bolivia'=>'Bolivia',
		'brasil'=>'Brasil',
		'chile'=>'Chile',
		'colombia'=>'Colombia',
		'ecuador'=>'Ecuador',
		'elsalvador'=>'El Salvador',
		'espana'=>'España',
		'estadosunidos'=>'Estados Unidos',
		'guatemala'=>'Guatemala',
		'haiti'=>'Haití',
		'italia'=>'Italia',
		'mexico'=>'México',
		'nicaragua'=>'Nicaragua',
		'panama'=>'Panamá',
		'paraguay'=>'Paraguay',
		'peru'=>'Perú',
		'repdom'=>'República Dominicana',
		'sierraleona'=>'Sierra Leona',
		'uruguay'=>'Uruguay',
		'venezuela'=>'Venezuela'

		],$gala->country2,['id'=>'count2'])}}
	</div>

	<div class="form-group">
		
		{{ Form::text('titulo2',$gala->titulo2,['class'=>'form-control', 'placeholder'=>'Título','id'=>'titulo3'])}}
	</div>

	<div class="form-group">
		
		{{ Form::text('nombre2',$gala->nombre2,['class'=>'form-control', 'placeholder'=>'Nombre','id'=>'nombre2'])}}
	</div>

	<div class="form-group">
		
		{{ Form::text('cancion2',$gala->cancion2,['class'=>'form-control', 'placeholder'=>'Canción','id'=>'cancion2'])}}
	</div>


	

	

	<h3>Ganador 3</h3>

	<div class="form-group">
		{{ Form::select('country3',[null=>'- Seleccionar Pais -',
		'argentina'=>'Argentina',
		'bolivia'=>'Bolivia',
		'brasil'=>'Brasil',
		'chile'=>'Chile',
		'colombia'=>'Colombia',
		'cuba'=>'Cuba',
		'ecuador'=>'Ecuador',
		'elsalvador'=>'El Salvador',
		'espana'=>'España',
		'estadosunidos'=>'Estados Unidos',
		'guatemala'=>'Guatemala',
		'haiti'=>'Haití',
		'italia'=>'Italia',
		'mexico'=>'México',
		'nicaragua'=>'Nicaragua',
		'panama'=>'Panamá',
		'paraguay'=>'Paraguay',
		'peru'=>'Perú',
		'repdom'=>'República Dominicana',
		'sierraleona'=>'Sierra Leona',
		'uruguay'=>'Uruguay',
		'venezuela'=>'Venezuela'

		],$gala->country3,['id'=>'count3'])}}
	</div>


	<div class="form-group">
		
		{{ Form::text('titulo3',$gala->titulo3,['class'=>'form-control', 'placeholder'=>'Título','id'=>'titulo3'])}}
	</div>

	<div class="form-group">
		
		{{ Form::text('nombre3',$gala->nombre3,['class'=>'form-control', 'placeholder'=>'Nombre','id'=>'namobre3'])}}
	</div>

	<div class="form-group">
		
		{{ Form::text('cancion3',$gala->cancion3,['class'=>'form-control', 'placeholder'=>'Canción','id'=>'cancion3'])}}
	</div>


	


	<h3>Ganador 4</h3>

	<div class="form-group">
		{{ Form::select('country4',[null=>'- Seleccionar Pais -',
		'argentina'=>'Argentina',
		'bolivia'=>'Bolivia',
		'brasil'=>'Brasil',
		'chile'=>'Chile',
		'colombia'=>'Colombia',
		'cuba'=>'Cuba',
		'ecuador'=>'Ecuador',
		'elsalvador'=>'El Salvador',
		'espana'=>'España',
		'estadosunidos'=>'Estados Unidos',
		'guatemala'=>'Guatemala',
		'haiti'=>'Haití',
		'italia'=>'Italia',
		'mexico'=>'México',
		'nicaragua'=>'Nicaragua',
		'panama'=>'Panamá',
		'paraguay'=>'Paraguay',
		'peru'=>'Perú',
		'repdom'=>'República Dominicana',
		'sierraleona'=>'Sierra Leona',
		'uruguay'=>'Uruguay',
		'venezuela'=>'Venezuela'

		],$gala->country4,['id'=>'count4'])}}
	</div>


	<div class="form-group">
		
		{{ Form::text('titulo4',$gala->titulo4,['class'=>'form-control', 'placeholder'=>'Título','id'=>'titulo4'])}}
	</div>

	<div class="form-group">
		
		{{ Form::text('nombre4',$gala->nombre4,['class'=>'form-control', 'placeholder'=>'Nombre','id'=>'namobre4'])}}
	</div>

	<div class="form-group">
		
		{{ Form::text('cancion4',$gala->cancion4,['class'=>'form-control', 'placeholder'=>'Canción','id'=>'cancion4'])}}
	</div>


	

	
	

	<div class="form-group text-right">

		<input type="submit" value="Guardar" class="btn btn-success">
	</div>

{!! Form::close() !!}