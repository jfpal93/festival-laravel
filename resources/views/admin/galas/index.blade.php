
<div class="row">
        <div class="col-md-12">

            <div class="white-box">
		            <div class="comment-body">
		            	<h3>Ediciones Anteriores</h3> 
						
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<td>Año</td>
										<td>Ganador 1</td>
										<td>Ganador 2</td>
										<td>Ganador 3</td>
										<td>Ganador 4</td>
									</tr>
								</thead>
								
								<tbody>
									@foreach ($galas as $gala)
										<tr>
											<td class="txt-oflo">{{$gala->ano}}</td>
											<td>
												{{$gala->titulo1}}
												
											</td>
											<td>
												{{$gala->titulo2}}
												
											</td>
											<td>
												{{$gala->titulo3}}
												
											</td>
											<td>
												{{$gala->titulo4}}
												
											</td>
											
											<td> 
												<a href="" data-toggle="modal" data-target="#ModalGalaShow{{$gala->id}}">Ver</a >
					        					<a href="" data-toggle="modal" data-target="#ModalGalaEdit{{$gala->id}}">Editar</a >
												@include("admin.galas.edit",["gala"=>$gala])

												@include("admin.galas.show",["gala"=>$gala])
												@include('admin.galas.delete',['gala'=>$gala])
											</td>
											
										</tr>
										
									@endforeach
									
								</tbody>

							</table>
						</div>
					    <div class="floating">
							<a class="btn btn-primary btn-fab" href="" data-toggle="modal" onclick="emptyModalNewGala()" data-target="#ModalGalaCreate">
							<i class="material-icons">add</i>
								</a >
						</div>
					</div>
                

            </div>
        </div>
    </div>

    @include("admin.galas.create",["galas"=>$galas])

<script type="text/javascript">
	function GetElementInsideContainer(containerID, childID) {
	    var elm = {};
	    var elms = document.getElementById(containerID).getElementsByTagName("*");
	    for (var i = 0; i < elms.length; i++) {
	        if (elms[i].id === childID) {
	            elm = elms[i];
	            break;
	        }
	    }
	    return elm;
	}
	function emptyModalNewGala(){
		var e = GetElementInsideContainer("ModalGalaCreate", "ano");
		e.value="";
		var l = GetElementInsideContainer("ModalGalaCreate", "link");
		l.value="";
		var c = GetElementInsideContainer("ModalGalaCreate", "count1");
		c.value="null";
		var c2 = GetElementInsideContainer("ModalGalaCreate", "count2");
		c2.value="null";
		var c3 = GetElementInsideContainer("ModalGalaCreate", "count3");
		c3.value="null";
		var c3 = GetElementInsideContainer("ModalGalaCreate", "count4");
		c3.value="null";
		var ni = GetElementInsideContainer("ModalGalaCreate", "titulo1");
		ni.value="";
		var di = GetElementInsideContainer("ModalGalaCreate", "nombre1");
		di.value="";
		var na = GetElementInsideContainer("ModalGalaCreate", "cancion1");
		na.value="";
		var da = GetElementInsideContainer("ModalGalaCreate", "titulo3");
		da.value="";
		var nc = GetElementInsideContainer("ModalGalaCreate", "nombre2");
		nc.value="";
		var dc = GetElementInsideContainer("ModalGalaCreate", "cancion2");
		dc.value="";
		var dc2 = GetElementInsideContainer("ModalGalaCreate", "titulo3");
		dc2.value="";
		var dc3 = GetElementInsideContainer("ModalGalaCreate", "namobre3");
		dc3.value="";
		var dc4 = GetElementInsideContainer("ModalGalaCreate", "cancion3");
		dc4.value="";

		var dc5 = GetElementInsideContainer("ModalGalaCreate", "titulo4");
		dc5.value="";
		var dc6 = GetElementInsideContainer("ModalGalaCreate", "namobre4");
		dc6.value="";
		var dc7 = GetElementInsideContainer("ModalGalaCreate", "cancion4");
		dc7.value="";
	}
	</script>