
<div class="cntl-image">
    <div class="video-container"><iframe width="853" height="480" src="{{$gala->link}}" frameborder="0" allowfullscreen></iframe></div>
    
</div>

<div class="hovereffect1 w3ls_banner_bottom_grid">
    <h4>{{$gala->ano}}</h4>
    <p>
    Ganadores: <br>
    @if(is_null($gala->titulo1))
    
    @else
        <h5>{{$gala->titulo1}}: "{{$gala->cancion1}}"</h5>
        <h6>{{$gala->nombre1}} - &#160; <img id="img" src="images/banderaP/{{$gala->country1}}.png"></h6>
    @endif

    <br>

    @if(is_null($gala->titulo2))
    
    @else
        <h5>{{$gala->titulo2}}: "{{$gala->cancion2}}"</h5>
        <h6>{{$gala->nombre2}} - &#160; <img id="img" src="images/banderaP/{{$gala->country2}}.png"></h6>
    @endif

    <br>

    @if(is_null($gala->titulo3))
    
    @else
        <h5>{{$gala->titulo3}}: "{{$gala->cancion3}}"</h5>
        <h6>{{$gala->nombre3}} - &#160; <img id="img" src="images/banderaP/{{$gala->country3}}.png"></h6>
    @endif
    </p>
    <br>
    @if(is_null($gala->titulo4))
    
    @else
        <h5>{{$gala->titulo4}}: "{{$gala->cancion4}}"</h5>
        <h6>{{$gala->nombre4}} - &#160; <img id="img" src="images/banderaP/{{$gala->country4}}.png"></h6>
    @endif
    </p>
</div>
