<!--FOrmulario-->
{!! Form::open(['url'=>$url,'method'=>$method, 'files'=>true]) !!} 
{!! csrf_field() !!}
	<div class="form-group">
		
		{{ Form::text('name',$invitado->name,['class'=>'form-control', 'placeholder'=>'Nombre de Artista','required' => 'required','id'=>'nameInvitado'])}}
	</div>
	<div class="form-group">
		{{ Form::file('cover',[])}}
	</div>
	<div class="form-group">
		{{ Form::select('country',[null=>'- Seleccionar Pais -',
		'argentina'=>'Argentina',
		'bolivia'=>'Bolivia',
		'brasil'=>'Brasil',
		'canada'=>'Canadá',
		'chile'=>'Chile',
		'colombia'=>'Colombia',
		'ecuador'=>'Ecuador',
		'elsalvador'=>'El Salvador',
		'espana'=>'España',
		'estadosunidos'=>'Estados Unidos',
		'guatemala'=>'Guatemala',
		'haiti'=>'Haití',
		'italia'=>'Italia',
		'mexico'=>'México',
		'nicaragua'=>'Nicaragua',
		'panama'=>'Panamá',
		'paraguay'=>'Paraguay',
		'peru'=>'Perú',
		'repdom'=>'República Dominicana',
		'sierraleona'=>'Sierra Leona',
		'uruguay'=>'Uruguay',
		'venezuela'=>'Venezuela'

		],$invitado->country,['required' => 'required','id'=>'countInvitado'])}}
	</div>
	<div class="form-group">
		
		{{ Form::text('link',$invitado->link,['class'=>'form-control', 'placeholder'=>'Link Videoclip','id'=>'linkInv'])}}
	</div>
	<div class="form-group">
		{{ Form::textarea('short',$invitado->descripcion_corta,['class'=>'form-control', 'placeholder'=>'Descripción Corta','required' => 'required','rows' => 4,'id'=>'shortInvitado'])}}
		
	</div>
	<div class="form-group">
		{{ Form::textarea('description',$invitado->description,['class'=>'form-control', 'placeholder'=>'Biografía','required' => 'required','rows' => 4,'id'=>'descInvitado'])}}
		
	</div>
	<div class="form-group text-right">
		<input type="submit" value="Enviar" class="btn btn-success">
	</div>

{!! Form::close() !!}