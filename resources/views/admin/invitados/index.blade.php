@extends("layouts.app2")

@section("content")
<!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="fa fa-bars"></i></a>
                <!-- <div class="top-left-part"><a class="logo" href="index.html"><b><img src="plugins/images/pixeladmin-logo.png" alt="home" /></b><span class="hidden-xs"><img src="plugins/images/pixeladmin-text.png" alt="home" /></span></a></div>
                <ul class="nav navbar-top-links navbar-left m-l-20 hidden-xs">
                    <li>
                        <form role="search" class="app-search hidden-xs">
                            <input type="text" placeholder="Search..." class="form-control"> <a href=""><i class="fa fa-search"></i></a>
                        </form>
                    </li>
                </ul> -->
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li>
                    @if (Auth::guest())
                       
                    @else
                        <!-- <a class="profile-pic" href="#"><b class="hidden-xs">Admin</b> </a> -->
                        <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out  fa-fw" aria-hidden="true"></i><span class="hide-menu">Logout</span>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {!! csrf_field() !!}
                            </form>
                    @endif
                        
                    </li>
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- Left navbar-header -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                <ul class="nav" id="side-menu">
                    <li style="padding: 10px 0 0;">
                        <a href="{{url('/home')}}" class="waves-effect"><i class="fa fa-home fa-fw" aria-hidden="true"></i><span class="hide-menu">Index</span></a>
                    </li>
                    <li>
                        <a href="{{url('/historia')}}" class="waves-effect"><i class="fa fa-history fa-fw" aria-hidden="true"></i><span class="hide-menu">Historia</span></a>
                    </li>
                    <li>
                        <a href="{{url('/jury')}}" class="waves-effect"><i class="fa fa-dashboard  fa-fw" aria-hidden="true"></i><span class="hide-menu">Jurados</span></a>
                    </li>
                     <li>
                        <a href="{{url('/participantes')}}" class="waves-effect"><i class="fa fa-fort-awesome  fa-fw" aria-hidden="true"></i><span class="hide-menu">Participantes</span></a>
                    </li>
                    <li>
                        <a href="{{url('/invitados')}}" class="waves-effect"><i class="fa fa-microphone  fa-fw" aria-hidden="true"></i><span class="hide-menu">Invitados</span></a>
                    </li>
                    <li>
                        <a href="{{url('/contactform')}}" class="waves-effect"><i class="fa fa-envelope-o  fa-fw" aria-hidden="true"></i><span class="hide-menu">Contactos</span></a>
                    </li>
                    <li >
                        <a href="{{url('/winners')}}" class="waves-effect"><i class="fa fa-trophy  fa-fw" aria-hidden="true"></i><span class="hide-menu">Ganadores</span></a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Left navbar-header end -->
        <!-- Page Content -->
        <div id="page-wrapper">

			<div class="container-fluid">
			    <div class="row bg-title">
			        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			            <h4 class="page-title">Artistas Invitados</h4>
			        </div>
			    </div>
				<div class="white-box">
					<table class="table table-bordered">
						<thead>
							<tr>
								<td>Nombre</td>
								<td>Pais</td>
							</tr>
						</thead>
						
						<tbody>
							@foreach ($invitados as $invitado)
								<tr>
									
									<td>{{$invitado->name}}</td>
									
									<td style="text-transform: capitalize;">
										@if($invitado->country === 'peru')
			                            Perú

			                            @elseif($invitado->country === 'puertorico')
			                            Puerto Rico

			                            @elseif($invitado->country === 'elsalvador')
			                            El Salvador

			                            @elseif($invitado->country === 'estadosunidos')
			                            Estados Unidos

			                            @elseif($invitado->country === 'espana')
			                            España

			                            @elseif($invitado->country === 'repdom')
			                            República Dominicana

			                            @elseif($invitado->country === 'sierraleona')
			                            Sierra Leona

			                            @elseif($invitado->country === 'haiti')
			                            Haití

			                            @elseif($invitado->country === 'mexico')
			                            México

			                            @elseif($invitado->country === 'panama')
			                            Panamá

			                            @else


			                            {{$invitado->country}} 
			                            @endif

									</td>
									
									<td> 
										<a href="" data-toggle="modal" data-target="#ModalInvitadoShow{{$invitado->id}}">Ver</a >
			        					<a href="" data-toggle="modal" data-target="#ModalInvitadoEdit{{$invitado->id}}">Editar</a >
										
										@include("admin.invitados.edit",["invitado"=>$invitado])

										@include("admin.invitados.show",["invitado"=>$invitado])
										@include('admin.invitados.delete',['invitado'=>$invitado])
									</td>
									
								</tr>
								
							@endforeach
							
						</tbody>

					</table>
				</div>
				<div class="floating">
					<a class="btn btn-primary btn-fab" href="" data-toggle="modal" onclick="emptyModalNewInvitado()" data-target="#ModalInvitadosCreate">
										<i class="material-icons">add</i>
											</a >
				</div>
			<!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; machincorp.com </footer>





        </div>
	</div>

@include("admin.invitados.create",["invitados"=>$invitados])
<script type="text/javascript">
	function GetElementInsideContainer(containerID, childID) {
	    var elm = {};
	    var elms = document.getElementById(containerID).getElementsByTagName("*");
	    for (var i = 0; i < elms.length; i++) {
	        if (elms[i].id === childID) {
	            elm = elms[i];
	            break;
	        }
	    }
	    return elm;
	}
	function emptyModalNewInvitado(){
		var e = GetElementInsideContainer("ModalInvitadosCreate", "nameInvitado");
		e.value="";
		var l = GetElementInsideContainer("ModalInvitadosCreate", "shortInvitado");
		l.value="";
		var c = GetElementInsideContainer("ModalInvitadosCreate", "countInvitado");
		c.value="null";
		var ni = GetElementInsideContainer("ModalInvitadosCreate", "descInvitado");
		ni.value="";
		var ni2 = GetElementInsideContainer("ModalInvitadosCreate", "linkInv");
		ni2.value="";
	}
	</script>

@endsection