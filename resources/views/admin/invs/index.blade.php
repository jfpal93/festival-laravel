
<div class="row">
        <div class="col-md-12">
            <div class="white-box">
            <div class="comment-body">
			    <div class="mail-contnet">
			        <h3>Descripción Artistas Invitados</h3> 
			        @foreach ($invs as $inv)
			    	<span class="mail-desc">{{$inv->description}}</span>
			        <span class="time pull-right">
			        	
					        	<a href="" data-toggle="modal" data-target="#ModalInvEdit">Editar</a >
			        </span>
			        @endforeach
			    </div>
			</div>
                

        </div>
    </div>
</div>


@include("admin.invs.edit",["invs"=>$invs])   