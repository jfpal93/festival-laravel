<!--FOrmulario-->
{!! Form::open(['url'=>$url,'method'=>$method, 'files'=>true]) !!} 
{!! csrf_field() !!}
	
	<div class="form-group">
		{{ Form::textarea('description',$inv->description,['class'=>'form-control', 'placeholder'=>'Descripción','required' => 'required','rows' => 4])}}
		
	</div>
	<div class="form-group text-right">
		<!-- <a href="{{url('/home')}}">Regresar</a> -->

		<input type="submit" value="Enviar" class="btn btn-success">
	</div>

{!! Form::close() !!}