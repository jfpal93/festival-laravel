<!--FOrmulario-->
{!! Form::open(['url'=>$url,'method'=>$method, 'files'=>true]) !!} 
{!! csrf_field() !!}
	<div class="form-group">
		
		{{ Form::text('name',$aval->name,['class'=>'form-control', 'placeholder'=>'Nombre aval','required' => 'required','id'=>'nameAval'])}}
	</div
>
	<div class="form-group">
		{{ Form::file('cover',[])}}
	</div>
	
	
	<div class="form-group text-right">

		<input type="submit" value="Enviar" class="btn btn-success">
	</div>

{!! Form::close() !!}