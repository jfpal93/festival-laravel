


<div class="row">
        <div class="col-md-12">

            <div class="white-box">
		            <div class="comment-body">
		            	<h3>Avales</h3> 
						
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<td>logo</td>
										<td>Nombre</td>
										
									</tr>
								</thead>
								
								<tbody>
									@foreach ($avales as $aval)
										<tr>
											<td >
												@if($aval->extension)
													<img src="{{url("/avales/images/avales/$aval->id.$aval->extension")}}" class="img-circle" width="10%" height="auto">
												@endif

											</td>
										
											<td >{{$aval->name}}</td>
											
											
											<td> 
												<a href="" data-toggle="modal" data-target="#ModalAvalShow{{$aval->id}}">Ver</a >
					        					<a href="" data-toggle="modal" data-target="#ModalAvalEdit{{$aval->id}}">Editar</a >
												@include("admin.avales.edit",["aval"=>$aval])

												@include("admin.avales.show",["aval"=>$aval])
												@include('admin.avales.delete',['aval'=>$aval])
											</td>
											
										</tr>
										
									@endforeach
									
								</tbody>

							</table>
						</div>
					    <div class="floating">
							<a class="btn btn-primary btn-fab" href="" data-toggle="modal" onclick="emptyModalNewAval()" data-target="#ModalAvalCreate">
							<i class="material-icons">add</i>
								</a >
						</div>
					</div>
                

            </div>
        </div>
    </div>
    @include("admin.avales.create",["avales"=>$avales])

<script type="text/javascript">
	function GetElementInsideContainer(containerID, childID) {
	    var elm = {};
	    var elms = document.getElementById(containerID).getElementsByTagName("*");
	    for (var i = 0; i < elms.length; i++) {
	        if (elms[i].id === childID) {
	            elm = elms[i];
	            break;
	        }
	    }
	    return elm;
	}
	function emptyModalNewAval(){
		var e = GetElementInsideContainer("ModalAvalCreate", "nameAval");
		e.value="";
		
	}
	</script>