<div class="hovereffect1 w3ls_banner_bottom_grid">
@if(Auth::check() && $part->user_id==Auth::user()->id)
	<div class="absolute actions">
		<a href="{{url('/parts/'.$part->id.'/edit')}}">
			Editar
		</a>
		@include('parts.delete',['part'=>$part])
	</div>
@endif

    <img src="{{url("/parts/images/parts/$part->id.$part->extension")}}" alt=" " class="img-responsive" />
    <div class="overlay">
        <h4>{{$part->description}}</h4>
    </div>
</div>