
<div class="row">
        <div class="col-md-12">
            <div class="white-box">
            <div class="comment-body">
			    <div class="mail-contnet">
			        <h3>Descripción Artistas Participantes</h3> 
			        @foreach ($parts as $part)
			    	<span class="mail-desc">{{$part->description}}</span>
			        <span class="time pull-right">
			        	<a href="" data-toggle="modal" data-target="#ModalPartEdit">Editar</a >
			        </span>
			        
			        @endforeach
			        
			    </div>
			</div>
                

        </div>
    </div>
</div>
@include("admin.parts.edit",["sedes"=>$sedes])
