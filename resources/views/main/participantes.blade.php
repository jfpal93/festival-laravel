@extends('layouts.app')

@section('content')
<!-- banner -->
	<div class="banner1" style='background: url({{url("/portadas/images/portadas/$portada->id.$portada->extension")}}) no-repeat 0px -140px; background-position: 0px -83px;'>
		<div class="container">
			<nav class="navbar navbar-default">
				<div class="navbar-header navbar-left">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
					<nav class="menu menu--iris">
						<ul class="nav navbar-nav menu__list">
							<li class="menu__item "><a href="{{url('/')}}" class="menu__link">Home</a></li>
							<li class="menu__item"><a href="{{url('/history')}}" class="menu__link">Historia</a></li>
                            <li class="menu__item"><a href="{{url('/jurado')}}" class="menu__link">Jurado</a></li>
                            <li class="menu__item"><a href="{{url('/contact')}}" class="menu__link">Contacto</a></li>
						</ul>
					</nav>
				</div>
			</nav>
		</div>
	</div>
<!-- //banner -->	
<!-- breadcrumbs -->
	<div class="breadcrumbs">
		<div class="container">
			<div class="w3l_breadcrumbs_left">
				<ul>
					<li><a href="{{url('/')}}">Inicio</a><i>/</i></li>
					<li>Países Participantes</li>
				</ul>
			</div>
			<div class="w3_agile_breadcrumbs_right">
				<h2>Países Participantes</h2>
				<p>"Sin música, la vida es un viaje por un desierto". <br>Pat Conroy. </p>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
<!-- //breadcrumbs -->
<!-- events -->
	<div class="events">
		<div class="container" id="Destacado">
			<h3 class="agileits_w3layouts_head">Países <span>Participantes</span></h3>
			
			@foreach (array_chunk($participantes->all(), 3) as $participantesRow)
			<div class="container">
				<div class="wthree_latest_albums_grids">
				@foreach($participantesRow as $participante)
					<div class="col-md-4 w3layouts_events_grid" id="race">
					 	<div class="video-container"><iframe width="853" height="480" src="{{$participante->link}}" frameborder="0" allowfullscreen></iframe></div>
						<br>
	                    <h4><a href="#" >{{$participante->song}}</a></h4>
	                    <p class="capitalize">
	                    @if($participante->country === 'peru')
	                    Perú

	                    @elseif($participante->country === 'puertorico')
	                    Puerto Rico

	                    @elseif($participante->country === 'elsalvador')
	                    El Salvador

	                    @elseif($participante->country === 'estadosunidos')
	                    Estados Unidos

	                    @elseif($participante->country === 'espana')
	                    España

	                    @elseif($participante->country === 'repdom')
	                    República Dominicana

	                    @elseif($participante->country === 'sierraleona')
	                    Sierra Leona

	                    @elseif($participante->country === 'haiti')
	                    Haití

	                    @elseif($participante->country === 'mexico')
	                    México

	                    @elseif($participante->country === 'panama')
	                    Panamá

	                    @else


	                    {{$participante->country}} 
	                    @endif
	                    - &#160; <img id="img" src="images/banderaP/{{$participante->country}}.png"> </p>
	                    <p>
	                    Autor: {{$participante->autor_name}}
	                    <br>
	                    Compositor: {{$participante->compositor_name}}
	                    <br>
	                    Intérprete: {{$participante->interprete_name}}
	                    </p>
						<div class="w3_more">
							<a href="#" data-toggle="modal" data-target="#Country{{$participante->id}}">Leer más</a>
						</div>
					</div>
				@endforeach

				</div>
			</div>

			@endforeach
			

			

		</div>
	</div>

	<!-- bootstrap-pop-up -->
	@foreach($participantes as $participante)
		<div class="modal video-modal fade" id="Country{{$participante->id}}" tabindex="-1" role="dialog" aria-labelledby="myModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						{{$participante->song}}
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
					</div>
					<section>
					@if($participante->interprete_name === $participante->autor_name && $participante->interprete_name === $participante->compositor_name)
						<div class="modal-body">

								<div class="col-md-6 w3_modal_body_left">
								@if($participante->interprete_extension)
									<img src="{{url("/participantes/images/participantes/interprete-$participante->id-$participante->country.$participante->interprete_extension")}}" class="img-responsive">
								@endif
								</div>
								<div class="col-md-6 w3_modal_body_right">
									<h4>Intérprete, Autor, Compositor</h4>
									<p><i>{{$participante->interprete_name}}</i>{{$participante->interprete_description}}</p>
									<div class="col-md-12">
										@if($participante->facebook)
										<span>
                            			
		                                <i class="fa fa-facebook" aria-hidden="true"></i>
		                            
				                            {{$participante->facebook}}
				                        </span>	
				                        <br>
				                        <br>
				                        @endif
				                        @if($participante->twitter)
				                        <span>

				                        	<i class="fa fa-twitter" aria-hidden="true"></i>
				                        	{{$participante->twitter}}

				                        </span>
				                        <br>
				                        <br>
					                    
					                    @endif
					                    @if($participante->instagram)
					                    <span>
					                    	<i class="fa fa-instagram" aria-hidden="true"></i>
					                    	{{$participante->instagram}}

					                    </span>
					                    <br>
					                    <br>
					                    
					                    @endif
					                    @if($participante->youtube)
					                    <span>
					                    	<i class="fa fa-youtube-play" aria-hidden="true"></i>
					                    	{{$participante->youtube}}

					                    </span>
					                    <br>
					                    <br>
					                    
					                    @endif
					                    @if($participante->web)
					                    <span>
					                    	<i class="fa fa-first-order" aria-hidden="true"></i>
					                    	<a href="{{$participante->web}}"></a>

					                    </span>
					                    <br>
					                    <br>
					                    
					                    @endif
					                    @if($participante->social6)
					                    <span>
					                    	<i class="fa fa-bullhorn" aria-hidden="true"></i>
					                    	{{$participante->social6}}

					                    </span>
					                    <br>
					                    <br>
					                    
					                    @endif

									</div>
									
                    
                    
								</div>
								<div class="clearfix"> </div>
							</div>

					@else
						@if($participante->interprete_name === $participante->autor_name )
						<div class="modal-body">

								<div class="col-md-6 w3_modal_body_left">
								@if($participante->interprete_extension)
									<img src="{{url("/participantes/images/participantes/interprete-$participante->id-$participante->country.$participante->interprete_extension")}}" class="img-responsive">
								@endif
								</div>
								<div class="col-md-6 w3_modal_body_right">
									<h4>Autor, Intérprete</h4>
									<p><i>{{$participante->interprete_name}}</i>{{$participante->interprete_description}}</p>
									<div class="col-md-12">
										@if($participante->facebook)
										<span>
                            			
		                                <i class="fa fa-facebook" aria-hidden="true"></i>
		                            
				                            {{$participante->facebook}}
				                        </span>	
				                        <br>
				                        <br>
				                        @endif
				                        @if($participante->twitter)
				                        <span>

				                        	<i class="fa fa-twitter" aria-hidden="true"></i>
				                        	{{$participante->twitter}}

				                        </span>
				                        <br>
				                        <br>
					                    
					                    @endif
					                    @if($participante->instagram)
					                    <span>
					                    	<i class="fa fa-instagram" aria-hidden="true"></i>
					                    	{{$participante->instagram}}

					                    </span>
					                    <br>
					                    <br>
					                    
					                    @endif
					                    @if($participante->youtube)
					                    <span>
					                    	<i class="fa fa-youtube-play" aria-hidden="true"></i>
					                    	{{$participante->youtube}}

					                    </span>
					                    <br>
					                    <br>
					                    
					                    @endif
					                    @if($participante->web)
					                    <span>
					                    	<i class="fa fa-first-order" aria-hidden="true"></i>
					                    	{{$participante->web}}

					                    </span>
					                    <br>
					                    <br>
					                    
					                    @endif
					                    @if($participante->social6)
					                    <span>
					                    	<i class="fa fa-bullhorn" aria-hidden="true"></i>
					                    	{{$participante->social6}}

					                    </span>
					                    <br>
					                    <br>
					                    
					                    @endif

									</div>
									
								</div>
								<div class="clearfix"> </div>
							</div>
							<div class="modal-body">
								<div class="col-md-6 w3_modal_body_left">
								@if($participante->compositor_extension)
									<img src="{{url("/participantes/images/participantes/compositor-$participante->id-$participante->country.$participante->compositor_extension")}}" class="img-responsive">
								@endif
								</div>
								
								<div class="col-md-6 w3_modal_body_right">
									<h4>Compositor</h4>
									<p><i>{{$participante->compositor_name}}</i>{{$participante->compositor_description}}</p>
								</div>
								
								<div class="clearfix"> </div>
							</div>



						

						@elseif($participante->interprete_name === $participante->compositor_name)
						<div class="modal-body">

								
								
								<div class="col-md-6 w3_modal_body_left">
								@if($participante->autor_extension)
									<img src="{{url("/participantes/images/participantes/autor-$participante->id-$participante->country.$participante->autor_extension")}}" class="img-responsive">
								@endif
								</div>
								<div class="col-md-6 w3_modal_body_right">
									<h4>Autor</h4>
									<p><i>{{$participante->autor_name}}</i>{{$participante->autor_description}}</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						<div class="modal-body">
								<div class="col-md-6 w3_modal_body_right">
									<h4>Intérprete, Compositor</h4>
									<p><i>{{$participante->interprete_name}}</i>{{$participante->interprete_description}}</p>
									<div class="col-md-12">
										@if($participante->facebook)
										<span>
                            			
		                                <i class="fa fa-facebook" aria-hidden="true"></i>
		                            
				                            {{$participante->facebook}}
				                        </span>	
				                        <br>
				                        <br>
				                        @endif
				                        @if($participante->twitter)
				                        <span>

				                        	<i class="fa fa-twitter" aria-hidden="true"></i>
				                        	{{$participante->twitter}}

				                        </span>
				                        <br>
				                        <br>
					                    
					                    @endif
					                    @if($participante->instagram)
					                    <span>
					                    	<i class="fa fa-instagram" aria-hidden="true"></i>
					                    	{{$participante->instagram}}

					                    </span>
					                    <br>
					                    <br>
					                    
					                    @endif
					                    @if($participante->youtube)
					                    <span>
					                    	<i class="fa fa-youtube-play" aria-hidden="true"></i>
					                    	{{$participante->youtube}}

					                    </span>
					                    <br>
					                    <br>
					                    
					                    @endif
					                    @if($participante->web)
					                    <span>
					                    	<i class="fa fa-first-order" aria-hidden="true"></i>
					                    	{{$participante->web}}

					                    </span>
					                    <br>
					                    <br>
					                    
					                    @endif
					                    @if($participante->social6)
					                    <span>
					                    	<i class="fa fa-bullhorn" aria-hidden="true"></i>
					                    	{{$participante->social6}}

					                    </span>
					                    <br>
					                    <br>
					                    
					                    @endif

									</div>
									
								</div>

								<div class="col-md-6 w3_modal_body_left">
								@if($participante->interprete_extension)
									<img src="{{url("/participantes/images/participantes/interprete-$participante->id-$participante->country.$participante->interprete_extension")}}" class="img-responsive">
								@endif
								</div>
								
								<div class="clearfix"> </div>
							</div>
							
						@elseif($participante->autor_name === $participante->compositor_name)
							<div class="modal-body">

								
								<div class="col-md-6 w3_modal_body_right">
									<h4>Autor, Compositor</h4>
									<p><i>{{$participante->autor_name}}</i>{{$participante->autor_description}}</p>
								</div>
								<div class="col-md-6 w3_modal_body_left">
								@if($participante->autor_extension)
									<img src="{{url("/participantes/images/participantes/autor-$participante->id-$participante->country.$participante->autor_extension")}}" class="img-responsive">
								@endif
								</div>
								<div class="clearfix"> </div>
							</div>
							<div class="modal-body">
								

								<div class="col-md-6 w3_modal_body_left">
								@if($participante->interprete_extension)
									<img src="{{url("/participantes/images/participantes/interprete-$participante->id-$participante->country.$participante->interprete_extension")}}" class="img-responsive">
								@endif
								</div>
								<div class="col-md-6 w3_modal_body_right">
									<h4>Intérprete</h4>
									<p><i>{{$participante->interprete_name}}</i>{{$participante->interprete_description}}</p>
									<div class="col-md-12">
										@if($participante->facebook)
										<span>
                            			
		                                <i class="fa fa-facebook" aria-hidden="true"></i>
		                            
				                            {{$participante->facebook}}
				                        </span>	
				                        <br>
				                        <br>
				                        @endif
				                        @if($participante->twitter)
				                        <span>

				                        	<i class="fa fa-twitter" aria-hidden="true"></i>
				                        	{{$participante->twitter}}

				                        </span>
				                        <br>
				                        <br>
					                    
					                    @endif
					                    @if($participante->instagram)
					                    <span>
					                    	<i class="fa fa-instagram" aria-hidden="true"></i>
					                    	{{$participante->instagram}}

					                    </span>
					                    <br>
					                    <br>
					                    
					                    @endif
					                    @if($participante->youtube)
					                    <span>
					                    	<i class="fa fa-youtube-play" aria-hidden="true"></i>
					                    	{{$participante->youtube}}

					                    </span>
					                    <br>
					                    <br>
					                    
					                    @endif
					                    @if($participante->web)
					                    <span>
					                    	<i class="fa fa-first-order" aria-hidden="true"></i>
					                    	{{$participante->web}}

					                    </span>
					                    <br>
					                    <br>
					                    
					                    @endif
					                    @if($participante->social6)
					                    <span>
					                    	<i class="fa fa-bullhorn" aria-hidden="true"></i>
					                    	{{$participante->social6}}

					                    </span>
					                    <br>
					                    <br>
					                    
					                    @endif

									</div>
									
								</div>
								
								<div class="clearfix"> </div>
							</div>
						@else
						<div class="modal-body">
								<div class="col-md-6 w3_modal_body_right">
									<h4>Autor</h4>
									<p><i>{{$participante->autor_name}}</i>{{$participante->autor_description}}</p>
								</div>
								
								
								<div class="col-md-6 w3_modal_body_left">
								@if($participante->autor_extension)
									<img src="{{url("/participantes/images/participantes/autor-$participante->id-$participante->country.$participante->autor_extension")}}" class="img-responsive">
								@endif
								</div>
								
								<div class="clearfix"> </div>
							</div>
						<div class="modal-body">

								
								
								<div class="col-md-6 w3_modal_body_left">
								@if($participante->compositor_extension)
									<img src="{{url("/participantes/images/participantes/compositor-$participante->id-$participante->country.$participante->compositor_extension")}}" class="img-responsive">
								@endif
								</div>
								<div class="col-md-6 w3_modal_body_right">
									<h4>Compositor</h4>
									<p><i>{{$participante->compositor_name}}</i>{{$participante->compositor_description}}</p>
								</div>
								<div class="clearfix"> </div>
							</div>
							<div class="modal-body">

								<div class="col-md-6 w3_modal_body_right">
									<h4>Intérprete</h4>
									<p><i>{{$participante->interprete_name}}</i>{{$participante->interprete_description}}</p>
									<div class="col-md-12">
										@if($participante->facebook)
										<span>
                            			
		                                <i class="fa fa-facebook" aria-hidden="true"></i>
		                            
				                            {{$participante->facebook}}
				                        </span>	
				                        <br>
				                        <br>
				                        @endif
				                        @if($participante->twitter)
				                        <span>

				                        	<i class="fa fa-twitter" aria-hidden="true"></i>
				                        	{{$participante->twitter}}

				                        </span>
				                        <br>
				                        <br>
					                    
					                    @endif
					                    @if($participante->instagram)
					                    <span>
					                    	<i class="fa fa-instagram" aria-hidden="true"></i>
					                    	{{$participante->instagram}}

					                    </span>
					                    <br>
					                    <br>
					                    
					                    @endif
					                    @if($participante->youtube)
					                    <span>
					                    	<i class="fa fa-youtube-play" aria-hidden="true"></i>
					                    	{{$participante->youtube}}

					                    </span>
					                    <br>
					                    <br>
					                    
					                    @endif
					                    @if($participante->web)
					                    <span>
					                    	<i class="fa fa-first-order" aria-hidden="true"></i>
					                    	{{$participante->web}}

					                    </span>
					                    <br>
					                    <br>
					                    
					                    @endif
					                    @if($participante->social6)
					                    <span>
					                    	<i class="fa fa-bullhorn" aria-hidden="true"></i>
					                    	{{$participante->social6}}

					                    </span>
					                    <br>
					                    <br>
					                    
					                    @endif

									</div>
									
								</div>

								<div class="col-md-6 w3_modal_body_left">
								@if($participante->interprete_extension)
									<img src="{{url("/participantes/images/participantes/interprete-$participante->id-$participante->country.$participante->interprete_extension")}}" class="img-responsive">
								@endif
								</div>
								
								<div class="clearfix"> </div>
							</div>

							


						@endif



					@endif
						
					</section>
				</div>
			</div>
		</div>
	<!-- //bootstrap-pop-up -->
	@endforeach
	<div class="modal video-modal fade" id="countryModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    Representantes Nacionales
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <section>
                    <div class="modal-body">
                        <div class="col-md-12 w3_modal_body_right">
                            @foreach($contactcountrys as $contact)
                                <p>
                                    <h4>
                                    	@if($contact->country === 'peru')
					                    Perú

					                    @elseif($contact->country === 'puertorico')
					                    Puerto Rico

					                    @elseif($contact->country === 'elsalvador')
					                    El Salvador

					                    @elseif($contact->country === 'estadosunidos')
					                    Estados Unidos

					                    @elseif($contact->country === 'espana')
					                    España

					                    @elseif($contact->country === 'repdom')
					                    República Dominicana

					                    @elseif($contact->country === 'sierraleona')
					                    Sierra Leona

					                    @elseif($contact->country === 'haiti')
					                    Haití

					                    @elseif($contact->country === 'mexico')
					                    México

					                    @elseif($contact->country === 'panama')
					                    Panamá

					                    @else


					                    {{$contact->country}} 
					                    @endif
                                    	</h4>
                                {{$contact->name}} - 
                                <a href="mailto:{{$contact->mail}}?Subject=Hello" target="_top">{{$contact->mail}}</a>
                                </p>


                            @endforeach
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
<!-- contactcountrys -->
@endsection