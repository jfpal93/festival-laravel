@extends('layouts.app')

@section('content')
    <!-- banner -->
    <div class="banner" style='background: url({{url("/portadas/images/portadas/$portada->id.$portada->extension")}}) no-repeat 0px 0px;'>
        
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="navbar-header navbar-left">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                    <nav class="menu menu--iris">
                        <ul class="nav navbar-nav menu__list">
                            <li class="menu__item menu__item--current"><a href="{{url('/')}}" class="menu__link">Home</a></li>
                            <li class="menu__item"><a href="{{url('/history')}}" class="menu__link">Historia</a></li>
                            
                            <li class="menu__item"><a href="{{url('/jurado')}}" class="menu__link">Jurado</a></li>
                            <li class="menu__item"><a href="{{url('/contact')}}" class="menu__link">Contacto</a></li>
                            
                        </ul>
                    </nav>
                </div>
            </nav>
           <div class="agile_banner_info">
                <h3></h3>
                <div class="agile_banner_info_pos">
                    <h2></h2>
                </div>
            </div>
        </div>
    </div>
<!-- //banner -->   
<!-- banner-bottom -->
    <div class="banner-bottom">
        <div class="col-md-3 wthree_banner_bottom_grid">
            
            @foreach ($sedes as $sede)
                
            <div class="wthree_banner_bottom_grid1">
                @if($sede->extension)
                    <img src="{{url("/sedes/images/sedes/principal-$sede->id.$sede->extension")}}" class="img-responsive">

                @endif
                <div class="wthree_banner_bottom_grid_pos">
                    <h4>{{$sede->name}} </h4>
                </div>
            </div>
            <div class="w3layouts_banner_bottom_grid">
                <p>{{$sede->descripcion_corta}}</p>
               
                <a href="" data-toggle="modal" data-target="#Sede{{$sede->id}}">Leer Más</a>
            </div>
           
            
            @endforeach
        </div>
        <div class="col-md-3 agileits_w3layouts_banner_bottom_grid">
            <div class="hovereffect">
                <img src="images/index/flags.png" alt=" " class="img-responsive" />
                
            </div>
            <div class="agileinfo_banner_bottom_grid">
                <div class="agileits_banner_bottom_grid1">
                    <h3>Artistas Invitados</h3>
                    @foreach ($invs as $inv)
                        <p>{{$inv->description}}</p>
                    @endforeach
                    <a href="{{url('/artistasinvitados')}}">Ver Invitados</a>
                </div>
            </div>
        </div>
        <div class="col-md-3 agileits_w3layouts_banner_bottom_grid">
            <div class="hovereffect">
                <img src="images/index/flags.png" alt=" " class="img-responsive" />
                
            </div>
            <div class="agileinfo_banner_bottom_grid w3l_banner_bottom1">
                <div class="agileits_banner_bottom_grid1">
                    <h3>Países Participantes</h3>
                    @foreach ($parts as $part)
                        <p>{{$part->description}}</p>
                    @endforeach
                    <a href="{{url('/paisesparticipantes')}}">Ver Participantes</a>
                    
                </div>
            </div>
        </div>
        <div class="col-md-3 agileits_w3layouts_banner_bottom_grid">
            @foreach ($lugares as $lugar)
                @if($lugar->extension)
                    <img src="{{url("/lugares/images/lugares/principal-$lugar->id.$lugar->extension")}}" class="img-responsive">

                @endif
            
            
            <div class="agileinfo_banner_bottom_grid w3l_banner_bottom2">
                <div class="agileits_banner_bottom_grid1">
                    <h3>{{$lugar->name}}</h3>
                    <p>{{$lugar->descripcion_corta}}</p>
                    <a href="" data-toggle="modal" data-target="#Lugar{{$lugar->id}}">Leer Más</a>
                </div>
            </div>
            @endforeach
        </div>
        <div class="clearfix"></div>
    </div>
<!-- //banner-bottom -->
<!-- about -->
    <div class="about">
        <div class="container">
            <div class="w3_agile_about_grids">
            @foreach ($desceventos as $descevento)
                <div class="col-md-6 w3_agile_about_grid_left">
                @if($descevento->extension)
                    <img src="{{url("/desceventos/images/desceventos/$descevento->id.$descevento->extension")}}" class="img-responsive">

                @endif
                </div>
                <div class="col-md-6 w3_agile_about_grid_right">
                    <h3>{{$descevento->title}}</h3>
                    <!--<h4>Music Album</h4>-->
                    <p>{{$descevento->texto1}}<br>
                        <span>{{$descevento->texto2}}</span></p>
                </div>
            @endforeach
                
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
<!-- //about -->
<!-- features -->
    <div class="features">
        <div class="container">
            <div class="col-md-6 agile_features_left">
                <img src="images/cronograma2.png" alt=" " class="img-responsive" />
            </div>
            <div class="col-md-6 agile_features_right">
                <p><a href="">FESTIVAL INTERNACIONAL DE LA CANCIÓN</a></p>
                <h4>Programa de Actividades - <a href="archives/Programa_actividades_festival.pdf" target="_blank">Ver más</a></h4>
                
                <ul>
                    @foreach ($cronogramas as $cronograma)
           
                    
                        <li><span>{{$cronograma->fecha}}</span>: {{$cronograma->lugar}} - {{$cronograma->evento}}</li>
                    
                    
                    @endforeach
                </ul>
               
                

            </div>
            
            <div class="clearfix"> </div>
        </div>
    </div>
<!-- //features -->
<!-- newsletter -->

<!-- //newsletter -->
<!-- team -->
    <div class="team">
        <div class="col-md-3 agile_team_left">
            <h3>Artistas Invitados</h3>
        </div>
        <div class="col-md-9 agile_team_grid">  
            <ul id="flexiselDemo1"> 
            @foreach($invitados as $invitado)
                <li>                    
                    <div class="hovereffect1 w3ls_banner_bottom_grid">
                        <img src="{{url("/invitados/images/invitados/$invitado->id.$invitado->extension")}}" alt=" " class="img-responsive" />

                        <div class="overlay">
                            <h4>{{$invitado->name}}</h4>
                            
                        </div>
                    </div>

                </li>
            
            @endforeach
                
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
<!-- //team -->
<!-- latest-albums -->
    <div class="latest-albums">
        <div class="container">
            <h3 class="agileits_w3layouts_head"><span>Avales</span></h3>
            <br>
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        @foreach ($avals as $aval)
                        
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="display: flex; justify-content: center; align-items: center; margin-bottom: 30px;">
                            @if($aval->extension)
                                <img src="{{url("/avales/images/avales/$aval->id.$aval->extension")}}" class="logo" width="180" height="180">

                            @endif
                            </div>
                        @endforeach
                         
                             
                         
                     </div>
                 </div>
             </div>
        </div>
    </div>

    <!-- latest-albums -->
    <div class="latest-albums">
        <div class="container">
            <h3 class="agileits_w3layouts_head"><span>Patrocinantes</span></h3>
            <br>
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                    @if (count($patrocinantes) === 1)
                        @foreach ($patrocinantes as $patrocinante)
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="display: flex; justify-content: center; align-items: center; margin-bottom: 30px;">
                            @if($patrocinante->extension)
                                <img src="{{url("/patrocinantes/images/patrocinantes/$patrocinante->id.$patrocinante->extension")}}" class="logo" width="250" height="250">

                            @endif
                            </div>
                        @endforeach
                    @elseif (count($patrocinantes) === 2)
                            @foreach ($patrocinantes as $patrocinante)
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="display: flex; justify-content: center; align-items: center; margin-bottom: 30px;">
                                @if($patrocinante->extension)
                                    @if($patrocinante->name === 'Clínica Avril')
                                    <img src="{{url("/patrocinantes/images/patrocinantes/$patrocinante->id.$patrocinante->extension")}}" class="logo" width="130" height="130">
                                    @else
                                    <img src="{{url("/patrocinantes/images/patrocinantes/$patrocinante->id.$patrocinante->extension")}}" class="logo" width="240" height="240">
                                    @endif

                                @endif
                            </div>
                        @endforeach
                    @elseif (count($patrocinantes) === 3)
                            @foreach ($patrocinantes as $patrocinante)
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="display: flex; justify-content: center; align-items: center; margin-bottom: 30px;">
                                @if($patrocinante->extension)
                                    @if($patrocinante->name === 'Clínica Avril')
                                    <img src="{{url("/patrocinantes/images/patrocinantes/$patrocinante->id.$patrocinante->extension")}}" class="logo" width="130" height="130">
                                    @else
                                    <img src="{{url("/patrocinantes/images/patrocinantes/$patrocinante->id.$patrocinante->extension")}}" class="logo" width="240" height="240">
                                    @endif

                                @endif
                            </div>
                        @endforeach

                    @else
                        @foreach ($patrocinantes as $patrocinante)
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="display: flex; justify-content: center; align-items: center; margin-bottom: 30px;">
                            @if($patrocinante->extension)
                                <img src="{{url("/patrocinantes/images/patrocinantes/$patrocinante->id.$patrocinante->extension")}}" class="logo" width="200" height="200">

                            @endif
                            </div>
                        @endforeach
                    @endif
                         
                             
                         
                     </div>
                 </div>
             </div>
        </div>
    </div>

    <!-- latest-albums -->
    <div class="latest-albums">
        <div class="container">
            <h3 class="agileits_w3layouts_head"><span>Auspiciantes</span></h3>
            <br>
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <?php $mod = (count($auspiciantes)%4); 

                        ?>
                        @if ($mod== 0)
                            @foreach ($auspiciantes as $auspiciante)
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="display: flex; justify-content: center; align-items: center; margin-bottom: 30px;">
                                @if($auspiciante->extension)
                                    @if($auspiciante->name==='Cantegril')
                                    <img src="{{url("/auspiciantes/images/auspiciantes/$auspiciante->id.$auspiciante->extension")}}" class="logo" width="230" height="230">

                                    
                                    @else
                                        @if($auspiciante->name==='MachínCorp')
                                            <a href="http://machincorp.com/" target="_blank">
                                                <img src="{{url("/auspiciantes/images/auspiciantes/$auspiciante->id.$auspiciante->extension")}}" class="logo" width="140" height="140">
                                            </a>
                                        @else
                                            <img src="{{url("/auspiciantes/images/auspiciantes/$auspiciante->id.$auspiciante->extension")}}" class="logo" width="140" height="140">
                                        @endif
                                    @endif

                                @endif
                                </div>
                            @endforeach
                        @else
                        <?php $a = count($auspiciantes) - count($auspiciantes)%4;
                        $b=12/(count($auspiciantes)%4);

                        ?>
                            @foreach ($auspiciantes as $index => $auspiciante)
                                @if($index<$a)
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="display: flex; justify-content: center; align-items: center; margin-bottom: 30px;">
                                   @else
                                    <div class="col-lg-{{$b}} col-md-{{$b}} col-sm-6 col-xs-12" style="display: flex; justify-content: center; align-items: center; margin-bottom: 30px;">


                                @endif
                                    @if($auspiciante->extension)
                                        @if($auspiciante->name==='Cantegril')
                                        <img src="{{url("/auspiciantes/images/auspiciantes/$auspiciante->id.$auspiciante->extension")}}" class="logo" width="230" height="230">


                                        

                                        
                                        @else
                                            @if($auspiciante->name==='MachínCorp')
                                            <a href="http://machincorp.com/" target="_blank">
                                                <img src="{{url("/auspiciantes/images/auspiciantes/$auspiciante->id.$auspiciante->extension")}}" class="logo" width="140" height="140">
                                            </a>
                                            @else
                                            <img src="{{url("/auspiciantes/images/auspiciantes/$auspiciante->id.$auspiciante->extension")}}" class="logo" width="140" height="140">
                                            @endif
                                        @endif

                                    @endif
                                    </div>
                                @endforeach

                        @endif
                         
                             
                         
                     </div>
                 </div>
             </div>
        </div>
    </div>


@foreach($lugares as $lugar)
        <div class="modal video-modal fade" id="Lugar{{$lugar->id}}" tabindex="-1" role="dialog" aria-labelledby="myModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        {{$lugar->name}}
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                        
                    </div>
                    <section>
                        <div class="modal-body">

                            <div class="col-md-6 w3_modal_body_left">
                            @if($lugar->extension)
                                <img src="{{url("/lugares/images/lugares/principal-$lugar->id.$lugar->extension")}}" class="img-responsive">
                            @endif
                            @if($lugar->extension2)
                                <img src="{{url("/lugares/images/lugares/modal1-$lugar->id.$lugar->extension2")}}" class="img-responsive">
                            @endif
                            @if($lugar->extension3)
                                <img src="{{url("/lugares/images/lugares/modal2-$lugar->id.$lugar->extension3")}}" class="img-responsive">
                            @endif
                            </div>
                            <div class="col-md-6 w3_modal_body_right">
                                <p><i>{{$lugar->name}}</i>{{$lugar->description}}</p>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    <!-- //bootstrap-pop-up -->
    @endforeach
    @foreach($sedes as $sede)
        <div class="modal video-modal fade " id="Sede{{$sede->id}}" tabindex="-1" role="dialog" aria-labelledby="myModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        {{$sede->name}}
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                        
                    </div>
                    <section>
                        <div class="modal-body">

                            <div class="col-md-6 w3_modal_body_left">
                            @if($sede->extension)
                                <img src="{{url("/sedes/images/sedes/principal-$sede->id.$sede->extension")}}" class="img-responsive">
                            @endif
                            @if($sede->extension)
                                <img src="{{url("/sedes/images/sedes/modal1-$sede->id.$sede->extension2")}}" class="img-responsive">
                            @endif
                            @if($sede->extension)
                                <img src="{{url("/sedes/images/sedes/modal2-$sede->id.$sede->extension3")}}" class="img-responsive">
                            @endif
                            </div>
                            <div class="col-md-6 w3_modal_body_right">
                                <h4>{{$sede->name}}</h4>
                                <p>{{$sede->description}}</p>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    <!-- //bootstrap-pop-up -->
    @endforeach

    @foreach($winners as $winner)
        @if($winner->enabled === 1)
            <div class="modal video-modal fade" id="winners" tabindex="-1" role="dialog" aria-labelledby="myModal">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            GANADORES {{$winner->ano}}
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                        
                        </div>
                        <section>
                            <div class="modal-body">

                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 " style="    margin: 50px 0px 10px;">
                                    
                                    
                                    
                                    <div class="video-container"><iframe width="853" height="480" src="{{$winner->link1}}" frameborder="0" allowfullscreen></iframe></div>
                                    <br>
                                    <h4 class="agileits_w3layouts_head">{{$winner->titulo1}}</h4>
                                    <!-- <h4>{{$winner->titulo1}}</h4>  -->
                                
                                
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 " style="    margin: 50px 0px 10px;">
                                    
                                    
                                    <div class="video-container"><iframe width="853" height="480" src="{{$winner->link2}}" frameborder="0" allowfullscreen></iframe></div>
                                    <br>
                                    <h4 class="agileits_w3layouts_head">{{$winner->titulo2}}</h4> 
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" style="    margin: 50px 0px 10px;">
                                    
                                    
                                    <div class="video-container"><iframe width="853" height="480" src="{{$winner->link3}}" frameborder="0" allowfullscreen></iframe></div>
                                    <br>
                                    <h4 class="agileits_w3layouts_head">{{$winner->titulo3}}</h4> 
                                    
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>

        <!-- //bootstrap-pop-up -->
        <script type="text/javascript">
            $(document).ready(function(){
                $("#winners").modal('show');
            });
        </script>
        @endif
    @endforeach

<div class="modal video-modal fade" id="countryModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    Representantes Nacionales
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <section>
                    <div class="modal-body">
                        <div class="col-md-12 w3_modal_body_right">
                            @foreach($contactcountrys as $contact)
                                <p>
                                    <h4>@if($contact->country === 'peru')
                                        Perú

                                        @elseif($contact->country === 'puertorico')
                                        Puerto Rico

                                        @elseif($contact->country === 'elsalvador')
                                        El Salvador

                                        @elseif($contact->country === 'estadosunidos')
                                        Estados Unidos

                                        @elseif($contact->country === 'espana')
                                        España

                                        @elseif($contact->country === 'repdom')
                                        República Dominicana

                                        @elseif($contact->country === 'sierraleona')
                                        Sierra Leona

                                        @elseif($contact->country === 'haiti')
                                        Haití

                                        @elseif($contact->country === 'mexico')
                                        México

                                        @elseif($contact->country === 'panama')
                                        Panamá

                                        @else


                                        {{$contact->country}} 
                                        @endif</h4>
                                {{$contact->name}} - 
                                <a href="mailto:{{$contact->mail}}?Subject=Hello" target="_top">{{$contact->mail}}</a>
                                </p>


                            @endforeach
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
<!-- contactcountrys -->