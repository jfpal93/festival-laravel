@extends('layouts.app')

@section('content')
<!-- banner -->
	<div class="banner1" style='background: url({{url("/portadas/images/portadas/$portada->id.$portada->extension")}}) no-repeat 0px -140px; background-position: 0px -83px;'>
		<div class="container">
			<nav class="navbar navbar-default">
				<div class="navbar-header navbar-left">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
					<nav class="menu menu--iris">
						<ul class="nav navbar-nav menu__list">
							<li class="menu__item "><a href="{{url('/')}}" class="menu__link">Home</a></li>
							<li class="menu__item"><a href="{{url('/history')}}" class="menu__link">Historia</a></li>
                            <li class="menu__item"><a href="{{url('/jurado')}}" class="menu__link">Jurado</a></li>
                            <li class="menu__item"><a href="{{url('/contact')}}" class="menu__link">Contacto</a></li>
							
						</ul>
					</nav>
				</div>
			</nav>
		</div>
	</div>
<!-- //banner -->	
<!-- breadcrumbs -->
	<div class="breadcrumbs">
		<div class="container">
			<div class="w3l_breadcrumbs_left">
				<ul>
					<li><a href="{{url('/')}}">Inicio</a><i>/</i></li>
					<li>Invitados</li>
				</ul>
			</div>
			<div class="w3_agile_breadcrumbs_right">
				<h2>Invitados</h2>
				<p>"La música es más elocuente que las palabras". <br>  Bono.</p>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
<!-- //breadcrumbs -->

<!-- events -->
	<div class="events">
		<div class="container">
			<h3 class="agileits_w3layouts_head">Artistas <span>Invitados</span></h3>
			
			@foreach (array_chunk($invitados->all(), 3) as $invitadosRow)
			<div class="container">
				<div class="wthree_latest_albums_grids">
					
						@foreach($invitadosRow as $invitado)
							<div class="col-md-4 w3layouts_events_grid">
								@if($invitado->extension)
		                        <img src="{{url("/invitados/images/invitados/$invitado->id.$invitado->extension")}}" class="img-responsive">
		                        @endif
		                        <br>
			                    <h4><a href="#" >{{$invitado->name}}</a></h4>
			                    <p class="capitalize">

			                    	@if($invitado->country === 'peru')
				                    Perú

				                    @elseif($invitado->country === 'puertorico')
				                    Puerto Rico

				                    @elseif($invitado->country === 'elsalvador')
				                    El Salvador

				                    @elseif($invitado->country === 'estadosunidos')
				                    Estados Unidos

				                    @elseif($invitado->country === 'espana')
				                    España

				                    @elseif($invitado->country === 'repdom')
				                    República Dominicana

				                    @elseif($invitado->country === 'sierraleona')
				                    Sierra Leona

				                    @elseif($invitado->country === 'haiti')
				                    Haití

				                    @elseif($invitado->country === 'mexico')
				                    México

				                    @else


				                    {{$invitado->country}} 
				                    @endif - &#160; <img id="img" src="images/banderaP/{{$invitado->country}}.png"> </p>
			                    <p>{{$invitado->descripcion_corta}}</p>
								<div class="w3_more">
									<a href="" data-toggle="modal" data-target="#Invitado{{$invitado->id}}">Leer más</a>
								</div>
							</div>
					
						@endforeach


					
				</div>
			</div>

			@endforeach
			

			

		</div>
	</div>



	<!-- bootstrap-pop-up -->
	@foreach($invitados as $invitado)
		<div class="modal video-modal fade" id="Invitado{{$invitado->id}}" tabindex="-1" role="dialog" aria-labelledby="myModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						{{$invitado->name}}
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
					</div>
					<section>
					
						<div class="modal-body">
							 	<div class="video-container"><iframe width="853" height="480" src="{{$invitado->link}}" frameborder="0" allowfullscreen></iframe></div>


								<div class="col-md-6 w3_modal_body_left">
								@if($invitado->extension)
									<img src="{{url("/invitados/images/invitados/$invitado->id.$invitado->extension")}}" class="img-responsive">
								@endif
								</div>
								<div class="col-md-6 w3_modal_body_right">
									<h4>{{$invitado->name}}</h4>
									<p>{{$invitado->description}}</p>
								</div>
								<div class="clearfix"> </div>
							</div>
					</section>
				</div>
			</div>
		</div>
	<!-- //bootstrap-pop-up -->
	@endforeach



<div class="modal video-modal fade" id="countryModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    Representantes Nacionales
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <section>
                    <div class="modal-body">
                        <div class="col-md-12 w3_modal_body_right">
                            @foreach($contactcountrys as $contact)
                                <p>
                                    <h4>
                                    	@if($contact->country === 'peru')
					                    Perú

					                    @elseif($contact->country === 'puertorico')
					                    Puerto Rico

					                    @elseif($contact->country === 'elsalvador')
					                    El Salvador

					                    @elseif($contact->country === 'estadosunidos')
					                    Estados Unidos

					                    @elseif($contact->country === 'espana')
					                    España

					                    @elseif($contact->country === 'repdom')
					                    República Dominicana

					                    @elseif($contact->country === 'sierraleona')
					                    Sierra Leona

					                    @elseif($contact->country === 'haiti')
					                    Haití

					                    @elseif($contact->country === 'mexico')
					                    México

					                    @elseif($contact->country === 'panama')
					                    Panamá

					                    @else


					                    {{$contact->country}} 
					                    @endif
                                    	</h4>
                                {{$contact->name}} - 
                                <a href="mailto:{{$contact->mail}}?Subject=Hello" target="_top">{{$contact->mail}}</a>
                                </p>


                            @endforeach
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
<!-- contactcountrys -->












@endsection