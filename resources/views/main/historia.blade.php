@extends('layouts.app')

@section('content')
    <!-- banner -->
    <div class="banner1" style='background: url({{url("/portadas/images/portadas/$portada->id.$portada->extension")}}) no-repeat 0px -140px; background-position: 0px -83px;'>
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="navbar-header navbar-left">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                    <nav class="menu menu--iris">
                        <ul class="nav navbar-nav menu__list">
                            <li class="menu__item "><a href="{{url('/')}}" class="menu__link">Home</a></li>
                            <li class="menu__item menu__item--current"><a href="{{url('/history')}}" class="menu__link">Historia</a></li>
                            
                            <li class="menu__item"><a href="{{url('/jurado')}}" class="menu__link">Jurado</a></li>
                            <li class="menu__item"><a href="{{url('/contact')}}" class="menu__link">Contacto</a></li>
                            
                        </ul>
                    </nav>
                </div>
            </nav>
        </div>
    </div>
<!-- //banner -->   
<!-- breadcrumbs -->
    <div class="breadcrumbs">
        <div class="container">
            <div class="w3l_breadcrumbs_left">
                <ul>
                    <li><a href="{{url('/')}}">Inicio</a><i>/</i></li>
                    <li>Historia</li>
                </ul>
            </div>
            <div class="w3_agile_breadcrumbs_right">
                <h2>Historia</h2>
                <p>"La música relata la historia de los pueblos".<br>R. Cardoso</p>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
<!-- //breadcrumbs -->
<!-- events -->
    
<!-- //events -->
<!-- music -->
    <div class="about w3_music">
        <div class="container" id="Galas">
            <h3 class="agileits_w3layouts_head">Ediciones <span> Anteriores</span></h3>
            <!-- <p class="w3_agileits_para">Quisque faucibus vel leo a luctus.</p> -->
            <div class="wthree_latest_albums_grids">
                <div class="cntl"> <span class="cntl-bar cntl-center"> <span class="cntl-bar-fill"></span> </span>
                    <div class="cntl-states">
                        @foreach ($galas as $index => $gala)
                        <div class="cntl-state">
                            <div class="cntl-content">
                                <h4>{{$gala->ano}}</h4>
                                <p>
                                Ganadores
                                <br>
                                @if(is_null($gala->titulo1))

                                @endif
                                @if(is_null($gala->titulo2))

                                @endif
                                @if(is_null($gala->titulo3))

                                @endif
                                @if(is_null($gala->titulo4))

                                @endif

                                @if($gala->titulo1 ===  $gala->titulo2 && $gala->titulo1 != NULL && $gala->titulo2 != NULL)
                                    @if($gala->titulo1 === "Mejor Videoclip" || $gala->titulo1 === "Mejor Canción")
                                    
                                    
                                        <h5>Empate {{$gala->titulo1}}: </h5>

                                        <h6>

                                        @if(is_null($gala->cancion1))

                                        @else
                                            
                                            "{{$gala->cancion1}}": {{$gala->nombre1}} - &#160; <img id="img" src="images/banderaP/{{$gala->country1}}.png">
                                          
                                        

                                        @endif
                                        </h6>
                                        
                                        <h6>

                                        @if(is_null($gala->cancion2))

                                        @else
                                            
                                            "{{$gala->cancion2}}": {{$gala->nombre2}} - &#160; <img id="img" src="images/banderaP/{{$gala->country2}}.png">
                                          
                                        

                                        @endif
                                        </h6>
                                        <br>
                                    @else
                                        <h5>Empate {{$gala->titulo1}}: </h5>

                                        <h6>

                                        @if(is_null($gala->cancion1))

                                        @else
                                            
                                            {{$gala->nombre1}} - &#160; <img id="img" src="images/banderaP/{{$gala->country1}}.png">
                                          
                                        

                                        @endif
                                        </h6>
                                        
                                        <h6>

                                        @if(is_null($gala->cancion2))

                                        @else
                                            
                                            {{$gala->nombre2}} - &#160; <img id="img" src="images/banderaP/{{$gala->country2}}.png">
                                          
                                        

                                        @endif
                                        </h6>
                                        <br>

                                    @endif
                                @endif
                                @if($gala->titulo1 ===  $gala->titulo3 && $gala->titulo1 != NULL && $gala->titulo3 != NULL)
                                    @if($gala->titulo1 === "Mejor Videoclip" || $gala->titulo1 === "Mejor Canción")
                                        <h5>Empate {{$gala->titulo1}}: </h5>

                                        <h6>

                                        @if(is_null($gala->cancion1))

                                        @else
                                            
                                            "{{$gala->cancion1}}": {{$gala->nombre1}} - &#160; <img id="img" src="images/banderaP/{{$gala->country1}}.png">
                                          
                                        

                                        @endif
                                        </h6>
                                        
                                        <h6>

                                        @if(is_null($gala->cancion3))

                                        @else
                                            
                                            "{{$gala->cancion3}}": {{$gala->nombre3}} - &#160; <img id="img" src="images/banderaP/{{$gala->country3}}.png">
                                          
                                        

                                        @endif
                                        </h6>
                                        <br>
                                    @else
                                        <h5>Empate {{$gala->titulo1}}: </h5>

                                        <h6>

                                        @if(is_null($gala->cancion1))

                                        @else
                                            
                                            {{$gala->nombre1}} - &#160; <img id="img" src="images/banderaP/{{$gala->country1}}.png">
                                          
                                        

                                        @endif
                                        </h6>
                                        
                                        <h6>

                                        @if(is_null($gala->cancion3))

                                        @else
                                            
                                            {{$gala->nombre3}} - &#160; <img id="img" src="images/banderaP/{{$gala->country3}}.png">
                                          
                                        

                                        @endif
                                        </h6>
                                        <br>

                                    @endif
                                @endif
                                @if($gala->titulo1 ===  $gala->titulo4 && $gala->titulo1 != NULL && $gala->titulo4 != NULL)
                                    @if($gala->titulo1 === "Mejor Videoclip" || $gala->titulo1 === "Mejor Canción")
                                        <h5>Empate {{$gala->titulo1}}: </h5>

                                        <h6>

                                        @if(is_null($gala->cancion1))

                                        @else
                                            
                                            "{{$gala->cancion1}}": {{$gala->nombre1}} - &#160; <img id="img" src="images/banderaP/{{$gala->country1}}.png">
                                          
                                        

                                        @endif
                                        </h6>
                                        
                                        <h6>

                                        @if(is_null($gala->cancion4))

                                        @else
                                            
                                            "{{$gala->cancion4}}": {{$gala->nombre4}} - &#160; <img id="img" src="images/banderaP/{{$gala->country4}}.png">
                                          
                                        

                                        @endif
                                        </h6>
                                        <br>

                                    @else
                                        <h5>Empate {{$gala->titulo1}}: </h5>

                                        <h6>

                                        @if(is_null($gala->cancion1))

                                        @else
                                            
                                            {{$gala->nombre1}} - &#160; <img id="img" src="images/banderaP/{{$gala->country1}}.png">
                                          
                                        

                                        @endif
                                        </h6>
                                        
                                        <h6>

                                        @if(is_null($gala->cancion4))

                                        @else
                                            
                                            {{$gala->nombre4}} - &#160; <img id="img" src="images/banderaP/{{$gala->country4}}.png">
                                          
                                        

                                        @endif
                                        </h6>
                                        <br>

                                    @endif
                                @endif

                                @if($gala->titulo2 ===  $gala->titulo3 && $gala->titulo2 != NULL && $gala->titulo3 != NULL)
                                    @if($gala->titulo2 === "Mejor Videoclip" || $gala->titulo2 === "Mejor Canción")
                                        <h5>Empate {{$gala->titulo2}}: </h5>

                                        <h6>

                                        @if(is_null($gala->cancion2))

                                        @else
                                            
                                            "{{$gala->cancion2}}": {{$gala->nombre2}} - &#160; <img id="img" src="images/banderaP/{{$gala->country2}}.png">
                                          
                                        

                                        @endif
                                        </h6>
                                        
                                        <h6>

                                        @if(is_null($gala->cancion3))

                                        @else
                                            
                                            "{{$gala->cancion3}}": {{$gala->nombre3}} - &#160; <img id="img" src="images/banderaP/{{$gala->country3}}.png">
                                          
                                        

                                        @endif
                                        </h6>
                                        <br>
                                    @else
                                        <h5>Empate {{$gala->titulo2}}: </h5>

                                        <h6>

                                        @if(is_null($gala->cancion2))

                                        @else
                                            
                                            {{$gala->nombre2}} - &#160; <img id="img" src="images/banderaP/{{$gala->country2}}.png">
                                          
                                        

                                        @endif
                                        </h6>
                                        
                                        <h6>

                                        @if(is_null($gala->cancion3))

                                        @else
                                            
                                            {{$gala->nombre3}} - &#160; <img id="img" src="images/banderaP/{{$gala->country3}}.png">
                                          
                                        

                                        @endif
                                        </h6>
                                        <br>

                                    @endif
                                @endif

                                @if($gala->titulo2 ===  $gala->titulo4 && $gala->titulo2 != NULL && $gala->titulo4 != NULL)
                                    @if($gala->titulo2 === "Mejor Videoclip" || $gala->titulo2 === "Mejor Canción")
                                        <h5>Empate {{$gala->titulo2}}: </h5>

                                        <h6>

                                        @if(is_null($gala->cancion2))

                                        @else
                                            
                                            "{{$gala->cancion2}}": {{$gala->nombre2}} - &#160; <img id="img" src="images/banderaP/{{$gala->country2}}.png">
                                          
                                        

                                        @endif
                                        </h6>
                                        
                                        <h6>

                                        @if(is_null($gala->cancion4))

                                        @else
                                            
                                            "{{$gala->cancion4}}": {{$gala->nombre4}} - &#160; <img id="img" src="images/banderaP/{{$gala->country4}}.png">
                                          
                                        

                                        @endif
                                        </h6>
                                        <br>
                                    @else
                                        <h5>Empate {{$gala->titulo2}}: </h5>

                                        <h6>

                                        @if(is_null($gala->cancion2))

                                        @else
                                            
                                            {{$gala->nombre2}} - &#160; <img id="img" src="images/banderaP/{{$gala->country2}}.png">
                                          
                                        

                                        @endif
                                        </h6>
                                        
                                        <h6>

                                        @if(is_null($gala->cancion4))

                                        @else
                                            
                                            {{$gala->nombre4}} - &#160; <img id="img" src="images/banderaP/{{$gala->country4}}.png">
                                          
                                        

                                        @endif
                                        </h6>
                                        <br>

                                    @endif
                                @endif

                                @if($gala->titulo3 ===  $gala->titulo4 && $gala->titulo3 != NULL && $gala->titulo4 != NULL)
                                    @if($gala->titulo3 === "Mejor Videoclip" || $gala->titulo3 === "Mejor Canción")
                                        <h5>Empate {{$gala->titulo3}}: </h5>

                                        <h6>

                                        @if(is_null($gala->cancion3))

                                        @else
                                            
                                            "{{$gala->cancion3}}": {{$gala->nombre3}} - &#160; <img id="img" src="images/banderaP/{{$gala->country3}}.png">
                                          
                                        

                                        @endif
                                        </h6>
                                        
                                        <h6>

                                        @if(is_null($gala->cancion4))

                                        @else
                                            
                                            "{{$gala->cancion4}}": {{$gala->nombre4}} - &#160; <img id="img" src="images/banderaP/{{$gala->country4}}.png">
                                          
                                        

                                        @endif
                                        </h6>
                                        <br>
                                    @else
                                        <h5>Empate {{$gala->titulo3}}: </h5>

                                        <h6>

                                        @if(is_null($gala->cancion3))

                                        @else
                                            
                                            {{$gala->nombre3}} - &#160; <img id="img" src="images/banderaP/{{$gala->country3}}.png">
                                          
                                        

                                        @endif
                                        </h6>
                                        
                                        <h6>

                                        @if(is_null($gala->cancion4))

                                        @else
                                            
                                            {{$gala->nombre4}} - &#160; <img id="img" src="images/banderaP/{{$gala->country4}}.png">
                                          
                                        

                                        @endif
                                        </h6>
                                        <br>

                                    @endif


                                @endif

                                @if($gala->titulo1  !== $gala->titulo2 && $gala->titulo1  !== $gala->titulo3 && $gala->titulo2  !== $gala->titulo3 && $gala->titulo1  !== $gala->titulo4 && $gala->titulo2  !== $gala->titulo4)
                                    @if(is_null($gala->titulo1))

                                    @else
                                        <h5>{{$gala->titulo1}}: 

                                        @if(is_null($gala->cancion1))

                                        @else
                                            
                                            "{{$gala->cancion1}}"
                                            
                                        

                                        @endif
                                        </h5>
                                        <h6>{{$gala->nombre1}} - &#160; <img id="img" src="images/banderaP/{{$gala->country1}}.png">
                                        </h6>

                                        <br>

                                    @endif
                                    @if(is_null($gala->titulo2))

                                    @else
                                        <h5>{{$gala->titulo2}}: 
                                        @if(is_null($gala->cancion2))

                                        @else
                                            
                                            "{{$gala->cancion2}}"
                                            

                                        @endif

                                        </h5>
                                        <h6>{{$gala->nombre2}} - &#160; <img id="img" src="images/banderaP/{{$gala->country2}}.png">
                                        </h6>

                                        <br>

                                    @endif
                                    @if(is_null($gala->titulo3))

                                    @else
                                        <h5>{{$gala->titulo3}}: 
                                        @if(is_null($gala->cancion3))

                                        @else
                                            
                                            "{{$gala->cancion3}}"
                                            

                                        @endif

                                        </h5>
                                        <h6>{{$gala->nombre3}} - &#160; <img id="img" src="images/banderaP/{{$gala->country3}}.png"></h6>
                                        <br>

                                    @endif
                                    @if(is_null($gala->titulo4))

                                    @else
                                        <h5>{{$gala->titulo4}}: 
                                        @if(is_null($gala->cancion4))

                                        @else
                                            
                                            "{{$gala->cancion4}}"
                                            

                                        @endif

                                        </h5>
                                        <h6>{{$gala->nombre4}} - &#160; <img id="img" src="images/banderaP/{{$gala->country4}}.png"></h6>
                                        <br>

                                    @endif
                                    

                                    

                                    

                                @endif

                                </p>
                                

                                
                            </div>
                            <div class="cntl-image">
                            	<div class="video-container"><iframe width="853" height="480" src="{{$gala->link}}" frameborder="0" allowfullscreen></iframe></div>
                                
                            </div>
                            <div class="cntl-icon cntl-center">{{$index + 1}}</div>
                        </div>

                        @endforeach     
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- //music -->
<div class="modal video-modal fade" id="countryModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    Representantes Nacionales
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <section>
                    <div class="modal-body">
                        <div class="col-md-12 w3_modal_body_right">
                            @foreach($contactcountrys as $contact)
                                <p>
                                    <h4>
                                        @if($contact->country === 'peru')
                                        Perú

                                        @elseif($contact->country === 'puertorico')
                                        Puerto Rico

                                        @elseif($contact->country === 'elsalvador')
                                        El Salvador

                                        @elseif($contact->country === 'estadosunidos')
                                        Estados Unidos

                                        @elseif($contact->country === 'espana')
                                        España

                                        @elseif($contact->country === 'repdom')
                                        República Dominicana

                                        @elseif($contact->country === 'sierraleona')
                                        Sierra Leona

                                        @elseif($contact->country === 'haiti')
                                        Haití

                                        @elseif($contact->country === 'mexico')
                                        México

                                        @elseif($contact->country === 'panama')
                                        Panamá

                                        @else


                                        {{$contact->country}} 
                                        @endif
                                        </h4>
                                {{$contact->name}} - 
                                <a href="mailto:{{$contact->mail}}?Subject=Hello" target="_top">{{$contact->mail}}</a>
                                </p>


                            @endforeach
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
<!-- contactcountrys -->
<!-- music-bottom -->
    
<!-- //music-bottom -->
<script type="text/javascript" src="js/jquery.cntl.js"></script> 
	<script type="text/javascript">
		$(document).ready(function(e){
			$('.cntl').cntl({
				revealbefore: 300,
				anim_class: 'cntl-animate',
				onreveal: function(e){
					console.log(e);
				}
			});
		});
	</script>


@endsection