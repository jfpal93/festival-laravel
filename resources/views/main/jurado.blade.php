@extends('layouts.app')

@section('content')
<!-- banner -->
	<div class="banner1" style='background: url({{url("/portadas/images/portadas/$portada->id.$portada->extension")}}) no-repeat 0px -140px; background-position: 0px -83px;'>
		<div class="container">
			<nav class="navbar navbar-default">
				<div class="navbar-header navbar-left">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
					<nav class="menu menu--iris">
						<ul class="nav navbar-nav menu__list">
							<li class="menu__item "><a href="{{url('/')}}" class="menu__link">Home</a></li>
							<li class="menu__item"><a href="{{url('/history')}}" class="menu__link">Historia</a></li>
                            <li class="menu__item menu__item--current"><a href="{{url('/jurado')}}" class="menu__link">Jurado</a></li>
                            <li class="menu__item"><a href="{{url('/contact')}}" class="menu__link">Contacto</a></li>
						</ul>
					</nav>
				</div>
			</nav>
		</div>
	</div>
<!-- //banner -->	
<!-- breadcrumbs -->
	<div class="breadcrumbs">
		<div class="container">
			<div class="w3l_breadcrumbs_left">
				<ul>
					<li><a href="{{url('/')}}">Inicio</a><i>/</i></li>
					<li>Jurados</li>
				</ul>
			</div>
			<div class="w3_agile_breadcrumbs_right">
				<h2>Jurados</h2>
				<p>"La música es lenguaje que todos entendemos".<br> Stevie Wonder.</p>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
<!-- //breadcrumbs -->
<!-- events -->
	<div class="events">
		<div class="container" id="Destacado">
			<h3 class="agileits_w3layouts_head">Jurado <span>Mejor Canción</span></h3>
				@foreach (array_chunk($jurados->all(), 3) as $juradosRow)
			<div class="container">
				<div class="wthree_latest_albums_grids">
					
						@foreach($juradosRow as $jurado)
							@if($jurado->tipo==='participante')
								<div class="col-md-4 w3layouts_events_grid">
									@if($jurado->extension)
					                    <img src="{{url("/jurados/images/jurados/$jurado->id.$jurado->extension")}}" class="img-responsive">

					                @endif
									<br>
			                		<h4><a href="#" data-toggle="modal" data-target="#myModal">{{$jurado->name}}</a></h4>
			                		<p class="capitalize">
			                		@if($jurado->country === 'peru')
				                    Perú

				                    @elseif($jurado->country === 'puertorico')
				                    Puerto Rico

				                    @elseif($jurado->country === 'elsalvador')
				                    El Salvador

				                    @elseif($jurado->country === 'estadosunidos')
				                    Estados Unidos

				                    @elseif($jurado->country === 'espana')
				                    España

				                    @elseif($jurado->country === 'repdom')
				                    República Dominicana

				                    @elseif($jurado->country === 'sierraleona')
				                    Sierra Leona

				                    @elseif($jurado->country === 'haiti')
				                    Haití

				                    @elseif($jurado->country === 'mexico')
				                    México

				                    @else


				                    {{$jurado->country}} 
				                    @endif

			                		 - &#160; <img id="img" src="images/banderaP/{{$jurado->country}}.png"> </p>
									<p>
										@if(is_null($jurado->descripcion_corta))

										@else
											{{$jurado->descripcion_corta}}
										@endif

									</p>
									
									@if(is_null($jurado->descripcion_corta))
									<div class="w3_more">
											<a href="#" data-toggle="modal" data-target="#Jurado{{$jurado->id}}">Leer Más</a>
										</div>
										<br>
										<br>
										<br>
										<br>
										<br>
										<br>
										<br>
									@else
										<div class="w3_more">
											<a href="#" data-toggle="modal" data-target="#Jurado{{$jurado->id}}">Leer Más</a>
										</div>

									@endif
								</div>
					 		@endif
						@endforeach


					
				</div>
			</div>

			@endforeach
     	</div>
	</div>
<!-- //events -->
<!-- events -->
	<div class="events">
		<div class="container" id="Destacado">
			<h3 class="agileits_w3layouts_head">Jurado <span>Mejor VideoClip</span></h3>
				@foreach (array_chunk($juradosvideo->all(), 3) as $juradosRow)
			<div class="container">
				<div class="wthree_latest_albums_grids">
					
						@foreach($juradosRow as $jurado)
							@if($jurado->tipo==='video')
								<div class="col-md-4 w3layouts_events_grid">
									@if($jurado->extension)
					                    <img src="{{url("/jurados/images/jurados/$jurado->id.$jurado->extension")}}" class="img-responsive">

					                @endif
									<br>
			                		<h4><a href="#" data-toggle="modal" data-target="#myModal">{{$jurado->name}}</a></h4>
			                		<p class="capitalize">
			                		@if($jurado->country === 'peru')
				                    Perú

				                    @elseif($jurado->country === 'puertorico')
				                    Puerto Rico

				                    @elseif($jurado->country === 'elsalvador')
				                    El Salvador

				                    @elseif($jurado->country === 'estadosunidos')
				                    Estados Unidos

				                    @elseif($jurado->country === 'espana')
				                    España

				                    @elseif($jurado->country === 'repdom')
				                    República Dominicana

				                    @elseif($jurado->country === 'sierraleona')
				                    Sierra Leona

				                    @elseif($jurado->country === 'haiti')
				                    Haití

				                    @elseif($jurado->country === 'mexico')
				                    México

				                    @else


				                    {{$jurado->country}} 
				                    @endif - &#160; <img id="img" src="images/banderaP/{{$jurado->country}}.png"> </p>
									<p>
										@if(is_null($jurado->descripcion_corta))

										@else
											{{$jurado->descripcion_corta}}
										@endif

									</p>
									
									@if(is_null($jurado->descripcion_corta))
									<div class="w3_more">
											<a href="#" data-toggle="modal" data-target="#Jurado{{$jurado->id}}">Leer Más</a>
										</div>
										<br>
										<br>
										<br>
										<br>
										<br>
										<br>
										<br>
									@else
										<div class="w3_more">
											<a href="#" data-toggle="modal" data-target="#Jurado{{$jurado->id}}">Leer Más</a>
										</div>

									@endif
								</div>
					 		@endif
						@endforeach


					
				</div>
			</div>

			@endforeach
     	</div>
	</div>
<!-- //events -->
<!-- bootstrap-pop-up -->
<!-- {{$jurado->name}} -->
	@foreach($jurados as $jurado)
		<div class="modal video-modal fade" id="Jurado{{$jurado->id}}" tabindex="-1" role="dialog" aria-labelledby="myModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						{{$jurado->name}}
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
					</div>
					<section>
						<div class="modal-body">

							<div class="col-md-6 w3_modal_body_left">
							@if($jurado->extension)
								<img src="{{url("/jurados/images/jurados/$jurado->id.$jurado->extension")}}" class="img-responsive">
							@endif
							</div>
							<div class="col-md-6 w3_modal_body_right">
								<h4>Biografía</h4>
								<p><i>{{$jurado->name}}</i>{{$jurado->description}}</p>
							</div>
							<div class="clearfix"> </div>
						</div>
					</section>
				</div>
			</div>
		</div>
	<!-- //bootstrap-pop-up -->
	@endforeach

	@foreach($juradosvideo as $juradoVideo)
		<div class="modal video-modal fade" id="Jurado{{$juradoVideo->id}}" tabindex="-1" role="dialog" aria-labelledby="myModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						{{$juradoVideo->name}}
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
					</div>
					<section>
						<div class="modal-body">

							<div class="col-md-6 w3_modal_body_left">
							@if($juradoVideo->extension)
								<img src="{{url("/jurados/images/jurados/$juradoVideo->id.$juradoVideo->extension")}}" class="img-responsive">
							@endif
							</div>
							<div class="col-md-6 w3_modal_body_right">
								<h4>Biografía</h4>
								<p><i>{{$juradoVideo->name}}</i>{{$juradoVideo->description}}</p>
							</div>
							<div class="clearfix"> </div>
						</div>
					</section>
				</div>
			</div>
		</div>
	<!-- //bootstrap-pop-up -->
	@endforeach

	<div class="modal video-modal fade" id="countryModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    Representantes Nacionales
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <section>
                    <div class="modal-body">
                        <div class="col-md-12 w3_modal_body_right">
                            @foreach($contactcountrys as $contact)
                                <p>
                                    <h4>
                                    	@if($contact->country === 'peru')
					                    Perú

					                    @elseif($contact->country === 'puertorico')
					                    Puerto Rico

					                    @elseif($contact->country === 'elsalvador')
					                    El Salvador

					                    @elseif($contact->country === 'estadosunidos')
					                    Estados Unidos

					                    @elseif($contact->country === 'espana')
					                    España

					                    @elseif($contact->country === 'repdom')
					                    República Dominicana

					                    @elseif($contact->country === 'sierraleona')
					                    Sierra Leona

					                    @elseif($contact->country === 'haiti')
					                    Haití

					                    @elseif($contact->country === 'mexico')
					                    México

					                    @elseif($contact->country === 'panama')
					                    Panamá

					                    @else


					                    {{$contact->country}} 
					                    @endif
                                    	</h4>
                                {{$contact->name}} - 
                                <a href="mailto:{{$contact->mail}}?Subject=Hello" target="_top">{{$contact->mail}}</a>
                                </p>


                            @endforeach
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
<!-- contactcountrys -->

@endsection