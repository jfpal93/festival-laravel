@extends('layouts.app')

@section('content')
    <!-- banner -->
    <div class="banner1" style='background: url({{url("/portadas/images/portadas/$portada->id.$portada->extension")}}) no-repeat 0px -140px; background-position: 0px -83px;'>
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="navbar-header navbar-left">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                    <nav class="menu menu--iris">
                        <ul class="nav navbar-nav menu__list">
                            <li class="menu__item "><a href="{{url('/')}}" class="menu__link">Home</a></li>
                            <li class="menu__item "><a href="{{url('/history')}}" class="menu__link">Historia</a></li>
                            
                            <li class="menu__item"><a href="{{url('/jurado')}}" class="menu__link">Jurado</a></li>
                            
                            <li class="menu__item menu__item--current"><a href="{{url('/contact')}}" class="menu__link">Contacto</a></li>
                        </ul>
                    </nav>
                </div>
            </nav>
        </div>
    </div>
<!-- //banner -->   
<!-- breadcrumbs -->
    <div class="breadcrumbs">
        <div class="container">
            <div class="w3l_breadcrumbs_left">
                <ul>
                    <li><a href="{{url('/')}}">Inicio</a><i>/</i></li>
                    <li>Contactos</li>
                </ul>
            </div>
            
            <div class="clearfix"> </div>
        </div>
    </div>
<!-- //breadcrumbs -->

<!-- mail -->
<div class="latest-albums">
        <div class="container">
            <h3 class="agileits_w3layouts_head"><span>Contactos</span></h3>
            <div class="wthree_latest_albums_grids gallery">
                <div class="col-md-8 agile_mail_grid_left">
               
                   

                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>

                    {!! Form::open(array('route' => 'contact_store')) !!}

                    <div class="form-group">
                        
                        {!! Form::text('name', null, 
                            array('required', 
                                  'placeholder'=>'Nombres','type'=>'text')) !!}
                    </div>

                    <div class="form-group">
                        
                        {!! Form::text('email', null, 
                            array('required', 
                                  
                                  'placeholder'=>'Correo Electronico','type'=>'email')) !!}
                    </div>

                    <div class="form-group">
                       
                        {!! Form::textarea('message', null, 
                            array('required', 
                                  
                                  'placeholder'=>'Mensaje','type'=>'text')) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::submit('Enviar', 
                          array('class'=>'btn btn-primary')) !!}
                    </div>
                    {!! Form::close() !!}

                
                
                
                </div>
                
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
<!-- //mail -->

<div class="modal video-modal fade" id="countryModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    Representantes Nacionales
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <section>
                    <div class="modal-body">
                        <div class="col-md-12 w3_modal_body_right">
                            @foreach($contactcountrys as $contact)
                                <p>
                                    <h4>
                                        @if($contact->country === 'peru')
                                        Perú

                                        @elseif($contact->country === 'puertorico')
                                        Puerto Rico

                                        @elseif($contact->country === 'elsalvador')
                                        El Salvador

                                        @elseif($contact->country === 'estadosunidos')
                                        Estados Unidos

                                        @elseif($contact->country === 'espana')
                                        España

                                        @elseif($contact->country === 'repdom')
                                        República Dominicana

                                        @elseif($contact->country === 'sierraleona')
                                        Sierra Leona

                                        @elseif($contact->country === 'haiti')
                                        Haití

                                        @elseif($contact->country === 'mexico')
                                        México

                                        @elseif($contact->country === 'panama')
                                        Panamá

                                        @else


                                        {{$contact->country}} 
                                        @endif


                                        </h4>
                                {{$contact->name}}
                                <a href="mailto:{{$contact->mail}}?Subject=Hello" target="_top">Enviar Correo</a>
                                </p>


                            @endforeach
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
<!-- contactcountrys -->

<div class="modal video-modal fade" id="countryModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    Representantes Nacionales
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <section>
                    <div class="modal-body">
                        <div class="col-md-12 w3_modal_body_right">
                            @foreach($contactcountrys as $contact)
                                <p>
                                    <h4>
                                        @if($contact->country === 'peru')
                                        Perú

                                        @elseif($contact->country === 'puertorico')
                                        Puerto Rico

                                        @elseif($contact->country === 'elsalvador')
                                        El Salvador

                                        @elseif($contact->country === 'estadosunidos')
                                        Estados Unidos

                                        @elseif($contact->country === 'espana')
                                        España

                                        @elseif($contact->country === 'repdom')
                                        República Dominicana

                                        @elseif($contact->country === 'sierraleona')
                                        Sierra Leona

                                        @elseif($contact->country === 'haiti')
                                        Haití

                                        @elseif($contact->country === 'mexico')
                                        México

                                        @elseif($contact->country === 'panama')
                                        Panamá

                                        @else


                                        {{$contact->country}} 
                                        @endif
                                        </h4>
                                {{$contact->name}} - 
                                <a href="mailto:{{$contact->mail}}?Subject=Hello" target="_top">{{$contact->mail}}</a>
                                </p>


                            @endforeach
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </section>
            </div>
        </div>
    </div>



@endsection