<!DOCTYPE html>
<html lang="en">
<head>
<title>Festival de la Canción</title>
<link rel="icon" href="images/logo/small-logo.png" type="image/png">
<!-- custom-theme -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="keywords" content="Festival de la canción, Punta del Este, Canción, El Festival Internacional de la Canción, Uruguay, La costa atlántica seduce con su encantamiento" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //custom-theme -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/app.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<!-- font-awesome-icons -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome-icons -->
<!-- <link href="//fonts.googleapis.com/css?family=Sofia" rel="stylesheet"> -->
<link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
<!-- <link href="https://fonts.googleapis.com/css?family=IM+Fell+Great+Primer:400i" rel="stylesheet"> -->
<link href="//fonts.googleapis.com/css?family=Prompt:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=latin-ext,thai,vietnamese" rel="stylesheet">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-85286834-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-85286834-3');
</script>

</head>
    
<body>
    

    @yield('content')
    <!-- bootstrap-pop-up -->
    <div class="modal video-modal fade" id="editorialModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    Editorial
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                        
                </div>
                <section>
                    <div class="modal-body">
                        <div class="col-md-12 w3_modal_body_right">
                            <h4>HACIENDO AMIGOS</h4>

                            <p><i>" La música es mi religión." Jimi Hendrix</i></p>

                            <p>La costa atlántica seduce con su encantamiento... Pocos saben que antes de la presencia europea en este continente, la Cruz del Sur amparaba este enclave marino. Entonces era llamada por los nativos: “la tierra de las peregrinaciones” o “la tierra sin males”… Los habitantes de los pueblos montañosos del norte, soñaban con la tierra tibia donde pudieran ver el mar.</p>

                            <p>
                                Por otro lado, los nativos de la selva, buscaban ver el sol, oculto por su tupida vegetación. De modo que desde el norte se trazó, hace miles de años la ruta de las peregrinaciones hacia este litoral marino… 
                            </p>
                            <p>
                                Rumbo a la VI Edición del Festival Internacional de la Canción, nos ponemos en marcha nuevamente y damos la bienvenida a Autores, Compositores e Intérpretes que comparten este mágico encuentro bajo la Cruz del Sur… 
                            </p>
                            <p>
                                Año a año, sumamos protagonistas de los cinco continentes que con entusiasmo por la música enriquecen esta propuesta que recorre países y afectos
                            </p>
                            <p>
                                Los artistas se suman con su música, su poesía y su arte a construir este encuentro de sensibilidad y amor por esa pasión que hermana… 
                            </p>
                            <p>
                                Los Directores Nacionales y organización agregan todo su esfuerzo y perfeccionismo para que ese soporte abrace al arte y lo convierta en hechos visibles y contundentes. 
                            </p>
                            <p>
                                Los patrocinadores construyen esa red esencial de apoyo para que el encuentro tenga la comodidad y amabilidad  que ofrecemos como anfitriones. 
                            </p>

                            <p>
                                Reiteramos, como en todas las editoriales que la música nos hermana, que la paz se construye con estos proyectos que trascienden a sus creadores, y perduran en el tiempo... 
                            </p>

                            <p>
                                Cada Edición suma novedades, nuevos respaldos de instituciones públicas y organizaciones privadas que de una forma u otra nuclean a los verdaderos protagonistas del Festival: Autores, Compositores e Intérpretes. 
                            </p>
                            <p>
                                Desde Uruguay nuestra más cálida bienvenida a quienes nos visitan con motivo del Festival Internacional de la Canción - Punta del Este 2017
                            </p>
                            <br>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            
                                        </div>
                                    </div>
                                    <div class="user-content">
                                        <img src="images/index/barrios.png" class="img-circle img-responsive" alt="img">
                                        <h3>Heber I. Barrios</h4>
                                        <h4>Director General</h5> 
                                    </div>
                                </div>
                            
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
<!-- //bootstrap-pop-up -->
    <!-- footer -->
    <div class="footer">
        <div class="container">
            <div class="col-md-4 agileinfo_footer_grid">
                <h3>Editorial</h3>
                <p>La costa atlántica seduce con su encantamiento... Pocos saben que antes de la presencia europea en este continente, la Cruz del Sur amparaba este enclave marino. Entonces era llamada por los nativos: “la tierra de las peregrinaciones” o “la tierra sin males”…</p>
                <a href="#" data-toggle="modal" data-target="#editorialModal">Leer Más.</a>
                
            </div>
            <div class="col-md-4 agileinfo_footer_grid">
                <h3>Anexos</h3>
                <ul class="w3agile_footer_grid_list">
                    <li>
                        <span>
                            <a href="archives/Reglamento_Festival_de_la_Cancion.pdf" target="_blank">
                                <i class="fa fa-download" aria-hidden="true"></i>
                            </a>
                            Reglamentos
                        </span>
                    </li> 
                        
                    <li>
                        <span>
                            <a href="archives/Historial_participantes_Festival.pdf" target="_blank">
                                <i class="fa fa-download" aria-hidden="true"></i>
                            </a>
                            Historial de participantes
                        </span>
                    </li> 
                </ul>
            </div>
            <div class="col-md-4 agileinfo_footer_grid">
                <h3>Representantes Nacionales</h3>
                <p class="agileinfo_social_icons">
                    Si quiere contactarse con los representantes nacionales, haga click <a href="#" data-toggle="modal" data-target="#countryModal">aquí</a>.
                </p>
                <h3>¡Síguenos en las redes!</h3>
                <ul class="agileinfo_social_icons">
                    <li><a href="https://www.facebook.com/festivalPDE" class="facebook" target="_blank"><span class="fa fa-facebook" aria-hidden="true"></span><i>-</i>Facebook</a></li>
                    <li><a href="https://twitter.com/FestivalPde" class="twitter" target="_blank"><span class="fa fa-twitter" aria-hidden="true"></span><i>-</i>Twitter</a></li>
                    <li><a href="https://www.youtube.com/channel/UCEvhk3jkG-ygYreacqhZfJw" class="google" target="_blank"><span class="fa fa-youtube-play" aria-hidden="true"></span><i>-</i>YouTube</a></li>
                    
                </ul>
            </div>
        </div>
    </div>
<!-- //footer -->
<!-- copy-right -->
    <div class="w3agile_copy_right">
        <div class="container">
            <div class="footer-logo"><a href="#"><img src="images/logo/footer-logo.png" alt=""></a></div>
        <span class="copyright">Copyright © 2017 | Festival de la Canción. Powered by <a href="http://machincorp.com/">MachínCorp</a> - ECUADOR</span>
        
        </div>
    </div>
<!-- //copy-right -->


    

    <!-- Scripts -->
    <!-- flexisel -->
    <script type="text/javascript">
            $(window).load(function() {
                $("#flexiselDemo1").flexisel({
                    visibleItems: 3,
                    animationSpeed: 1000,
                    autoPlay: true,
                    autoPlaySpeed: 3000,            
                    pauseOnHover: true,
                    enableResponsiveBreakpoints: true,
                    responsiveBreakpoints: { 
                        portrait: { 
                            changePoint:480,
                            visibleItems: 1
                        }, 
                        landscape: { 
                            changePoint:640,
                            visibleItems:2
                        },
                        tablet: { 
                            changePoint:768,
                            visibleItems: 2
                        }
                    }
                });
                
            });
    </script>
    <script type="text/javascript" src="js/jquery.flexisel.js"></script>
<!-- //flexisel -->
<!-- start-smooth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){     
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<!-- start-smooth-scrolling -->
<!-- for bootstrap working -->
    <script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<!-- here stars scrolling icon -->
    <script type="text/javascript">
        $(document).ready(function() {
            
                var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear' 
                };
            
                                
            $().UItoTop({ easingType: 'easeOutQuart' });
                                
            });
    </script>
    
<!-- //here ends scrolling icon -->
</body>
</html>
