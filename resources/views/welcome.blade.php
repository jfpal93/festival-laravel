@extends('layouts.app')

@section('content')
    <!-- banner -->
    <div class="banner">
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="navbar-header navbar-left">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <h1><a class="navbar-brand" href="index.html"><img src="images/logo/logo.png" alt=""></a></h1>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                    <nav class="menu menu--iris">
                        <ul class="nav navbar-nav menu__list">
                            <li class="menu__item menu__item--current"><a href="index.html" class="menu__link">Home</a></li>
                            <li class="dropdown menu__item">
                                <a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown">Historia <b class="caret"></b></a>
                                <ul class="dropdown-menu agile_short_dropdown">
                                    <li><a href="historia.html#Galas">Galas Anteriores</a></li>
                                    <li><a href="historia.html#Destacado">Destacado</a></li>
                                </ul>
                            </li>
                            <li class="menu__item"><a href="gallery.html" class="menu__link">Gallery</a></li>
                            <!-- <li class="dropdown menu__item">
                                <a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown">Short Codes <b class="caret"></b></a>
                                <ul class="dropdown-menu agile_short_dropdown">
                                    <li><a href="icons.html">Web Icons</a></li>
                                    <li><a href="typography.html">Typography</a></li>
                                </ul>
                            </li> -->
                            <li class="menu__item"><a href="mail.html" class="menu__link">Mail Us</a></li>
                        </ul>
                    </nav>
                </div>
            </nav>
            <div class="agile_banner_info">
                <h3 align="center">FESTIVAL INTERNACIONAL <br> DE LA CANCIÓN</h3>
                <div class="agile_banner_info_pos">
                </div>
            </div>
        </div>
    </div>
<!-- //banner -->   
<!-- banner-bottom -->
    <div class="banner-bottom">
        <div class="col-md-3 wthree_banner_bottom_grid">
            <div class="wthree_banner_bottom_grid1">
                <img src="images/1.jpg" alt=" " class="img-responsive" />
                <div class="wthree_banner_bottom_grid_pos">
                    <h4>Punta del Este </h4>
                </div>
            </div>
            <div class="w3layouts_banner_bottom_grid">
                <h3 align="center">Sede</h3>
            </div>
        </div>
        <div class="col-md-3 agileits_w3layouts_banner_bottom_grid">
            <div class="hovereffect">
                <img src="images/2.jpg" alt=" " class="img-responsive" />
                <div class="overlay">
                    <!--<h3 class="w3_instruments">Invitados</h3>-->
                    <div class="rotate">
                        <p class="group1">
                            <a href="#">
                                <i class="fa fa-twitter"></i>
                            </a>
                            <a href="#">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </p>
                            <hr>
                            <hr>
                        <p class="group2">
                            <a href="#">
                                <i class="fa fa-instagram"></i>
                            </a>
                            <a href="#">
                                <i class="fa fa-dribbble"></i>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="agileinfo_banner_bottom_grid">
                <div class="agileits_banner_bottom_grid1">
                    <!--<h4 class="w3ls_color">Invitados</h4>-->
                    <h3>Artistas Invitados</h3>
                    <p>Los mejores interpretes de la musica, en el mejor lugar como siempre en el Festival Internacional de la Canción.</p>
                </div>
            </div>
        </div>
        <div class="col-md-3 agileits_w3layouts_banner_bottom_grid">
            <div class="hovereffect">
                <img src="images/3.jpg" alt=" " class="img-responsive" />
                <div class="overlay">
                    <!--<h3 class="w3_instruments">Music Instruments</h3>-->
                    <div class="rotate">
                        <p class="group1">
                            <a href="#">
                                <i class="fa fa-twitter"></i>
                            </a>
                            <a href="#">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </p>
                            <hr>
                            <hr>
                        <p class="group2">
                            <a href="#">
                                <i class="fa fa-instagram"></i>
                            </a>
                            <a href="#">
                                <i class="fa fa-dribbble"></i>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="agileinfo_banner_bottom_grid w3l_banner_bottom1">
                <div class="agileits_banner_bottom_grid1">
                    <!--<h4 class="w3ls_color1"></h4>-->
                    <h3>Artistas Participantes</h3>
                    <p>Los mayores exponentes de America, Europa, Africa. Reunidos en un solo lugar en el Festival Internacional de la Canción .</p>
                </div>
            </div>
        </div>
        <div class="col-md-3 agileits_w3layouts_banner_bottom_grid">
            <div class="hovereffect">
                <img src="images/4.jpg" alt=" " class="img-responsive" />
                <div class="overlay">
                    <!--<h3 class="w3_instruments">Music Instruments</h3>-->
                    <div class="rotate">
                        <p class="group1">
                            <a href="#">
                                <i class="fa fa-twitter"></i>
                            </a>
                            <a href="#">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </p>
                            <hr>
                            <hr>
                        <p class="group2">
                            <a href="#">
                                <i class="fa fa-instagram"></i>
                            </a>
                            <a href="#">
                                <i class="fa fa-dribbble"></i>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="agileinfo_banner_bottom_grid w3l_banner_bottom2">
                <div class="agileits_banner_bottom_grid1">
                    <!--<h4 class="w3ls_color2">Symphony</h4>-->
                    <h3>Sala Cantegril</h3>
                    <p>Praesent suscipit nunc vel orci dictum pretium. Donec ullamcorper sagittis turpis.</p>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
<!-- //banner-bottom -->
<!-- about -->
    <div class="about">
        <div class="container">
            <div class="w3_agile_about_grids">
                <div class="col-md-6 w3_agile_about_grid_left">
                    <img src="images//logo/logo.png" alt=" " class="img-responsive" />
                </div>
                <div class="col-md-6 w3_agile_about_grid_right">
                    <h3>FESTIVAL INTERNACIONAL DE LA CANCIÓN</h3>
                    <!--<h4>Music Album</h4>-->
                    <p>El Festival Internacional de la Canción - Punta del Este 2017 <br>
                    es un evento diseñado para promover creaciones inéditas en el campo de la música y como
                    dar a conocer nuevos intérpretes y compositores.<br>
                        <span>Es un espacio de difusión de la cultura de los pueblos y el
                    posicionamiento de los talentos que fueron forjados a través de singulares
                    procesos de materialización de la identidad nacional.<br>
                    Es una vitrina donde las fronteras desaparecen y se logra aunar esfuerzos
                    con instituciones públicas y privadas para conocer, fomentar y difundir la
                    música nacional e internacional.</span></p>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
<!-- //about -->
<!-- features -->
    <div class="features">
        <div class="container">
            <div class="col-md-6 agile_features_left">
                <img src="images/index/cronograma.jpg" alt=" " class="img-responsive" />
            </div>
            <div class="col-md-6 agile_features_right">
                <p>FESTIVAL INTERNACIONAL DE LA CANCIÓN</p>
                <h4>Cronograma</h4>
                <ul>
                    <li><span>Monday</span>: 10:00 AM - 5:00 PM</li>
                    <li><span>Tuesday</span>: 10:00 AM - 5:00 PM</li>
                    <li><span>Wednesday</span>: 10:00 AM - 5:00 PM</li>
                    <li><span>Saturday</span>: 8:00 AM - 3:00 PM</li>
                    <li><span>Sunday</span>: Closed</li>
                </ul>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
<!-- //features -->
<!-- newsletter -->
<!--    <div class="newsletter">
        <div class="container">
            <div class="col-md-6 w3_agile_newsletter_left">
                <p>Subscribe Newsletter</p>
            </div>
            <div class="col-md-6 w3_agile_newsletter_right">
                <form action="#" method="post">
                    <input type="email" name="Email" placeholder="Email..." required="">
                    <input type="submit" value="Send">
                </form>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>-->
<!-- //newsletter -->
<!-- team -->
    <div class="team">
        <div class="col-md-3 agile_team_left">
            <h3>Invitados</h3>
        </div>
        <div class="col-md-9 agile_team_grid">  
            <ul id="flexiselDemo1"> 
                <!-- <li>
                    <div class="hovereffect1 w3ls_banner_bottom_grid">
                        <img src="images/artisaIn/cigoi.jpg" alt=" " class="img-responsive" />
                        <div class="overlay">
                            <h4>Cindy Goméz</h4>
                            <ul class="social_agileinfo">
                                <li><a href="#" class="w3_facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" class="w3_twitter"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" class="w3_instagram"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#" class="w3_google"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="hovereffect1 w3ls_banner_bottom_grid">
                        <img src="images/artisaIn/raulci.jpg" alt=" " class="img-responsive" />
                        <div class="overlay">
                            <h4>Raúl Cela</h4>
                            <ul class="social_agileinfo">
                                <li><a href="#" class="w3_facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" class="w3_twitter"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" class="w3_instagram"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#" class="w3_google"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="hovereffect1 w3ls_banner_bottom_grid">
                        <img src="images/artisaIn/oscari.jpg" alt=" " class="img-responsive" />
                        <div class="overlay">
                            <h4>Oscar Caballero</h4>
                            <ul class="social_agileinfo">
                                <li><a href="#" class="w3_facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" class="w3_twitter"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" class="w3_instagram"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#" class="w3_google"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="hovereffect1 w3ls_banner_bottom_grid">
                        <img src="images/9.jpg" alt=" " class="img-responsive" />
                        <div class="overlay">
                            <h4>Javier Santivañez</h4>
                            <ul class="social_agileinfo">
                                <li><a href="#" class="w3_facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" class="w3_twitter"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" class="w3_instagram"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#" class="w3_google"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </li> -->
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
<!-- //team -->
<!-- latest-albums -->
    <div class="latest-albums">
        <div class="container">
            <h3 class="agileits_w3layouts_head">Our Latest released <span>albums</span></h3>
            <p class="w3_agileits_para">Quisque faucibus vel leo a luctus.</p>
            <div class="wthree_latest_albums_grids">
                <div class="col-md-4 wthree_latest_albums_grid_left">
                    <figure class="effect-julia">
                        <img src="images/14.jpg" alt=" " class="img-responsive" />
                        <figcaption>
                            <div class="w3l_banner_figure">
                                <p>Ut ac gravida quam id ornare</p>
                                <p>Nullam imperd scelerisque ullamcorper</p>
                                <p>Praesent pellentesque neque feugiat</p>
                            </div>
                        </figcaption>           
                    </figure>
                </div>
                <div class="col-md-4 wthree_latest_albums_grid_left">
                    <figure class="effect-julia">
                        <img src="images/13.jpg" alt=" " class="img-responsive" />
                        <figcaption>
                            <div class="w3l_banner_figure">
                                <p>Ut ac gravida quam id ornare</p>
                                <p>Nullam imperd scelerisque ullamcorper</p>
                                <p>Praesent pellentesque neque feugiat</p>
                            </div>
                        </figcaption>           
                    </figure>
                </div>
                <div class="col-md-4 wthree_latest_albums_grid_left">
                    <figure class="effect-julia">
                        <img src="images/15.jpg" alt=" " class="img-responsive" />
                        <figcaption>
                            <div class="w3l_banner_figure">
                                <p>Ut ac gravida quam id ornare</p>
                                <p>Nullam imperd scelerisque ullamcorper</p>
                                <p>Praesent pellentesque neque feugiat</p>
                            </div>
                        </figcaption>           
                    </figure>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
<!-- //latest-albums -->
<!-- footer -->
    <div class="footer">
        <div class="container">
            <div class="col-md-4 agileinfo_footer_grid">
                <h3>About Us</h3>
                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <div class="agileits_footer_grid_gallery">
                    <div class="agileits_footer_grid_gallery1">
                        <a href="#" data-toggle="modal" data-target="#myModal"><img src="images/2.jpg" alt=" " class="img-responsive" /></a>
                    </div>
                    <div class="agileits_footer_grid_gallery1">
                        <a href="#" data-toggle="modal" data-target="#myModal"><img src="images/3.jpg" alt=" " class="img-responsive" /></a>
                    </div>
                    <div class="agileits_footer_grid_gallery1">
                        <a href="#" data-toggle="modal" data-target="#myModal"><img src="images/4.jpg" alt=" " class="img-responsive" /></a>
                    </div>
                    <div class="agileits_footer_grid_gallery1">
                        <a href="#" data-toggle="modal" data-target="#myModal"><img src="images/7.jpg" alt=" " class="img-responsive" /></a>
                    </div>
                    <div class="agileits_footer_grid_gallery1">
                        <a href="#" data-toggle="modal" data-target="#myModal"><img src="images/8.jpg" alt=" " class="img-responsive" /></a>
                    </div>
                    <div class="agileits_footer_grid_gallery1">
                        <a href="#" data-toggle="modal" data-target="#myModal"><img src="images/9.jpg" alt=" " class="img-responsive" /></a>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <div class="col-md-4 agileinfo_footer_grid">
                <h3>Twitter Posts</h3>
                <ul class="w3agile_footer_grid_list">
                    <li>Ut aut reiciendis voluptatibus maiores <a href="#">http://symphony@example.com</a> alias, ut aut reiciendis.
                        <span><i class="fa fa-twitter" aria-hidden="true"></i>02 days ago</span></li>
                    <li>Itaque earum rerum hic tenetur a sapiente delectus <a href="#">http://symphony@example1.com</a> ut aut
                        voluptatibus.<span><i class="fa fa-twitter" aria-hidden="true"></i>03 days ago</span></li>
                </ul>
            </div>
            <div class="col-md-4 agileinfo_footer_grid">
                <h3>Social Media</h3>
                <ul class="agileinfo_social_icons">
                    <li><a href="#" class="facebook"><span class="fa fa-facebook" aria-hidden="true"></span><i>-</i>Facebook</a></li>
                    <li><a href="#" class="twitter"><span class="fa fa-twitter" aria-hidden="true"></span><i>-</i>Twitter</a></li>
                    <li><a href="#" class="google"><span class="fa fa-google-plus" aria-hidden="true"></span><i>-</i>Google+</a></li>
                    <li><a href="#" class="instagram"><span class="fa fa-instagram" aria-hidden="true"></span><i>-</i>Instagram</a></li>
                </ul>
            </div>
        </div>
    </div>
<!-- //footer -->
<!-- copy-right -->
    <div class="w3agile_copy_right">
        <div class="container">
            <p>Copyright &copy; 2017 Symphony. All Rights Reserved.</p>
        </div>
    </div>
<!-- //copy-right -->
<!-- bootstrap-pop-up -->
    <div class="modal video-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    Symphony
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                        
                </div>
                <section>
                    <div class="modal-body">
                        <div class="col-md-6 w3_modal_body_left">
                            <img src="images/15.jpg" alt=" " class="img-responsive" />
                        </div>
                        <div class="col-md-6 w3_modal_body_right">
                            <h4>Suspendisse et sapien ac diam suscipit posuere</h4>
                            <p>Ut enim ad minima veniam, quis nostrum 
                            exercitationem ullam corporis suscipit laboriosam, 
                            nisi ut aliquid ex ea commodi consequatur? Quis autem 
                            vel eum iure reprehenderit qui in ea voluptate velit 
                            esse quam nihil molestiae consequatur.
                            <i>" Quis autem vel eum iure reprehenderit qui in ea voluptate velit 
                                esse quam nihil molestiae consequatur.</i>
                                Fusce in ex eget ligula tempor placerat. Aliquam laoreet mi id felis commodo 
                                interdum. Integer sollicitudin risus sed risus rutrum 
                                elementum ac ac purus.</p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
<!-- //bootstrap-pop-up -->
@endsection