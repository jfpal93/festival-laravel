<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

use App\Mail\ContactForm;

//Publico
Route::get('/', 'MainController@home');
Route::get('/history', 'HistoriaController@home');
Route::get('/jurado', 'JuradoController@home');
Route::get('/paisesparticipantes', 'PaisParticipanteController@home');
Route::get('/artistasinvitados', 'InvitadoController@home');

Route::get('contact', 
  ['as' => 'contact', 'uses' => 'ContactosController@home']);
Route::post('contact', 
  ['as' => 'contact_store', 'uses' => 'ContactosController@mailToAdmin']);

//Admin
Route::get('/home', 'HomeController@index');
Route::get('/historia', 'HistoryController@index');
Route::get('/jury', 'JuryController@index');
Route::get('/contactform', 'ContactFormController@index');
Route::resource('invitados','InvitadosController');
Route::resource('participantes','ParticipantesController');
Route::resource('jurados','JuradosController');
Route::resource('winners','WinnersController');

Route::resource('avales','AvalesController');
Route::resource('portadas','PortadasController');

Route::resource('patrocinantes','PatrocinanteController');

Route::resource('auspiciantes','AuspicianteController');

Route::resource('cronogramas','CronogramaController');
Route::resource('desceventos','DescEventoController');
Route::resource('invs','InvController');
Route::resource('lugares','LugarController');
Route::resource('parts','PartController');
Route::resource('sedes','SedeController');

Route::resource('destacados','DestacadosController');
Route::resource('galas','GalasController');

Route::resource('contactorganizacion','ContactController');
Route::resource('contactcountry','ContactCountryController');

Route::get('invitados/images/invitados/{filename}',function($filename){
	$path=storage_path("app/images/invitados/$filename");

	if(!\File::exists($path)){
		abort(404);
	}

	$file = \File::get($path);

	$type = \File::mimeType($path);

	$response = Response::make($file,200);

	$response->header("Content-Type",$type);

	return $response;	
});
Route::get('participantes/images/participantes/{filename}',function($filename){
	$path=storage_path("app/images/participantes/$filename");

	if(!\File::exists($path)){
		abort(404);
	}

	$file = \File::get($path);

	$type = \File::mimeType($path);

	$response = Response::make($file,200);

	$response->header("Content-Type",$type);

	return $response;	
});
Route::get('jurados/images/jurados/{filename}',function($filename){
	$path=storage_path("app/images/jurados/$filename");

	if(!\File::exists($path)){
		abort(404);
	}

	$file = \File::get($path);

	$type = \File::mimeType($path);

	$response = Response::make($file,200);

	$response->header("Content-Type",$type);

	return $response;	
});

Route::get('avales/images/avales/{filename}',function($filename){
	$path=storage_path("app/images/avales/$filename");

	if(!\File::exists($path)){
		abort(404);
	}

	$file = \File::get($path);

	$type = \File::mimeType($path);

	$response = Response::make($file,200);

	$response->header("Content-Type",$type);

	return $response;	
});
Route::get('portadas/images/portadas/{filename}',function($filename){
	$path=storage_path("app/images/portadas/$filename");

	if(!\File::exists($path)){
		abort(404);
	}

	$file = \File::get($path);

	$type = \File::mimeType($path);

	$response = Response::make($file,200);

	$response->header("Content-Type",$type);

	return $response;	
});
Route::get('cronogramas/images/cronogramas/{filename}',function($filename){
	$path=storage_path("app/images/cronogramas/$filename");

	if(!\File::exists($path)){
		abort(404);
	}

	$file = \File::get($path);

	$type = \File::mimeType($path);

	$response = Response::make($file,200);

	$response->header("Content-Type",$type);

	return $response;	
});
Route::get('desceventos/images/desceventos/{filename}',function($filename){
	$path=storage_path("app/images/desceventos/$filename");

	if(!\File::exists($path)){
		abort(404);
	}

	$file = \File::get($path);

	$type = \File::mimeType($path);

	$response = Response::make($file,200);

	$response->header("Content-Type",$type);

	return $response;	
});
Route::get('invs/images/invs/{filename}',function($filename){
	$path=storage_path("app/images/invs/$filename");

	if(!\File::exists($path)){
		abort(404);
	}

	$file = \File::get($path);

	$type = \File::mimeType($path);

	$response = Response::make($file,200);

	$response->header("Content-Type",$type);

	return $response;	
});
Route::get('lugares/images/lugares/{filename}',function($filename){
	$path=storage_path("app/images/lugares/$filename");

	if(!\File::exists($path)){
		abort(404);
	}

	$file = \File::get($path);

	$type = \File::mimeType($path);

	$response = Response::make($file,200);

	$response->header("Content-Type",$type);

	return $response;	
});
Route::get('parts/images/parts/{filename}',function($filename){
	$path=storage_path("app/images/parts/$filename");

	if(!\File::exists($path)){
		abort(404);
	}

	$file = \File::get($path);

	$type = \File::mimeType($path);

	$response = Response::make($file,200);

	$response->header("Content-Type",$type);

	return $response;	
});
Route::get('sedes/images/sedes/{filename}',function($filename){
	$path=storage_path("app/images/sedes/$filename");

	if(!\File::exists($path)){
		abort(404);
	}

	$file = \File::get($path);

	$type = \File::mimeType($path);

	$response = Response::make($file,200);

	$response->header("Content-Type",$type);

	return $response;	
});
Route::get('galas/images/galas/{filename}',function($filename){
	$path=storage_path("app/images/galas/$filename");

	if(!\File::exists($path)){
		abort(404);
	}

	$file = \File::get($path);

	$type = \File::mimeType($path);

	$response = Response::make($file,200);

	$response->header("Content-Type",$type);

	return $response;	
});
Route::get('destacados/images/destacados/{filename}',function($filename){
	$path=storage_path("app/images/destacados/$filename");

	if(!\File::exists($path)){
		abort(404);
	}

	$file = \File::get($path);

	$type = \File::mimeType($path);

	$response = Response::make($file,200);

	$response->header("Content-Type",$type);

	return $response;	
});
Route::get('auspiciantes/images/auspiciantes/{filename}',function($filename){
	$path=storage_path("app/images/auspiciantes/$filename");

	if(!\File::exists($path)){
		abort(404);
	}

	$file = \File::get($path);

	$type = \File::mimeType($path);

	$response = Response::make($file,200);

	$response->header("Content-Type",$type);

	return $response;	
});
Route::get('patrocinantes/images/patrocinantes/{filename}',function($filename){
	$path=storage_path("app/images/patrocinantes/$filename");

	if(!\File::exists($path)){
		abort(404);
	}

	$file = \File::get($path);

	$type = \File::mimeType($path);

	$response = Response::make($file,200);

	$response->header("Content-Type",$type);

	return $response;	
});